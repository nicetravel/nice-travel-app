import 'package:dio/dio.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_repository_interface.dart';
import 'package:sqflite/sqflite.dart';

class MockClient extends Mock implements Dio {}

class MockSecurityService extends Mock implements ISecurityService {}

class MockLocalDatabase extends Mock implements IDatabase {}
class MockCloudCronogramaRepository extends Mock implements ICloudCronogramaRepository {}
class MockCloudSearchRepository extends Mock implements ICloudSearchCronogramaRepository {}

class MockDiaCronogramaRepository extends Mock
    implements IDiaCronogramaRepository {}

class MockMeuCronogramaRepository extends Mock
    implements IMeuCronogramaRepository {}

class MockDatabase extends Mock implements Database {}

Map<String, Object> mockOneCronogramaJsonToAPI() {
  return {
    'scheduleCod': 2,
    'numberStar': 0,
    'publish': false,
    'cityAddress': 'Salvador',
    'userUID': '123',
    'userName': 'Thiago',
    'userEmail': 'Thiago',
    'placeID': '123',
    'qtdDays': 5,
    'priceFinal': 0.0,
    'imagesUrl': ['imageurl.com.br']
  };
}

Map<String, Object> mockOneDiaCronogramaJsonToAPI() {
  return {
    'id': 1,
    'day': 1,
    'scheduleCod': 1,
    'typeFirstActivity': 'PARK',
    'qtdActivities': null,
    'priceDay': null
  };
}

Map<String, Object> mockOneAtividadeJsonToAPI() {
  return {
    'description': 'abc',
    'nameOfPlace': 'nomelugar',
    'price': 10.5,
    'styleActivity': 'PARK',
    'startActivity': '06:00',
    'finishActivity': '06:00',
    'idScheduleDay': 88,
    'id': 1
  };
}

Cronograma createCronograma() {
  return Cronograma(
      userName: 'A',
      scheduleCod: 1,
      qtdDays: 5,
      imagesUrl: ['ab'],
      cityAddress: 'a',
      userUID: '1');
}
