import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/shared/city_autocomplete/google_places_repository.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';

import 'mock_json_google.dart';

class MockClient extends Mock implements DioForNative {}

void main() {
  FlavorConfig(
    name: "TEST",
    variables: {
      "google_place_api": 'teste',
    },
  );

  ICityAutocomplete repository;
  MockClient client;

  setUp(() {
    client = MockClient();
    repository = GooglePlacesAPI(client);
  });

  group('CityAutocompleteRepository Test', () {
    test("First Test", () {
      expect(repository, isInstanceOf<GooglePlacesAPI>());
    });

    test('returns a Post if the http call completes successfully', () async {
      when(client.get(
              'https://maps.googleapis.com/maps/api/place/autocomplete/json',
              queryParameters: anyNamed('queryParameters')))
          .thenAnswer((_) async => Response(
              requestOptions: null,
              data: jsonGooglePlaceMock,
              statusCode: 200));
      var cities = await repository.getCities('Salvador');
      expect(cities.isRight(), true);
      cities.fold((l) => null, (r) => expect(r.length, 5));
    });
  });
}
