import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';

void main() {
  CityAutocompleteController cityController;

  setUp(() {
    cityController = CityAutocompleteController();
  });

  group('CityAutcompleteController Test', () {
    test("Set PlaceID", () {
      expect(cityController.getPlaceId(), equals(null));
      cityController.changePlaceId("ABC");
      expect(cityController.getPlaceId(), equals("ABC"));
    });

    test("Set NameCity", () {
      expect(cityController.getNameCity(), equals(null));
      cityController.changeNameCity("ABC");
      expect(cityController.getNameCity(), equals("ABC"));
    });

    test("Set cityInputsNull", () {
      expect(cityController.cityInputsNull, equals(true));
      cityController.cityInputsNull = false;
      expect(cityController.cityInputsNull, equals(false));
    });

    test("Clear Place ID", () {
      cityController.changeNameCity("ABC");
      cityController.changePlaceId("ABC");
      expect(cityController.getNameCity(), equals("ABC"));
      expect(cityController.getPlaceId(), equals("ABC"));
      cityController.clearPlaceID();

      expect(cityController.getPlaceId(), equals(null));
      expect(cityController.getNameCity(), equals(null));
    });
  });
}
