
var jsonGooglePlaceMock =
{
  "predictions": [
    {
      "description": "Salvador, BA, Brasil",
      "matched_substrings": [{"length": 7, "offset": 0}],
      "place_id": "ChIJvS5CUCARFgcRndtzlTaEHPc",
      "reference": "ChIJvS5CUCARFgcRndtzlTaEHPc",
      "structured_formatting": {
        "main_text": "Salvador",
        "main_text_matched_substrings": [{"length": 7, "offset": 0}],
        "secondary_text": "BA, Brasil"
      },
      "terms": [
        {"offset": 0, "value": "Salvador"},
        {"offset": 10, "value": "BA"},
        {"offset": 14, "value": "Brasil"}
      ],
      "types": ["locality", "political", "geocode"]
    },
    {
      "description": "Salvador do Sul, RS, Brasil",
      "matched_substrings": [{"length": 7, "offset": 0}],
      "place_id": "ChIJW9Jh-bwFHJURrS537_KdnJw",
      "reference": "ChIJW9Jh-bwFHJURrS537_KdnJw",
      "structured_formatting": {
        "main_text": "Salvador do Sul",
        "main_text_matched_substrings": [{"length": 7, "offset": 0}],
        "secondary_text": "RS, Brasil"
      },
      "terms": [
        {"offset": 0, "value": "Salvador do Sul"},
        {"offset": 17, "value": "RS"},
        {"offset": 21, "value": "Brasil"}
      ],
      "types": ["locality", "political", "geocode"]
    },
    {
      "description": "Salvador Mazza, Salta, Argentina",
      "matched_substrings": [{"length": 7, "offset": 0}],
      "place_id": "ChIJ93r4-zkvD5QRwoBNM9G9Yk4",
      "reference": "ChIJ93r4-zkvD5QRwoBNM9G9Yk4",
      "structured_formatting": {
        "main_text": "Salvador Mazza",
        "main_text_matched_substrings": [{"length": 7, "offset": 0}],
        "secondary_text": "Salta, Argentina"
      },
      "terms": [
        {"offset": 0, "value": "Salvador Mazza"},
        {"offset": 16, "value": "Salta"},
        {"offset": 23, "value": "Argentina"}
      ],
      "types": ["locality", "political", "geocode"]
    },
    {
      "description": "Salvador Escalante, Michoacán, México",
      "matched_substrings": [{"length": 7, "offset": 0}],
      "place_id": "ChIJ16jLgWCMLYQRaUC10DIofX4",
      "reference": "ChIJ16jLgWCMLYQRaUC10DIofX4",
      "structured_formatting": {
        "main_text": "Salvador Escalante",
        "main_text_matched_substrings": [{"length": 7, "offset": 0}],
        "secondary_text": "Michoacán, México"
      },
      "terms": [
        {"offset": 0, "value": "Salvador Escalante"},
        {"offset": 20, "value": "Michoacán"},
        {"offset": 31, "value": "México"}
      ],
      "types": ["locality", "political", "geocode"]
    },
    {
      "description": "Salvador do Mundo, Goa, Índia",
      "matched_substrings": [{"length": 7, "offset": 0}],
      "place_id": "ChIJofSs1_m_vzsRrZQiQGgV9yY",
      "reference": "ChIJofSs1_m_vzsRrZQiQGgV9yY",
      "structured_formatting": {
        "main_text": "Salvador do Mundo",
        "main_text_matched_substrings": [{"length": 7, "offset": 0}],
        "secondary_text": "Goa, Índia"
      },
      "terms": [
        {"offset": 0, "value": "Salvador do Mundo"},
        {"offset": 19, "value": "Goa"},
        {"offset": 24, "value": "Índia"}
      ],
      "types": ["locality", "political", "geocode"]
    }
  ],
  "status": "OK"
};