import 'package:connectivity/connectivity.dart';
import 'package:connectivity_platform_interface/connectivity_platform_interface.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/security_service.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'mocks/firebase_mock.dart';

class MockUser extends Mock implements User {}

void main() {
  ISecurityService service;
  setupFirebaseAuthMocks();

  MethodChannel('plugins.it_nomads.com/flutter_secure_storage')
      .setMockMethodCallHandler((MethodCall methodCall) async {
    switch (methodCall.method) {
      case 'read':
        return null;
      case 'write':
        return null;
      default:
        return null;
    }
  });

  setUp(() {
    MockConnectivityPlatform  fakePlatform = MockConnectivityPlatform();
    ConnectivityPlatform.instance = fakePlatform;
    service = SecurityService();
  });


  group('SecurityService Test', () {
    test("First Test", () {
      expect(service, isInstanceOf<ISecurityService>());
    });

    test("Test SignInSilently", () async {
      var retorno = await service.signInSilently();
      expect(retorno, false);
    });

  });
}

const ConnectivityResult kCheckConnectivityResult = ConnectivityResult.wifi;
const LocationAuthorizationStatus kRequestLocationResult =
    LocationAuthorizationStatus.authorizedAlways;
const LocationAuthorizationStatus kGetLocationResult =
    LocationAuthorizationStatus.authorizedAlways;

class MockConnectivityPlatform extends Fake
    with MockPlatformInterfaceMixin
    implements ConnectivityPlatform {
  Future<ConnectivityResult> checkConnectivity() async {
    return kCheckConnectivityResult;
  }
}
