import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/config/api/api_connection_helper.dart';
import 'package:nice_travel/app/config/api/cronograma_api_connection.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cronograma_api_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/cronograma_api_resource.dart';

import '../../mocks/mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  MockClient mockClient;
  ICloudCronogramaRepository repository;
  IDatabase _databaseConfig;
  IMeuCronogramaRepository _meuCronogramaRepository;
  ISecurityService _securityService;

  FlavorConfig(
    name: "TEST",
    variables: {
      "url_api": 'teste',
    },
  );

  setUp(() {
    _databaseConfig = MockLocalDatabase();
    _securityService = MockSecurityService();
    _meuCronogramaRepository = MockMeuCronogramaRepository();
    mockClient = MockClient();
    repository = CronogramaRepository(mockClient, _databaseConfig,
        _meuCronogramaRepository, _securityService);
  });

  test("First Test", () {
    expect(repository, isInstanceOf<CronogramaRepository>());
  });

  group('SearchCronogramaRepository Test', () {
    Cronograma cronograma;
    setUp(() {
      cronograma = Cronograma(
          userName: 'A',
          scheduleCod: 1,
          qtdDays: 5,
          imagesUrl: ['ab'],
          cityAddress: 'a',
          userUID: '123');
    });

    test('Should publish Cronograma', () async {
      var json = jsonEncode(CronogramaAPI(cronograma, []).toMap());
      when(_databaseConfig.fetchDiasCronograma(1, codFromApi: false))
          .thenAnswer((_) async => []);
      when(mockClient.post(
              ApiConnectionHelper.urlApi + CronogramaAPIConnection.path,
              options: anyNamed('options'),
              data: json))
          .thenAnswer((_) async => Response(
              requestOptions: null,
              data: {
                'scheduleCod': 1,
                'imagesUrl': [],
                'scheduleDays': [],
              },
              statusCode: 200));

      await repository.publish(cronograma);

      verify(_meuCronogramaRepository.publishCronograma(1, any));
    });

    test('Should remove Cronograma', () async {
      cronograma.scheduleCodAPI = 88;
      when(mockClient.delete(
              ApiConnectionHelper.urlApi +
                  CronogramaAPIConnection.path +
                  '${cronograma.scheduleCodAPI}',
              options: anyNamed('options')))
          .thenAnswer((_) async =>
              Response(requestOptions: null, data: 1, statusCode: 200));

      await repository.removeCronograma(cronograma);

      verify(_meuCronogramaRepository.remove(cronograma.scheduleCod));
    });

    test('Should duplicate Cronograma', () async {
      when(_securityService.getSessionUser()).thenAnswer((_) => SessionUser(
          uid: '1', tokenMessage: '1', email: 'email', displayName: 'user'));

      when(mockClient.post(
        ApiConnectionHelper.urlApi + CronogramaAPIConnection.path + 'duplicate',
        data: anyNamed("data"),
        options: anyNamed('options'),
      )).thenAnswer((_) async => Response(
          requestOptions: null,
          data: {
            'cronograma': mockOneCronogramaJsonToAPI(),
            'diasCronogramas': []
          },
          statusCode: 200));

      await repository.duplicate(cronograma);
      verify(_meuCronogramaRepository.duplicateCronograma(any));
    });

    test('Should republish Cronograma', () async {
      when(_securityService.getSessionUser()).thenAnswer((_) => SessionUser(
          uid: '1', tokenMessage: '1', email: 'email', displayName: 'user'));

      when(_databaseConfig.fetchDiasCronograma(1, codFromApi: false))
          .thenAnswer((_) async => []);
      when(mockClient.post(
        ApiConnectionHelper.urlApi +
            CronogramaAPIConnection.path +
            'republish/1',
        options: anyNamed('options'),
        data: anyNamed("data"),
      )).thenAnswer((_) async => Response(
          requestOptions: null,
          data: {
            'scheduleCod': 1,
            'imagesUrl': [],
            'scheduleDays': [],
          },
          statusCode: 200));
      cronograma.scheduleCodAPI = 1;
      await repository.rePublish(cronograma);
      verify(_meuCronogramaRepository.publishCronograma(any, any));
    });
  });
}
