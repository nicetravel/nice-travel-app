import 'package:dio/dio.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/config/api/api_connection_helper.dart';
import 'package:nice_travel/app/config/api/atividade_api_connection.dart';
import 'package:nice_travel/app/config/api/cronograma_api_connection.dart';
import 'package:nice_travel/app/config/api/dia_cronograma_api_connection.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/repository/search_cronograma_api_repository.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import '../../mocks/mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  MockClient mockClient;
  ICloudSearchCronogramaRepository repository;

  FlavorConfig(
    name: "TEST",
    variables: {
      "url_api": 'teste',
    },
  );

  setUp(() {
    mockClient = MockClient();
    repository = SearchCronogramaRepository(mockClient);
  });

  group('SearchCronogramaRepository Test', () {
    test("First Test", () {
      expect(repository, isInstanceOf<SearchCronogramaRepository>());
    });

    test('Should find all cronogramas by placeID', () async {
      final placeID = "placeID1";
      final qtdElements = 2;

      when(mockClient.get(
              ApiConnectionHelper.urlApi +
                  CronogramaAPIConnection.path +
                  "city?sizeElements=$qtdElements&placeID=$placeID",
              options: anyNamed('options')))
          .thenAnswer((_) async => Response(
              requestOptions: null,
              data: [mockOneCronogramaJsonToAPI()],
              statusCode: 200));

      ApiResponse<List<Cronograma>> cronogramas =
          await repository.findCronogramas(placeID, qtdElements);
      expect(cronogramas.data.length, 1);
    });

    test('Should find all dias cronogramas by cronograma ID', () async {
      final scheduleCod = 21;
      when(mockClient.get(
              ApiConnectionHelper.urlApi +
                  DiaCronogramaApiConnection.path +
                  "$scheduleCod",
              options: anyNamed('options')))
          .thenAnswer((_) async => Response(
              requestOptions: null,
              data: [mockOneDiaCronogramaJsonToAPI()],
              statusCode: 200));

      ApiResponse<List<DiaCronograma>> diasCronogramas =
          await repository.findDiasCronogramas(scheduleCod);
      expect(diasCronogramas.data.length, 1);
    });

    test('Should find all atividades by dia cronograma id', () async {
      final scheduleDay = 87;
      when(mockClient.get(
              ApiConnectionHelper.urlApi +
                  AtividadeApiConnection.path +
                  "$scheduleDay",
              options: anyNamed('options')))
          .thenAnswer((_) async => Response(
              requestOptions: null,
              data: [mockOneAtividadeJsonToAPI()],
              statusCode: 200));

      ApiResponse<List<Atividade>> atividades =
          await repository.findAllAtividades(scheduleDay);
      expect(atividades.data.length, 1);
    });
  });
}
