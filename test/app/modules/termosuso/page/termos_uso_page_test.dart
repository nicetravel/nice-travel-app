import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/termosuso/page/termos_uso_page.dart';
import 'package:nice_travel/app/modules/termosuso/termos_uso_module.dart';
import 'package:nice_travel/app/modules/termosuso/widget/termos_uso_text.dart';

import '../../../mocks/firebase_mock.dart';

void main() {

  setupFirebaseAuthMocks();
  initModules([AppModule(), TermosUsoModule()]);
  testWidgets('TermosUsoPage has title', (tester) async {
    await tester.pumpWidget(buildTestableWidget(TermosUsoPage()));
    final titleFinder = find.text('Termos de Uso');
    expect(titleFinder, findsOneWidget);
  });

  testWidgets('TermosUsoPage has TermosUsoText', (tester) async {
    await tester.pumpWidget(buildTestableWidget(TermosUsoPage()));
    final termosTextFinder = find.byType(TermosUsoText);
    expect(termosTextFinder, findsOneWidget);
  });
}
