import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/navigation/page/navigation_page.dart';

import '../../mocks/firebase_mock.dart';


void main() {
  initModules([AppModule()]);
  setupFirebaseAuthMocks();

  testWidgets('NavigationPage has bottom bar', (tester) async {
    await tester.pumpWidget(buildTestableWidget(NavigationPage()));
    final drawerFinder = find.byIcon(Icons.person);
    expect(drawerFinder, findsOneWidget);
  });

}
