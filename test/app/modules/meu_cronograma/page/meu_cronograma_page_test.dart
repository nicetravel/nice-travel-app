import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/meu_cronograma_controller.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/meu_cronograma_page.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import '../../../mocks/firebase_mock.dart';

class MeuCronogramaControllerMock extends Mock
    implements MeuCronogramaController {}

void main() {
  var controller = MeuCronogramaControllerMock();
  initModules([AppModule(), MeuCronogramaModule()],
      changeBinds: [Bind<MeuCronogramaController>((_) => controller)]);
  setupFirebaseAuthMocks();

  setUp(() {
    var diasCronogramas = ApiResponse.completed(getCronogramas());
    when(controller.findAll()).thenAnswer((_) async => diasCronogramas);
    when(controller.canRefresh()).thenAnswer((_) => true);
    when(controller.meusCronogramas).thenAnswer((_) => getCronogramas());
    when(controller.synchronizedFromApi()).thenAnswer((_) async => false);
  });

  test("Test Mock", () {
    expect(MeuCronogramaModule.to.get<MeuCronogramaController>(),
        isInstanceOf<MeuCronogramaControllerMock>());
  });

  testWidgets('MeuCronogramaPage has bottom bar', (tester) async {
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaPage()));
    final drawerFinder = find.byIcon(Icons.favorite);
    expect(drawerFinder, findsOneWidget);
  });
}

List<Cronograma> getCronogramas() {
  return [
    Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityAddress: 'teste')
  ];
}
