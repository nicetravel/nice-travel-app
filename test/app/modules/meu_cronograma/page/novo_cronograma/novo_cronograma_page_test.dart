import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/novo_cronograma/novo_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/novo_cronograma/page/novo_cronograma_page.dart';

import '../../../../mocks/firebase_mock.dart';

void main() {
  setupFirebaseAuthMocks();
  initModules([MeuCronogramaModule(), NovoCronogramaModule(), AppModule()]);

  testWidgets('NovoCronogramaPage has title', (tester) async {
    await tester.pumpWidget(buildTestableWidget(NovoCronogramaPage()));
    final titleFinder = find.text('Novo Cronograma');
    expect(titleFinder, findsOneWidget);
  });
}
