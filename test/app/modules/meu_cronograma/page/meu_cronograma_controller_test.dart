import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/meu_cronograma_controller.dart';

import '../../../mocks/firebase_mock.dart';
import 'novo_cronograma/novo_cronograma_controller_test.dart';

void main() {
  var _mockRepository = MeuCronogramaRepositoryMock();
  initModules([MeuCronogramaModule(), AppModule()],
      changeBinds: [Bind<IMeuCronogramaRepository>((_) => _mockRepository)]);
  setupFirebaseAuthMocks();
  MeuCronogramaController meuCronograma;

  setUp(() {
    meuCronograma = MeuCronogramaModule.to.get<MeuCronogramaController>();
  });

  group('MeuCronogramaController Test', () {
    test("First Test", () {
      expect(meuCronograma, isInstanceOf<MeuCronogramaController>());
    });

    test("Should find all inside repository", () {
      meuCronograma.findAll();
      verify(_mockRepository.findAll());
    });

    test("Should find cronogramas", () {
      expect(meuCronograma.canRefresh(), true);
      meuCronograma.fillCronogramas([]);
      expect(meuCronograma.canRefresh(), true);
      meuCronograma.fillCronogramas([Cronograma()]);
      expect(meuCronograma.canRefresh(), false);
    });
  });
}
