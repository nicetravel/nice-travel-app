import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/config/local_database.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_local_repository.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/cronograma_publish_response.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/duplicated_cronograma_api_resource.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:sqflite/sqflite.dart';

import '../../../mocks/mock.dart';

void main() {
  IMeuCronogramaRepository repository;
  TestWidgetsFlutterBinding.ensureInitialized();
  IDatabase mockLocalDatabase;
  Database mockDatabase;
  ISecurityService securityClient;
  final scheduleCod = 31;

  setUp(() {
    securityClient = MockSecurityService();
    mockDatabase = MockDatabase();
    mockLocalDatabase = MockLocalDatabase();
    repository =
        MeuCronogramaLocalRepository(securityClient, mockLocalDatabase);
  });

  group('MeuCronogramaRepository Test', () {
    test("First Test", () {
      expect(repository, isInstanceOf<MeuCronogramaLocalRepository>());
    });

    setUp(() {
      when(mockLocalDatabase.database).thenAnswer((_) async => mockDatabase);

      when(securityClient.getSessionUser()).thenAnswer((_) => SessionUser(
          uid: '1', tokenMessage: '1', email: 'email', displayName: 'user'));

      when(mockDatabase.insert(LocalDatabase.TB_SCHEDULE_TRAVEL, any))
          .thenAnswer((_) async => scheduleCod);
    });

    test('Should find all meus Cronogramas', () async {
      when(mockDatabase.rawQuery(any)).thenAnswer((_) async => [
            {
              'CO_SCHEDULE_TRAVEL': 2,
              'NU_LIKE': 0,
              'ST_PUBLIC_ACCESS': false,
              'DS_PLACE': 'Salvador',
              'CO_USER': '123',
              'DS_USER': 'Thiago',
              'NU_DAYS': 5,
              'NU_PRICE': 0.0,
              'imagesUrl':
                  'https://media.istockphoto.com/vectors/city-silhouette-land-scape-horizontal-city-landscape-downtown-skyline-vector-id959234938?k=6&m=959234938&s=170667a&w=0&h=yjJNnGWHPO23Jxsv2vMsYxbhi3oEEjEwlnM3dUEwBBU='
            }
          ]);

      ApiResponse<List<Cronograma>> cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 1);

      await repository
          .saveOrUpdate(MeuCronograma.initValues('_nomeLugar', 2, null));
      cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 2);
    });

    test(
        'should get error when try to alter local repository list'
        ' (Must be immutable)', () async {
      MeuCronograma meuCronograma = createNewMeuCronograma();
      await repository.saveOrUpdate(meuCronograma);
      ApiResponse<List<Cronograma>> cronogramas = await repository.findAll();
      expect(() => cronogramas.data.removeLast(),
          throwsA(isA<UnsupportedError>()));
    });

    test('should delete meu meus Cronogramas ', () async {
      MeuCronograma meuCronograma = createNewMeuCronograma();
      await repository.saveOrUpdate(meuCronograma);
      ApiResponse<List<Cronograma>> cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 1);
      await repository.remove(scheduleCod);

      cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 0);
    });

    test('should create new and update meus Cronogramas ', () async {
      MeuCronograma meuCronograma = createNewMeuCronograma();
      await repository.saveOrUpdate(meuCronograma);
      ApiResponse<List<Cronograma>> cronogramas = await repository.findAll();
      expect(cronogramas.data[0].cityAddress, "CIDADE");

      await repository.saveOrUpdate(
          MeuCronograma.initValues('CIDADE ALTERADA', 1, scheduleCod));
      cronogramas = await repository.findAll();
      expect(cronogramas.data[0].cityAddress, "CIDADE ALTERADA");
    });

    test('should duplicate Cronograma ', () async {
      MeuCronograma meuCronograma = createNewMeuCronograma();
      await repository.saveOrUpdate(meuCronograma);
      Cronograma cronograma;
      CronogramaPublishResponse cronogramaPublish =
          new CronogramaPublishResponse(scheduleCod, [], []);
      cronograma =
          await repository.publishCronograma(scheduleCod, cronogramaPublish);
      expect(cronograma.scheduleCod, scheduleCod);
      var cronogramas = await repository.findAll();
      expect(cronogramas.data.length, 1);
    });

    test('should publish Cronograma ', () async {
      var duplicated = DuplicatedCronogramaAPI.fromMap(
          {'cronograma': mockOneCronogramaJsonToAPI(), 'diasCronogramas': []});
      final newCronograma = await repository.duplicateCronograma(duplicated);
      expect(newCronograma.qtdDays, 5);
      expect(newCronograma.userName, "Thiago");
      expect(newCronograma.scheduleCod, isNot(2));
      expect(newCronograma.scheduleCodAPI, 2);
    });
  });
}

MeuCronograma createNewMeuCronograma() {
  MeuCronograma meuCronograma = MeuCronograma();
  meuCronograma.setNomeLugar("CIDADE");
  meuCronograma.setPlaceId("ID");
  meuCronograma.setQtdDias("1");
  return meuCronograma;
}
