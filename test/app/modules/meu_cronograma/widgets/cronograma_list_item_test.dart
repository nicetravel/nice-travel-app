import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/widgets/cronograma_list_item.dart';

void main() {
  testWidgets('MeuCronogramaListItem has DecorationImage', (tester) async {
    Cronograma cronograma = Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityState: "UF",
        cityAddress: 'teste');
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaListItem(
      cronograma: cronograma,
    )));
    final imageFinder = find.byKey(Key("list_item_card_image"));
    expect(imageFinder, findsOneWidget);
  });

  testWidgets('MeuCronogramaListItem has city text', (tester) async {
    Cronograma cronograma = Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityState: "UF",
        cityAddress: 'teste');
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaListItem(
      cronograma: cronograma,
    )));
    final cityFinder = find.text("teste - UF");
    expect(cityFinder, findsNWidgets(2));
  });

  testWidgets('MeuCronogramaListItem has qtd days', (tester) async {
    Cronograma cronograma = Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityState: "UF",
        cityAddress: 'teste');
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaListItem(
      cronograma: cronograma,
    )));
    final dayFinder = find.text("4 Dia(s)");
    expect(dayFinder, findsNWidgets(2));
  });

  testWidgets('MeuCronogramaListItem has price', (tester) async {
    Cronograma cronograma = Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityState: "UF",
        cityAddress: 'teste');
    cronograma.priceFinal = 25.0;
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaListItem(
      cronograma: cronograma,
    )));
    final moneyFinder = find.text("R\$  25,00");
    expect(moneyFinder, findsNWidgets(2));
  });

  testWidgets('MeuCronogramaListItem has stars', (tester) async {
    Cronograma cronograma = Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityState: "UF",
        cityAddress: 'teste');
    cronograma.priceFinal = 25.0;
    cronograma.numberLike = 3;
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaListItem(
      cronograma: cronograma,
    )));
    final moneyFinder = find.text("3");
    expect(moneyFinder, findsNWidgets(2));
  });

  testWidgets('MeuCronogramaListItem has nothing', (tester) async {
    Cronograma cronograma = Cronograma(
        scheduleCod: 1,
        userName: 'teste',
        userUID: '123',
        qtdDays: 4,
        cityState: "UF",
        cityAddress: 'teste');
    cronograma.removed = true;
    await tester.pumpWidget(buildTestableWidget(MeuCronogramaListItem(
      cronograma: cronograma,
    )));
    final cityFinder = find.byType(Text);
    expect(cityFinder, findsNothing);
  });
}
