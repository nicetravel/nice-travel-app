import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/atividades_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/nova_atividade/nova_atividade_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/repository/atividade_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/atividade_card.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/new_atividade_button.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/util/format_util.dart';

import '../../../../mocks/mock.dart';

class AtividadeRepositoryMock extends Mock implements IAtividadeRepository {}

void main() {
  var securityMock = MockSecurityService();
  initModules([
    AppModule(),
    AtividadesModule(),
    DiaCronogramaModule(),
  ], changeBinds: [
    Bind<ISecurityService>((i) => securityMock),
    Bind<IAtividadeRepository>((i) => AtividadeRepositoryMock()),
  ]);
  Modular.init(AppModule());
  AtividadesModule.to.get<IAtividadeRepository>();

  IAtividadeRepository atividadeRepository;

  setUp(() {
    atividadeRepository = AtividadesModule.to.get<IAtividadeRepository>();
    when(securityMock.getSessionUser()).thenAnswer((realInvocation) =>
        new SessionUser(
            displayName: null, email: null, uid: "2", tokenMessage: null));
  });

  testWidgets('Load atividadesPage', (tester) async {
    mockAtividades(atividadeRepository);
    var diaCronograma = DiaCronograma(
        day: 1,
        id: 1,
        priceDay: 25,
        qtdActivities: 5,
        typeFirstActivity: 'Park');
    await tester.pumpWidget(buildTestableWidget(AtividadesPage(
      cronograma: Cronograma(userUID: "2"),
      diaCronograma: diaCronograma,
    )));
    await tester.pump();
    final scaffoldFinder = find.byType(Scaffold);
    expect(scaffoldFinder, findsOneWidget);
    final timeLineFinder = find.byType(AtividadeCard);
    expect(timeLineFinder, findsOneWidget);
    final appBarFinder = find.byType(AppBar);
    expect(appBarFinder, findsOneWidget);
  });

  testWidgets('Click new atividade page', (tester) async {
    mockAtividades(atividadeRepository);
    var diaCronograma = DiaCronograma(
        day: 1,
        id: 1,
        priceDay: 25,
        qtdActivities: 5,
        typeFirstActivity: 'Park');
    await tester.pumpWidget(buildTestableWidget(AtividadesPage(
      cronograma: Cronograma(userUID: "2"),
      diaCronograma: diaCronograma,
    )));
    await tester.tap(find.byType(NewAtividadeButton));
    await tester.pumpAndSettle();
    expect(find.byType(NovaAtividadePage), findsOneWidget);
  });
}

void mockAtividades(IAtividadeRepository atividadeRepository) {
  var atividades = [
    Atividade(
        id: 1,
        nameOfPlace: 'Praia do Farol da Barra',
        startActivityDate: formatStringToHora('08:00'),
        finishActivityDate: formatStringToHora('13:00'),
        price: 25.0,
        styleActivity: 'SWIMMING'),
  ];
  when(atividadeRepository.findAll(1))
      .thenAnswer((_) async => ApiResponse.completed(atividades));
}
