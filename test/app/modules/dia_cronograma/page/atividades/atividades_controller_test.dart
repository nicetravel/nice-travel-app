import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/atividades_controller.dart';
import 'package:nice_travel/app/util/format_util.dart';

import '../../../../mocks/firebase_mock.dart';

void main() {
  initModules([AtividadesModule(), AppModule()]);
  setupFirebaseAuthMocks();
  AtividadesController controller;

  setUp(() async {
    controller = AtividadesModule.to.get<AtividadesController>();
  });

  group('AtividadesController Test', () {
    Atividade atividade;
    setUp(() {
      controller.atividades.clear();
      atividade = Atividade(
          nameOfPlace: 'Praia do Farol da Barra',
          startActivityDate: formatStringToHora('08:00'),
          finishActivityDate: formatStringToHora('13:00'),
          price: 25.0,
          styleActivity: 'SWIMMING');
    });

    test("First Test", () {
      expect(controller, isInstanceOf<AtividadesController>());
    });

    test("Test Delete Local Atividade", () {
      controller.saveOrUpdateLocal(atividade, null);
      expect(controller.atividades.length, 1);
      controller.deleteLocal(atividade);
      expect(controller.atividades.length, 0);
    });

    test("Test Save Local Atividade", () {
      expect(controller.atividades.length, 0);
      controller.saveOrUpdateLocal(atividade, null);
      expect(controller.atividades.length, 1);
    });

    test("Test Update Local Atividade", () {
      controller.saveOrUpdateLocal(atividade, null);
      expect(controller.atividades[0].price, 25.0);

      atividade.price = 200.0;
      atividade.id = 1;
      controller.saveOrUpdateLocal(atividade, atividade.id);
      expect(controller.atividades[0].price, 200.0);
    });
  });
}
