import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/atividades_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/nova_atividade/nova_atividade_page.dart';

import '../../../../../../mocks/mock.dart';

class AtividadesControllerMock extends Mock implements AtividadesController {}

void main() {
  var controller = AtividadesControllerMock();
  initModules([AppModule(), AtividadesModule()],
      changeBinds: [Bind<AtividadesController>((_) => controller)]);

  setUp(() {
    when(controller.saveAtividade(any)).thenAnswer((value) => Future.value(1));
  });

  test("Test Mock", () {
    expect(AtividadesModule.to.get<AtividadesController>(),
        isInstanceOf<AtividadesControllerMock>());
  });

  testWidgets('Should load atividade form', (tester) async {
    DiaCronograma diaCronograma = createDiaCronograma();
    Atividade atividade = createAtividade();
    await tester.pumpWidget(buildTestableWidget(NovaAtividadePage(
      atividade: atividade,
      diaCronograma: diaCronograma,
    )));
    final formFinder = find.byType(Form);
    expect(formFinder, findsOneWidget);

    await tapSaveButton(tester);
    verify(controller.saveAtividade(atividade));
  });

  testWidgets('Should validate form', (tester) async {
    DiaCronograma diaCronograma = createDiaCronograma();
    var atividade = Atividade.newInstance(1);
    await tester.pumpWidget(buildTestableWidget(NovaAtividadePage(
      atividade: atividade,
      diaCronograma: diaCronograma,
    )));
    final formFinder = find.byType(Form);
    expect(formFinder, findsOneWidget);

    await tapSaveButton(tester);
    await tester.pump();
    expect(find.text('É necessário preencher todos os campos'), findsOneWidget);
  });

  testWidgets('Should update Price of dia Cronograma ', (tester) async {
    DiaCronograma diaCronograma = createDiaCronograma();
    Atividade atividade = createAtividade();
    await tester.pumpWidget(buildTestableWidget(NovaAtividadePage(
      atividade: atividade,
      diaCronograma: diaCronograma,
    )));
    final formFinder = find.byType(Form);
    expect(formFinder, findsOneWidget);

    final priceTextFinder = find.byKey(Key('price_text'));
    await tester.tap(priceTextFinder);
    await tester.enterText(priceTextFinder, "10.00");
    await tester.pump();
    await tapSaveButton(tester);
    await tester.pump();
    expect(atividade.price, 10.0);
    expect(diaCronograma.priceDay, 40);
  });

  testWidgets('Should validate Date', (tester) async {
    DiaCronograma diaCronograma = createDiaCronograma();
    var atividade = Atividade.newInstance(1);
    await tester.pumpWidget(buildTestableWidget(NovaAtividadePage(
      atividade: atividade,
      diaCronograma: diaCronograma,
    )));
    final formFinder = find.byType(Form);
    expect(formFinder, findsOneWidget);

    await tester.tap(find.byKey(Key('date_end_text')));
    await tester.pump();
    var center = tester
        .getCenter(find.byKey(const ValueKey<String>('time-picker-dial')));

    await tester.tapAt(Offset(center.dx - 100, center.dy));
    await tester.tap(find.text('OK'));
    await tester.pump();
    expect(find.text(NovaAtividadePage.DATA_BETWEEN_VALIDATE_MESSAGE),
        findsOneWidget);
  });
}

Future tapSaveButton(WidgetTester tester) async {
  final saveButtonFinder = find.byKey(Key('saveatividade_button'));
  await tester.tap(saveButtonFinder);
}

Atividade createAtividade() {
  var atividade = Atividade(
      startActivityDate: DateTime(2000, 1, 1, 23),
      id: 2,
      nameOfPlace: 'Almoço no Pirão Baiano',
      price: 20,
      styleActivity: 'RESTAURANT');
  return atividade;
}

DiaCronograma createDiaCronograma() {
  var diaCronograma = DiaCronograma(
      day: 1, id: 1, priceDay: 50, qtdActivities: 5, typeFirstActivity: 'Park');
  diaCronograma.setCronograma(createCronograma());
  return diaCronograma;
}
