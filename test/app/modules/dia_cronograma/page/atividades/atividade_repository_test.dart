import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/repository/atividade_local_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/repository/atividade_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:sqflite/sqflite.dart';

import '../../../../mocks/mock.dart';

class MockClient extends Mock implements Dio {}

void main() {
  IAtividadeRepository repository;
  IDatabase mockLocalDatabase;
  Database mockDatabase;

  setUp(() {
    mockDatabase = MockDatabase();
    mockLocalDatabase = MockLocalDatabase();
    repository = AtividadeLocalRepository(mockLocalDatabase);
  });

  group('AtividadeRepository Test', () {
    test("First Test", () {
      expect(repository, isInstanceOf<AtividadeLocalRepository>());
    });

    setUp(() {
      when(mockLocalDatabase.database).thenAnswer((_) async => mockDatabase);
      when(mockLocalDatabase.fetchAtividades(any)).thenAnswer((_) async => []);
    });

    test('should find all atividades by idDiaCronograma', () async {
      when(mockLocalDatabase.fetchAtividades(any)).thenAnswer((_) async => [
            Atividade.fromMap({
              'CO_ACTIVITY': 1,
              'DS_DESCRIBE': 'teste',
              'DT_END': '08:00',
              'DT_START': '06:00',
              'DS_NAME': 'Corrida',
              'VL_PRICE': 20.0,
              'DS_TYPE_ACTIVITY': 'PARK',
              'CO_SCHEDULE_DAY': 1
            }),
            Atividade.fromMap({
              'CO_ACTIVITY': 2,
              'DS_DESCRIBE': 'teste',
              'DT_END': '08:00',
              'DT_START': '06:00',
              'DS_NAME': 'Corrida',
              'VL_PRICE': 20.0,
              'DS_TYPE_ACTIVITY': 'PARK',
              'CO_SCHEDULE_DAY': 1
            })
          ]);
      ApiResponse<List<Atividade>> atividades = await repository.findAll(1);
      expect(atividades.data.length, 2);
    });

    test(
        'should get error when try to alter local repository list'
        ' (Must be immutable)', () async {
      Atividade atividade = Atividade.newInstance(3);
      await repository.saveOrUpdate(atividade);
      ApiResponse<List<Atividade>> cronogramas = await repository.findAll(1);
      expect(() => cronogramas.data.removeLast(),
          throwsA(isA<UnsupportedError>()));
    });

    test('should delete activity', () async {
      Atividade atividade = Atividade.newInstance(3);
      atividade.description = 'nova';
      await repository.saveOrUpdate(atividade);
      ApiResponse<List<Atividade>> atividades = await repository.findAll(1);
      expect(atividades.data.length, 1);
      await repository.remove(atividade);
      atividades = await repository.findAll(1);
      expect(atividades.data.length, 0);
    });

    test('should create new and update activity', () async {
      Atividade atividade = Atividade.newInstance(3);
      atividade.description = 'nova';
      var id = await repository.saveOrUpdate(atividade);
      ApiResponse<List<Atividade>> atividades = await repository.findAll(1);
      atividade.id = id;
      expect(atividades.data[0].description, 'nova');

      atividade.description = 'atualizada';
      await repository.saveOrUpdate(atividade);
      atividades = await repository.findAll(1);
      expect(atividades.data[0].description, 'atualizada');
    });
  });
}
