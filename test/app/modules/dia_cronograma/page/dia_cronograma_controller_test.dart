import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import '../../../mocks/mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  DiaCronogramaController controller;

  IDiaCronogramaRepository repository = MockDiaCronogramaRepository();
  IMeuCronogramaRepository meuRepository = MockMeuCronogramaRepository();
  ICloudCronogramaRepository _cloudRepository = MockCloudCronogramaRepository();
  ISecurityService _securityService = MockSecurityService();
  ICloudSearchCronogramaRepository _cloudSearchRepository =
      MockCloudSearchRepository();

  setUp(() {
    initModules([
      DiaCronogramaModule(),
      AppModule()
    ], changeBinds: [
      Bind<IDiaCronogramaRepository>((_) => repository),
      Bind<IMeuCronogramaRepository>((_) => meuRepository),
      Bind<ICloudCronogramaRepository>((_) => _cloudRepository),
      Bind<ICloudSearchCronogramaRepository>((_) => _cloudSearchRepository),
      Bind<ISecurityService>((_) => _securityService),
    ]);
    controller = DiaCronogramaModule.to.get<DiaCronogramaController>();
  });

  test("First Test", () {
    expect(DiaCronogramaModule.to.get<IDiaCronogramaRepository>(),
        isInstanceOf<MockDiaCronogramaRepository>());
    expect(DiaCronogramaModule.to.get<ICloudCronogramaRepository>(),
        isInstanceOf<MockCloudCronogramaRepository>());
    expect(AppModule.to.get<IMeuCronogramaRepository>(),
        isInstanceOf<MockMeuCronogramaRepository>());
    expect(controller, isInstanceOf<DiaCronogramaController>());
  });

  group('DiaCronogramaController Test', () {
    setUp(() {
      controller.diasCronograma.clear();
    });

    test("get values", () {
      controller.diasCronograma.add(DiaCronograma(
          day: 1,
          id: 1,
          priceDay: 25,
          qtdActivities: 5,
          typeFirstActivity: 'Park'));
      expect(controller.diasCronograma.length, 1);
    });

    test("find all dias cronogramas", () async {
      var cronograma = Cronograma();
      cronograma.scheduleCodAPI = 21;
      when(_cloudSearchRepository.findDiasCronogramas(21))
          .thenAnswer((realInvocation) async => ApiResponse.completed([
                DiaCronograma(
                    day: 1,
                    id: 1,
                    priceDay: 25,
                    qtdActivities: 5,
                    typeFirstActivity: 'Park')
              ]));
      var diasCronogramas = await controller.findAll(cronograma);
      expect(diasCronogramas.data[0].day, 1);
    });

    test("find all dias cronogramas owner", () async {
      var cronograma = Cronograma(userUID: "1234X");
      cronograma.scheduleCod = 11;
      when(_securityService.getSessionUser()).thenAnswer((_) => SessionUser(
          uid: "1234X", displayName: "", tokenMessage: "", email: ""));
      controller.findAll(cronograma);
      verify(repository.findAll(11));
    });

    test("find all dias cronogramas owner and publish", () async {
      var cronograma = Cronograma(userUID: "1234X");
      cronograma.scheduleCodAPI = 41;
      cronograma.isPublish = true;
      when(_securityService.getSessionUser()).thenAnswer((_) => SessionUser(
          uid: "1234X", displayName: "", tokenMessage: "", email: ""));
      controller.findAll(cronograma);
      verify(repository.findAll(41, codFromApi: true));
    });

    test("Should delete internal dia cronograma", () {
      controller.diasCronograma.add(DiaCronograma(
          day: 1,
          id: 1,
          priceDay: 25,
          qtdActivities: 5,
          typeFirstActivity: 'Park'));
      expect(controller.diasCronograma.length, 1);
      controller.deleteInternal(5);
      expect(controller.diasCronograma.length, 1);
      controller.deleteInternal(1);
      expect(controller.diasCronograma.length, 0);
    });

    test("Should delete dia cronograma in repository", () {
      controller.deleteDiaCronograma(1, Cronograma());
      verify(repository.remove(1, any, any));
    });

    test("Should delete cronograma in local repository", () {
      var cronograma = Cronograma();
      cronograma.scheduleCod = 25;
      controller.deleteCronograma(cronograma);
      verify(meuRepository.remove(25));
    });

    test("Should delete cronograma in cloud repository", () {
      var cronograma = Cronograma();
      cronograma.scheduleCodAPI = 88;
      controller.deleteCronograma(cronograma);
      verify(_cloudRepository.removeCronograma(cronograma));
    });

    test("Should create new dia cronograma", () {
      var cronograma = Cronograma();
      cronograma.scheduleCod = 21;
      when(repository.createDiaCronograma(any, any, 21))
          .thenAnswer((_) async => 1);
      var diaCronograma = controller.createNewDiaCronograma(cronograma);
      expect(diaCronograma.day, 1);
    });

    test("Should create new dia cronograma", () {
      var cronograma = Cronograma();
      controller.publishCronograma(cronograma);
      verify(_cloudRepository.publish(cronograma));
    });
  });
}
