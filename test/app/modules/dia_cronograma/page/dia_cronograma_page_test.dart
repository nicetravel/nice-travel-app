import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/button_bar_schedule_day.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/carousel_travel_photos.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/dia_cronograma_list.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import '../../../mocks/firebase_mock.dart';
import '../../../mocks/mock.dart';

class DiaCronogramaControllerMock extends Mock
    implements DiaCronogramaController {}

void main() {

  setupFirebaseAuthMocks();

  var controller = DiaCronogramaControllerMock();
  initModules([AppModule(), DiaCronogramaModule()],
      changeBinds: [Bind<DiaCronogramaController>((_) => controller)]);

  setUp(() {
    var diasCronogramas =
        ApiResponse.completed([DiaCronograma.newInstance(1, createCronograma())]);
    when(controller.findAll(any))
        .thenAnswer((realInvocation) async => diasCronogramas);
    when(controller.showAnimation).thenAnswer((realInvocation) => false);
    when(controller.diasCronograma)
        .thenAnswer((realInvocation) => diasCronogramas.data);
  });

  test("Test Mock", () {
    expect(DiaCronogramaModule.to.get<DiaCronogramaController>(),
        isInstanceOf<DiaCronogramaControllerMock>());
  });

  testWidgets('Should find DayScheduleList and ButtonBarScheduleDay ',
      (tester) async {
    when(controller.hasVoted).thenAnswer((_) => true);
    final Cronograma cronograma = createCronograma();
    await tester.pumpWidget(
        buildTestableWidget(DiaCronogramaPage(cronograma: cronograma)));
    final buttonBarScheduleFinder = find.byType(ButtonBarScheduleDay);
    expect(buttonBarScheduleFinder, findsOneWidget);
    await tester.pump();

  });
  testWidgets('Should find DayScheduleList and DayScheduleList ',
      (tester) async {
    when(controller.hasVoted).thenAnswer((_) => true);
    final Cronograma cronograma = createCronograma();
    await tester.pumpWidget(
        buildTestableWidget(DiaCronogramaPage(cronograma: cronograma)));
    final dayScheduleFinder = find.byType(DayScheduleList);
    expect(dayScheduleFinder, findsOneWidget);
    await tester.pump();
  });


  testWidgets('Should find DayScheduleList and CarouselTravelPhotos ',
      (tester) async {
    when(controller.hasVoted).thenAnswer((_) => true);
    final Cronograma cronograma = createCronograma();
    await tester.pumpWidget(
        buildTestableWidget(DiaCronogramaPage(cronograma: cronograma)));
    final carouselTravelPhotos = find.byType(CarouselTravelPhotos);
    expect(carouselTravelPhotos, findsOneWidget);
    await tester.pump();
  });
}
