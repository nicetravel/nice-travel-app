import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/config/local_database.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_local_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:sqflite/sqflite.dart';

import '../../../mocks/mock.dart';

void main() {
  IDiaCronogramaRepository repository;
  IDatabase mockLocalDatabase;
  Database mockDatabase;

  setUp(() {
    mockDatabase = MockDatabase();
    mockLocalDatabase = MockLocalDatabase();
    repository = DiaCronogramaLocalRepository(mockLocalDatabase);
  });

  group('DiaCronogramaRepository Test', () {
     test("First Test", () {
       expect(repository, isInstanceOf<DiaCronogramaLocalRepository>());
     });

     setUp(() {
       when(mockLocalDatabase.database).thenAnswer((_) async => mockDatabase);
       when(mockLocalDatabase.fetchDiasCronograma(any)).thenAnswer((_) async => []);
     });

     test('Should list all dias Cronogramas by scheduleCod', () async {
       when(mockLocalDatabase.fetchDiasCronograma(any)).thenAnswer((_) async => [
         DiaCronograma.fromMap({'CO_SCHEDULE_DAY': 1, 'NU_DAY': 1, 'CO_SCHEDULE_TRAVEL': 1, 'qtdActivities': null, 'priceDay': null}),
         DiaCronograma.fromMap({'CO_SCHEDULE_DAY': 2, 'NU_DAY': 2, 'CO_SCHEDULE_TRAVEL': 1, 'qtdActivities': null, 'priceDay': null}),
         DiaCronograma.fromMap({'CO_SCHEDULE_DAY': 3, 'NU_DAY': 3, 'CO_SCHEDULE_TRAVEL': 1, 'qtdActivities': null, 'priceDay': null}),
       ]);

       ApiResponse<List<DiaCronograma>> diasCronogramas = await repository.findAll(1);
       expect(diasCronogramas.data.length, 3);
     });

     test(
         'should get error when try to alter local repository list'
             ' (Must be immutable)', () async {
       await repository.createDiaCronograma(DiaCronograma(day: 1, scheduleCod: 1), 1 ,1);
       ApiResponse<List<DiaCronograma>> diasCronogramas = await repository.findAll(1);
       expect(() => diasCronogramas.data.removeLast(),
           throwsA(isA<UnsupportedError>()));
     });

     test('should create and delete meu dia Cronograma ', () async {
       var id = await repository.createDiaCronograma(DiaCronograma(day: 1, scheduleCod: 1), 1 ,1);
       ApiResponse<List<DiaCronograma>> diasCronogramas = await repository.findAll(1);
       expect(diasCronogramas.data.length, 1);
       await repository.remove(id,1,1);

       diasCronogramas = await repository.findAll(1);
       expect(diasCronogramas.data.length, 0);
     });

     test("reorder Schedule Value", () async {

       when(mockDatabase.insert(LocalDatabase.TB_SCHEDULE_DAY, {'NU_DAY': 1, 'CO_SCHEDULE_TRAVEL': 1})).thenAnswer((_) => Future.value(1));
       when(mockDatabase.insert(LocalDatabase.TB_SCHEDULE_DAY, {'NU_DAY': 2, 'CO_SCHEDULE_TRAVEL': 1})).thenAnswer((_) => Future.value(2));

       await repository.createDiaCronograma(DiaCronograma(day: 1, scheduleCod: 1), 1 ,1);
       await repository.createDiaCronograma(DiaCronograma(day: 2, scheduleCod: 1), 1 ,1);
       ApiResponse<List<DiaCronograma>> diasCronogramas = await repository.findAll(1);
       expect(diasCronogramas.data[0].id, 1);

       repository.reorderDays(1,2);

       diasCronogramas = await repository.findAll(1);
       expect(diasCronogramas.data[0].id, 2);
     });

  });

}
