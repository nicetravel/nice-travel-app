import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/buttons/republish_button.dart';

import '../../page/dia_cronograma_page_test.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  var controller = DiaCronogramaControllerMock();
  setUpAll(() {
    Modular.init(AppModule());
    initModules([AppModule(), DiaCronogramaModule(), AtividadesModule()],
        changeBinds: [Bind<DiaCronogramaController>((_) => controller)]);
  });


  testWidgets('Should RePublish Cronograma', (tester) async {
    Cronograma cronograma = Cronograma(
        userName: 'A',
        scheduleCod: 1,
        qtdDays: 5,
        imagesUrl: ['ab'],
        cityAddress: 'a',
        userUID: '123');
    when(controller.rePublishCronograma(any)).thenAnswer((_) async => cronograma);

    await tester.pumpWidget(buildTestableWidget(
      Scaffold(body: RePublishButton(cronograma)),
    ));
    expect(cronograma.isPublish, false);
    final buttonFinder = find.byType(MaterialButton);
    expect(buttonFinder, findsOneWidget);

    await tester.tap(buttonFinder);
    await tester.pumpAndSettle();

    final titleText = find.text("Deseja republicar esse Cronograma?");
    expect(titleText, findsOneWidget);
    await tester.tap(find.text("OK"));
    await tester.pumpAndSettle();

    expect(titleText, findsNothing);
    expect(cronograma.isPublish, true);
  });
}
