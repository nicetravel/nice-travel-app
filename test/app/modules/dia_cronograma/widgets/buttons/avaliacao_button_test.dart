import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/avaliacoes_bar_chart.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/avaliacoes_list.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/buttons/avaliar_button.dart';

import '../../../../mocks/firebase_mock.dart';
import '../../../../mocks/mock.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  setupFirebaseAuthMocks();

  var security = MockSecurityService();
  var client = MockClient();

  FlavorConfig(
    name: "TEST",
    variables: {
      "url_api": 'teste',
    },
  );

  setUpAll(() {
    initModules([
      AppModule(),
      DiaCronogramaModule(),
    ], changeBinds: [
      Bind<ISecurityService>((_) => security),
      Bind<Dio>((_) => client)
    ]);
  });

  group("Test Configuration", () {
    test('Test Mocks', () {
      expect(Modular.get<ISecurityService>(), isA<MockSecurityService>());

      expect(Modular.get<Dio>(), isA<MockClient>());
    });
  });

  group("Test Vote Button", () {
    var cronograma;
    var user;
    setUp(() {
      cronograma = Cronograma(
          userName: 'A',
          scheduleCod: 1,
          qtdDays: 5,
          imagesUrl: ['ab'],
          cityAddress: 'a',
          userUID: '123');
      user = SessionUser(
          displayName: null, email: null, uid: "999", tokenMessage: null);
      when(security.getSessionUser()).thenAnswer((_) => user);

      when(client.get(any, options: anyNamed("options"))).thenAnswer(
          (_) async =>
              Response(requestOptions: null, data: [], statusCode: 200));
    });

    testWidgets('Should Include one Star Cronograma', (tester) async {
      when(security.isLoggedIn()).thenAnswer((realInvocation) => false);
      when(client.patch(any,
          data: anyNamed("data"), options: anyNamed("options")))
          .thenAnswer((_) async =>
          Response(requestOptions: null, data: true, statusCode: 200));

      await tester.pumpWidget(buildTestableWidget(
        Scaffold(body: AvaliarButton(cronograma: cronograma)),
      ));
      expect(cronograma.numberLike, 0);
      await tester.tap(find.byType(MaterialButton));
      await tester.pump();
      await tester.pumpAndSettle();
      expect(find.byType(AvaliacoesList), findsOneWidget);
      expect(find.byKey(Key("avaliar_button")), findsOneWidget);
      await tester.tap(find.byKey(Key("avaliar_button")));
      await tester.pump();
      await tester.pumpAndSettle();
      expect(cronograma.numberLike, 1);
    });


    testWidgets('Should not include avaliacoes ', (tester) async {
      when(security.isLoggedIn()).thenAnswer((realInvocation) => false);
      when(client.patch(any,
              data: anyNamed("data"), options: anyNamed("options")))
          .thenAnswer((_) async =>
              Response(requestOptions: null, data: false, statusCode: 200));

      await tester.pumpWidget(buildTestableWidget(
        Scaffold(body: AvaliarButton(cronograma: cronograma)),
      ));

      expect(cronograma.numberLike, 0);
      await tester.tap(find.byType(MaterialButton));
      await tester.pump();
      await tester.pumpAndSettle();
      expect(find.byType(AvaliacoesList), findsOneWidget);
      expect(find.byKey(Key("avaliar_button")), findsOneWidget);
      await tester.tap(find.byKey(Key("avaliar_button")));
      await tester.pump();
      await tester.pumpAndSettle();
      expect(cronograma.numberLike, 0);
    });

    testWidgets('Should Contains List Commentaries ', (tester) async {
      when(client.get(any, options: anyNamed("options"))).thenAnswer(
          (_) async => Response(
              requestOptions: null,
              data: getCommentariesJson(),
              statusCode: 200));

      await tester.pumpWidget(buildTestableWidget(
        Scaffold(body: AvaliarButton(cronograma: cronograma)),
      ));

      await tester.tap(find.byType(MaterialButton));
      await tester.pump();
      await tester.pumpAndSettle();
      expect(find.byType(AvaliacoesList), findsOneWidget);
      expect(find.byType(AvaliacoesBarChart), findsOneWidget);
    });
  });
}

List<Map<String, Object>> getCommentariesJson() {
  return [
    {
      "qtdStar": 4,
      "description": "Bom, recomendado.\\n Vale a pena ver de novo.",
      "date": [2021, 4, 21]
    },
    {
      "qtdStar": 5,
      "description": "Ótimo roteiro.",
      "date": [2021, 4, 21]
    },
    {
      "qtdStar": 4.5,
      "description": "top",
      "date": [2021, 4, 21]
    },
    {
      "qtdStar": 4,
      "description": "Bom, recomendo",
      "date": [2021, 4, 21]
    },
    {
      "qtdStar": 4,
      "description": "Bom, recomendo",
      "date": [2021, 4, 21]
    },
    {
      "qtdStar": 4,
      "description":
          "Bom, recomendo. As opções de almoço e jantar são excelentes, vale a pena seguir esse cronograma.",
      "date": [2021, 4, 21]
    },
    {
      "qtdStar": 4,
      "description":
          "Bom, recomendo. As opções de almoço e jantar são excelentes. Muit bom mesmo. vale a pena seguir esse cronograma.",
      "date": [2021, 4, 21]
    }
  ];
}
