import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/exception/api_exception.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/buttons/remove_button.dart';

import '../../page/dia_cronograma_page_test.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  var controller = DiaCronogramaControllerMock();
  setUpAll(() {
    Modular.init(AppModule());
    initModules([AppModule(), DiaCronogramaModule(), AtividadesModule()],
        changeBinds: [Bind<DiaCronogramaController>((_) => controller)]);
  });


  testWidgets('Should Remove Cronograma', (tester) async {
    Cronograma cronograma = Cronograma(
        userName: 'A',
        scheduleCod: 1,
        qtdDays: 5,
        imagesUrl: ['ab'],
        cityAddress: 'a',
        userUID: '123');
    when(controller.deleteCronograma(any)).thenAnswer((_) async => cronograma);

    await tester.pumpWidget(buildTestableWidget(
      Scaffold(body: RemoveButton(cronograma)),
    ));
    expect(cronograma.removed, false);
    await tester.tap(find.byType(MaterialButton));
    await tester.pump();
    await tester.pumpAndSettle();

    final titleText = find.text("Deseja remover esse cronograma?");
    expect(titleText, findsOneWidget);
    await tester.tap(find.text("OK"));
    await tester.pumpAndSettle();
    expect(titleText, findsNothing);
    expect(cronograma.removed, true);
  });

  testWidgets('Should Show error when Remove Cronograma', (tester) async {
    Cronograma cronograma = Cronograma(
        userName: 'A',
        scheduleCod: 1,
        qtdDays: 5,
        imagesUrl: ['ab'],
        cityAddress: 'a',
        userUID: '123');
    when(controller.deleteCronograma(any))
        .thenAnswer((_) async => throw BadRequestException("erro"));

    await tester.pumpWidget(buildTestableWidget(
      Scaffold(body: RemoveButton(cronograma)),
    ));
    expect(cronograma.removed, false);

    await tester.tap(find.byType(MaterialButton));
    await tester.pumpAndSettle();
    await tester.tap(find.text("OK"));
    await tester.pumpAndSettle();

    final titleText =
        find.text("Erro ao deletar o cronograma, tente novamente mais tarde.");
    expect(titleText, findsOneWidget);
    await tester.tap(find.widgetWithText(AnimatedButton, "Ok"));
    await tester.pumpAndSettle();
    expect(titleText, findsNothing);
    expect(cronograma.removed, false);
  });
}
