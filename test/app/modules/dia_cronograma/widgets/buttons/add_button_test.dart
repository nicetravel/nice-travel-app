import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/buttons/add_button.dart';

import '../../../../mocks/firebase_mock.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {

  setupFirebaseAuthMocks();

  setUpAll(() {
    Modular.init(AppModule());
    initModules([AppModule(), DiaCronogramaModule(), AtividadesModule()]);
  });

  testWidgets('AddButon action', (tester) async {
    Cronograma cronograma = Cronograma(
        userName: 'A',
        scheduleCod: 1,
        qtdDays: 5,
        imagesUrl: ['ab'],
        cityAddress: 'a',
        userUID: '123');

    await tester.pumpWidget(buildTestableWidget(
      Scaffold(
          body: AddButton(
        cronograma: cronograma,
      )),
    ));
    final dayScheduleFinder = find.byType(MaterialButton);
    expect(dayScheduleFinder, findsOneWidget);

    await tester.tap(dayScheduleFinder);
    await tester.pump();

    final atividadePageFinder = find.byType(Scaffold);
    expect(atividadePageFinder, findsOneWidget);
  });
}
