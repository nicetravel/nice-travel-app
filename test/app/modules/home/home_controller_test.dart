import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/home/page/home_controller.dart';
import 'package:nice_travel/app/shared/api_response.dart';

import '../../mocks/mock.dart';

class MockCronogramaRepository extends Mock implements ICloudSearchCronogramaRepository {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  var securityMock = MockSecurityService();
  MockCronogramaRepository repository = MockCronogramaRepository();

  initModules([
    AppModule(),
    HomeModule()
  ], changeBinds: [
    Bind<ICloudSearchCronogramaRepository>((_) => repository),
    Bind<ISecurityService>((_) => securityMock),
  ]);
  HomeController controller = HomeModule.to.get<HomeController>();

  group('HomeController Test', () {
    test("First Test", () {
      expect(AppModule.to.get<ICloudSearchCronogramaRepository>(),
          isInstanceOf<MockCronogramaRepository>());
    });
  });

  group('DiaCronogramaController Test', () {
    setUp(() {
      controller.cronogramas.clear();
      when(securityMock.getSessionUser()).thenAnswer((realInvocation) =>
      new SessionUser(
          displayName: null, email: null, uid: "userUID", tokenMessage: null));
    });

    test("First Test", () {
      expect(controller, isInstanceOf<HomeController>());
    });

    test("Should fill cronogramas list", () {
      final List<Cronograma> cronogramas = getCronogramas();
      controller.fillCronogramas(cronogramas);
      expect(controller.cronogramas.length, 2);
    });

    test("Should call repository API", () async {
      var placeId = "placeID13";
      when(repository.findCronogramas(placeId, any))
          .thenAnswer((_) async => ApiResponse.completed(getCronogramas()));
      ApiResponse<List<Cronograma>> apiResponse =
          await controller.findAll(placeId);
      expect(apiResponse.data.length, 2);
    });

    test("Should change state refresh api can be called", () {
      controller.setPlaceId("placeID");
      expect(controller.filteredChanged(), true);
      controller.fillCronogramas([]);
      expect(controller.filteredChanged(), false);
      controller.setPlaceId("placeID");
      expect(controller.filteredChanged(), false);
      controller.setPlaceId("placeID000");
      expect(controller.filteredChanged(), true);
    });
  });
}

List<Cronograma> getCronogramas() {
  var cronogramas = [
    Cronograma(
      scheduleCod: 1,
      userName: 'userName',
      userUID: 'userUID',
      qtdDays: 5,
      cityAddress: 'cityAddress',
    ),
    Cronograma(
      scheduleCod: 2,
      userName: 'userName2',
      userUID: 'userUID',
      qtdDays: 2,
      cityAddress: 'cityAddress2',
    )
  ];
  return cronogramas;
}
