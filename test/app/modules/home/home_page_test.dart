import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/home/page/home_page.dart';

import '../../mocks/firebase_mock.dart';

void main() {
  initModules([AppModule(), HomeModule()]);
  setupFirebaseAuthMocks();


  testWidgets('HomePage has bottom bar', (tester) async {
    await tester.pumpWidget(buildTestableWidget(HomePage()));
    final drawerFinder = find.byIcon(Icons.date_range);
    expect(drawerFinder, findsOneWidget);
  });

}
