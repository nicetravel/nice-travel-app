import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/login/login_module.dart';
import 'package:nice_travel/app/modules/login/page/login_page.dart';

import '../../../mocks/firebase_mock.dart';

void main() {
  initModules([AppModule(), HomeModule(), LoginModule()]);
  setupFirebaseAuthMocks();

  setUpAll(() async {
    await Firebase.initializeApp();
  });

  testWidgets('LoginPage has Google Button', (tester) async {
     await tester.pumpWidget(buildTestableWidget(LoginPage()));
     tester.tap(find.byIcon(MdiIcons.google));
  });
}
