import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/login/login_module.dart';
import 'package:nice_travel/app/modules/login/page/login_controller.dart';

import '../../../mocks/mock.dart';

class MockUser extends Mock implements User {}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  MockSecurityService mockSecurityClient = new MockSecurityService();
  initModules([AppModule(), LoginModule()],
      changeBinds: [Bind<ISecurityService>((_) => mockSecurityClient)]);
  LoginController login;

  setUp(() {
    login = LoginModule.to.get<LoginController>();
  });

  group('LoginController Test', () {
    test("Test Configuration", () {
      expect(login, isInstanceOf<LoginController>());
      expect(mockSecurityClient, isInstanceOf<ISecurityService>());
    });

    test("Test SigIn", () {
      var mockUser = new MockUser();
      login.signIn(mockUser);
      verify(mockSecurityClient.signIn(any));
    });
  });
}
