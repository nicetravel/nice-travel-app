import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class LocalDatabase extends IDatabase {
  static const String TB_SCHEDULE_TRAVEL = "TB_SCHEDULE_TRAVEL";
  static const String TB_PLACE_PHOTO = "TB_PLACE_PHOTO";
  static const String TB_SCHEDULE_DAY = "TB_SCHEDULE_DAY";
  static const String TB_ACTIVITY = "TB_ACTIVITY";

  static Database _database;
  static bool _synchronized = false;

  final ICloudSearchCronogramaRepository _cronogramaSearchRepository;

  LocalDatabase(this._cronogramaSearchRepository);

  @override
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "NiceTravel.db");
    // Delete the database -> await deleteDatabase(path);
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE $TB_SCHEDULE_TRAVEL ("
          "CO_SCHEDULE_TRAVEL INTEGER PRIMARY KEY AUTOINCREMENT,"
          "NU_LIKE INTEGER,"
          "ST_PUBLIC_ACCESS TEXT,"
          "DS_PLACE TEXT,"
          "DS_PLACE_STATE TEXT,"
          "CO_PLACE_ID TEXT,"
          "CO_USER TEXT,"
          "DS_USER TEXT,"
          "DS_USER_EMAIL TEXT,"
          "NU_DAYS INTEGER,"
          "CO_SCHEDULE_TRAVEL_API INTEGER"
          ")");
      await db.execute("CREATE TABLE TB_PLACE_PHOTO ("
          "CO_PLACE_PHOTO INTEGER PRIMARY KEY AUTOINCREMENT,"
          "CO_SCHEDULE_TRAVEL INTEGER,"
          "DS_IMAGE_URL TEXT"
          ")");

      await db.execute("CREATE TABLE TB_SCHEDULE_DAY ("
          "CO_SCHEDULE_DAY INTEGER PRIMARY KEY AUTOINCREMENT,"
          "NU_DAY INTEGER,"
          "CO_SCHEDULE_TRAVEL INTEGER,"
          "CO_SCHEDULE_DAY_API INTEGER"
          ")");

      await db.execute("CREATE TABLE TB_ACTIVITY ("
          "CO_ACTIVITY INTEGER PRIMARY KEY AUTOINCREMENT,"
          "DS_DESCRIBE TEXT,"
          "DT_END TEXT,"
          "DT_START TEXT,"
          "DS_NAME TEXT,"
          "VL_PRICE DOUBLE,"
          "DS_TYPE_ACTIVITY TEXT,"
          "CO_SCHEDULE_DAY INTEGER,"
          "CO_ACTIVITY_API INTEGER"
          ")");
    });
  }

  Future<List<DiaCronograma>> fetchDiasCronograma(int scheduleCod,
      {bool codFromApi = false}) async {
    var whereQuery = codFromApi
        ? " WHERE travel.CO_SCHEDULE_TRAVEL_API = $scheduleCod"
        : " WHERE travel.CO_SCHEDULE_TRAVEL  = $scheduleCod";
    var currentDatabase = await database;
    var elements = await currentDatabase.rawQuery(
        'SELECT distinct travel_day.*, '
        ' activity.qtdActivities as qtdActivities,'
        ' activity.priceDay as priceDay,'
        ' activityType.typeFirstActivity as typeFirstActivity'
        ' FROM ${LocalDatabase.TB_SCHEDULE_DAY} travel_day '
        ' inner join  ${LocalDatabase.TB_SCHEDULE_TRAVEL} travel on travel.CO_SCHEDULE_TRAVEL = travel_day.CO_SCHEDULE_TRAVEL'
        ' left join '
        ' ( SELECT count(activity.CO_SCHEDULE_DAY) as qtdActivities, sum(activity.VL_PRICE) as priceDay, min(activity.CO_ACTIVITY) as CO_firstActivity,  activity.CO_SCHEDULE_DAY '
        ' FROM ${LocalDatabase.TB_ACTIVITY} activity GROUP BY activity.CO_SCHEDULE_DAY ) activity'
        '  on activity.CO_SCHEDULE_DAY = travel_day.CO_SCHEDULE_DAY '
        ' left join '
        ' ( SELECT activityType.DS_TYPE_ACTIVITY typeFirstActivity, activityType.CO_ACTIVITY FROM ${LocalDatabase.TB_ACTIVITY} activityType ) activityType'
        '  on activity.CO_firstActivity = activityType.CO_ACTIVITY '
        ' $whereQuery ');

    if (elements != null) {
      var diasCronogramas =
          elements.map<DiaCronograma>((e) => DiaCronograma.fromMap(e)).toList();
      diasCronogramas.sort((a, b) => a.day.compareTo(b.day));
      return diasCronogramas;
    }
    return [];
  }

  Future<List<Atividade>> fetchAtividades(int scheduleDayId) async {
    var currentDatabase = await database;
    var elements = await currentDatabase.rawQuery('SELECT atividade.* '
        ' FROM ${LocalDatabase.TB_ACTIVITY} atividade '
        ' where atividade.CO_SCHEDULE_DAY = $scheduleDayId');
    if (elements != null) {
      var atividades =
          elements.map<Atividade>((e) => Atividade.fromMap(e)).toList();
      return atividades;
    }
    return [];
  }

  Future<bool> synchronizedFromApi(SessionUser sessionUser) async {
    if (!_synchronized) {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.wifi) {
        var apiResponse = await _cronogramaSearchRepository
            .findCronogramasByUser(sessionUser.uid);
        if (apiResponse.status == Status.COMPLETED) {
          List<Cronograma> localCronogramas = [];
          var localDatabase = await database;
          var elements = await localDatabase.rawQuery('SELECT * '
              ' FROM ${LocalDatabase.TB_SCHEDULE_TRAVEL} ');
          if (elements != null) {
            localCronogramas =
                elements.map<Cronograma>((e) => Cronograma.fromMap(e)).toList();
          }
          Iterable<Cronograma> cronogramasNaoSincronizados;
          if (localCronogramas.isEmpty) {
            cronogramasNaoSincronizados = apiResponse.data;
          } else {
            cronogramasNaoSincronizados = apiResponse.data.where(
                (cronogramaAPI) =>
                    localCronogramas.firstWhere(
                        (c) => c.scheduleCodAPI == cronogramaAPI.scheduleCodAPI,
                        orElse: () => null) ==
                    null);
          }
          if (cronogramasNaoSincronizados.isNotEmpty) {
            await _syncNewCronogramas(cronogramasNaoSincronizados);
          }
          updateStars(apiResponse.data);
          _synchronized = true;
          return true;
        }
      }
    }
    return false;
  }

  updateStars(List<Cronograma> cronogramas) async {
    await Future.forEach(
        cronogramas,
        (cronograma) async => await _database.execute(
            'UPDATE  ${LocalDatabase.TB_SCHEDULE_TRAVEL} SET NU_LIKE = ? WHERE CO_SCHEDULE_TRAVEL_API =:cod ',
            [cronograma.numberLike, cronograma.scheduleCodAPI]));
  }

  Future _syncNewCronogramas(
      Iterable<Cronograma> cronogramasNaoSincronizados) async {
    await Future.forEach(cronogramasNaoSincronizados,
        (cronograma) async => await _addCronogramaLocal(cronograma));
  }

  _addCronogramaLocal(Cronograma cronograma) async {
    var id = await _database.insert(
        LocalDatabase.TB_SCHEDULE_TRAVEL, cronograma.toMap());
    cronograma.imagesUrl
        .forEach((imageUrl) async => await _addImageLocal(id, imageUrl));

    var response = await _cronogramaSearchRepository
        .findDiasCronogramas(cronograma.scheduleCodAPI);
    if (response.status == Status.COMPLETED) {
      response.data.forEach((dia) => _addDiaCronogramaLocal(dia, id));
    }
  }

  Future _addImageLocal(int id, imageUrl) async {
    Map<String, dynamic> map = {
      'CO_SCHEDULE_TRAVEL': id,
      'DS_IMAGE_URL': imageUrl,
    };
    await _database.insert(LocalDatabase.TB_PLACE_PHOTO, map);
  }

  _addDiaCronogramaLocal(DiaCronograma dia, int cronogramaId) async {
    dia.scheduleCod = cronogramaId;
    var id = await _database.insert(LocalDatabase.TB_SCHEDULE_DAY, dia.toMap());
    var response =
        await _cronogramaSearchRepository.findAllAtividades(dia.idApi);
    if (response.status == Status.COMPLETED) {
      response.data.forEach((atividade) => _addAtividadeLocal(atividade, id));
    }
  }

  void _addAtividadeLocal(Atividade atividade, int id) {
    atividade.idScheduleDay = id;
    _database.insert(LocalDatabase.TB_ACTIVITY, atividade.toMap());
  }

  @override
  void dispose() {
    //dispose will be called automatically
  }
}
