import 'dart:developer' as util;

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/exception/networking_exception.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';

class ErrorLogger {
  void log(String message, String exception) {
    util.log("Treated Error");
    util.log("--------------------------------");
    util.log("Error :  $message");
    util.log("ExceptionMessage :  $exception");
  }

  void logInternalFlutterError(Object error) async {
    final ISecurityService _securityService =
        AppModule.to.get<ISecurityService>();
    util.log("Flutter Internal Error");
    util.log("--------------------------------");
    util.log("Error :  $error");
    if (error is InternalError) {
      util.log("ExceptionMessage :  ${error.exceptionMessage}");
    }
    var sessionUser = _securityService.getSessionUser();
    await FirebaseCrashlytics.instance
        .setUserIdentifier(sessionUser?.email ?? 'anonymous');

    FirebaseCrashlytics.instance.recordFlutterError(error);
  }

  void logInternal(Object error, StackTrace stackTrace) {
    util.log("Internal Error");
    util.log("--------------------------------");
    util.log("Error :  $error");
    if (error is InternalError) {
      util.log("ExceptionMessage :  ${error.exceptionMessage}");
    }
    util.log("StackTrace :  $stackTrace");
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  }
}
