
import 'package:dio/dio.dart';
import 'package:nice_travel/app/config/api/api_connection_helper.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';

class DiaCronogramaApiConnection {
  static final path = '/schedulesDay/';

  DiaCronogramaApiConnection._();

  static final DiaCronogramaApiConnection instance =
      new DiaCronogramaApiConnection._();

  Future<List<DiaCronograma>> getDiaCronogramaByCodSchedule(
      Dio dio, int codSchedule) async {
    final url =
        ApiConnectionHelper.urlApi + path + "$codSchedule";
    return _formatFutureToSchedulesDay(await ApiConnectionHelper.get(dio, url));
  }

  List<DiaCronograma> _formatFutureToSchedulesDay(responseResult) {
    return _formatSchedulesDayJson(responseResult);
  }

  List<DiaCronograma> _formatSchedulesDayJson(json) {
    if (json != null) {
      return json
          .map<DiaCronograma>((e) => DiaCronograma.fromMapApi(e))
          .toList();
    }
    return [];
  }
}
