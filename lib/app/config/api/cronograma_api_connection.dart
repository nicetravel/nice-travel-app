import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:nice_travel/app/config/api/api_connection_helper.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/cronograma_publish_response.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/duplicated_cronograma_api_resource.dart';

class CronogramaAPIConnection {
  static final path = '/schedules/';

  CronogramaAPIConnection._();

  static final CronogramaAPIConnection instance =
      new CronogramaAPIConnection._();

  Future<List<Cronograma>> getScheduleByCityPlaceID(Dio dio, String placeID,
      {int sizeElements = 5}) async {
    final url = ApiConnectionHelper.urlApi +
        path +
        "city?sizeElements=$sizeElements"
            "${placeID != null ? "&placeID=$placeID" : ""}";
    return _formatFutureToSchedules(await ApiConnectionHelper.get(dio, url));
  }

  Future<List<Cronograma>> findCronogramasByUser(Dio dio, String userId) async {
    final url = ApiConnectionHelper.urlApi +
        path + "user/$userId";
    return _formatFutureToSchedules(await ApiConnectionHelper.get(dio, url));
  }

  Future<List<Cronograma>> getSchedulesByIds(
      Dio dio, List<int> scheduleIds) async {
    String ids = '';
    scheduleIds..forEach((id) => ids += '&scheduleIds=$id');
    final url = ApiConnectionHelper.urlApi + path + 'ids?1=1$ids';
    return _formatFutureToSchedules(await ApiConnectionHelper.get(dio, url));
  }

  Future<CronogramaPublishResponse> publishSchedule(
      Dio dio, String json) async {
    final url = ApiConnectionHelper.urlApi + path;
    return _formatToPublishSchedule(
        await ApiConnectionHelper.postData(dio, url, json));
  }

  Future<CronogramaPublishResponse> rePublishSchedule(int scheduleApiId,
      Dio dio, String json) async {
    final url = ApiConnectionHelper.urlApi + path + "republish/$scheduleApiId";
    return _formatToPublishSchedule(
        await ApiConnectionHelper.postData(dio, url, json));
  }

  Future<DuplicatedCronogramaAPI> duplicateSchedule(
      Dio dio, int codSchedule, SessionUser sessionUser) async {
    final url = ApiConnectionHelper.urlApi + path + "duplicate";

    var json = jsonEncode({
      "scheduleId": codSchedule,
      "userUID": sessionUser.uid,
      "userEmail": sessionUser.email,
      "userName": sessionUser.displayName
    });
    return _formatToDuplicatedSchedule(
        await ApiConnectionHelper.postData(dio, url, json));
  }

  Future<bool> avaliar(
      Dio dio, int codSchedule, SessionUser sessionUser, String commentary, double qtdStar) async {
    final url = ApiConnectionHelper.urlApi + path + "commentary";

    var json = jsonEncode({
      "scheduleId": codSchedule,
      "userUID": sessionUser.uid,
      "email": sessionUser.email,
      "displayName": sessionUser.displayName,
      "commentary": commentary,
      "qtdStar": qtdStar,
    });
    return await ApiConnectionHelper.patchData(dio, url, json);
  }


  Future<List<Avaliacao>> findAllAvaliacoes(Dio dio, int scheduleId, {int sizeElements = 10}) async {
    final url = ApiConnectionHelper.urlApi +
        path + "commentaries?scheduleId=$scheduleId&sizeElements=$sizeElements";
    return _formatFutureToAvaliacao(await ApiConnectionHelper.get(dio, url));
  }

  Future<dynamic> deleteSchedule(Dio dio, int scheduleCod) {
    final url = ApiConnectionHelper.urlApi + path + "$scheduleCod";
    return ApiConnectionHelper.delete(dio, url);
  }

  List<Cronograma> _formatFutureToSchedules(responseResult) {
    return _formatSchedulesJson(responseResult);
  }

  List<Avaliacao> _formatFutureToAvaliacao(responseResult) {
    return _formatAvaliacoesJson(responseResult);
  }

  List<Cronograma> _formatSchedulesJson(json) {
    if (json != null) {
      return json.map<Cronograma>((e) => Cronograma.fromMapApi(e)).toList();
    }
    return [];
  }

  List<Avaliacao> _formatAvaliacoesJson(json) {
    if (json != null) {
      return json.map<Avaliacao>((e) => Avaliacao.fromMapApi(e)).toList();
    }
    return [];
  }

  CronogramaPublishResponse _formatToPublishSchedule(json) {
    if (json != null) {
      return CronogramaPublishResponse.fromMap(json);
    }
    return null;
  }

  DuplicatedCronogramaAPI _formatToDuplicatedSchedule(json) {
    if (json != null) {
      return DuplicatedCronogramaAPI.fromMap(json);
    }
    return null;
  }
}
