
import 'package:dio/dio.dart';
import 'package:nice_travel/app/config/api/api_connection_helper.dart';
import 'package:nice_travel/app/domain/atividade.dart';

class AtividadeApiConnection {
  static final path = '/activities/';

  AtividadeApiConnection._();

  static final AtividadeApiConnection instance = new AtividadeApiConnection._();

  Future<List<Atividade>> getActiviesByScheduleDay(
      Dio dio, int scheduleDayId) async {
    final url = ApiConnectionHelper.urlApi + path + '$scheduleDayId';
    return _formatFutureToActivities(await ApiConnectionHelper.get(dio, url));
  }

  List<Atividade> _formatFutureToActivities(response) {
    return _formatActivitiesJson(response);
  }

  List<Atividade> _formatActivitiesJson(json) {
    if (json != null) {
      return json.map<Atividade>((e) => Atividade.fromMapApi(e)).toList();
    }
    return [];
  }

}
