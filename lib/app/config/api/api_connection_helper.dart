import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:nice_travel/app/config/error_logger.dart';
import 'package:nice_travel/app/exception/api_exception.dart';

class ApiConnectionHelper {
  static final urlApi = FlavorConfig.instance.variables["url_api"];

  static String encodeFull(String url) {
    var urlFormatted = Uri.encodeFull(url);
    if (FlavorConfig.instance.name != null) {
      print(urlFormatted);
    }
    return urlFormatted;
  }

  static Future<dynamic> get(Dio dio, String url) async {
    final response =
        await dio.get(encodeFull(url), options: _getConnectionsOptions());
    return _returnResponse(response);
  }

  static Future<dynamic> post(Dio dio, String url) async {
    final response =
        await dio.post(encodeFull(url), options: _getConnectionsOptions());
    return _returnResponse(response);
  }

  static Future<dynamic> postData(Dio dio, String url, String json) async {
    final response = await dio.post(encodeFull(url),
        data: json, options: _getConnectionsOptions());
    return _returnResponse(response);
  }

  static Future<dynamic> patchData(Dio dio, String url, String json) async {
    final response = await dio.patch(encodeFull(url),
        data: json, options: _getConnectionsOptions());
    return _returnResponse(response);
  }

  static Future<dynamic> delete(Dio dio, String url) async {
    final response = await dio.delete(url, options: _getConnectionsOptions());
    return _returnResponse(response);
  }

  static Options _getConnectionsOptions() {
    Options options = new Options(
      headers: {'authorization': 'Basic ${_getBasicAuthorization()}'},
      sendTimeout: 10000,
      receiveTimeout: 10000,
      validateStatus: (status) {
        return status <= 500;
      },
    );
    return options;
  }

  static String _getBasicAuthorization() {
    var basic = base64Encode(
        utf8.encode('admin:${FlavorConfig.instance.variables['url_key']}'));
    return basic;
  }

  static dynamic _returnResponse(response) {
    if (response.statusCode < 300) {
      return response.data;
    }
    switch (response.statusCode) {
      case 400:
        throw BadRequestException(response.data.toString());
      case 401:
      case 403:
      case 404:
        throw UnauthorisedException(response.data.toString());
      case 500:
      default:
        ErrorLogger().log(response.toString(), response.data);
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
