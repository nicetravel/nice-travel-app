import 'package:flutter/material.dart';
import 'package:nice_travel/app/modules/termosuso/widget/termos_uso_text.dart';
import 'package:nice_travel/app/shared/custom_app_bar.dart';


class TermosUsoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "Termos de Uso",),
      body: TermosUsoText(),
    );
  }
}
