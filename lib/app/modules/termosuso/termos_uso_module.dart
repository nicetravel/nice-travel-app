import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/modules/termosuso/page/termos_uso_page.dart';

class TermosUsoModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => TermosUsoPage()),
      ];

  static Inject get to => Inject<TermosUsoModule>.of();
}
