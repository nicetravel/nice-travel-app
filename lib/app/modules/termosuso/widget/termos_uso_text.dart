
import 'package:flutter/material.dart';

class TermosUsoText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            _buildTopic(
                context,
                title: "1. Nice Travel",
                message:
                "Nice Travel é uma aplicação voltada para o mercado de turismo."
                    "\nA aplicação tem como missão tornar suas viagens melhores, por meio de roteiros prontos e avaliados."
                    "\nOs roteiros serão criados por usuários e avaliados por outros usuários. "
                    "Sendo assim, é de responsabilidade do usuário que está utilizando o roteiro de realizar as devidas adequações caso necessário."),
            _buildTopic(
                context,
                title: "2. Entendendo a aplicação",
                message:
                "2.1 O uso do Nice Travel deve ser feito para obter e publicar Roteiros de viagem a fim de ajudar outros usuários."
                    "\n2.2 Os roteiros serão avaliados por outros usuários, caso os roteiros produzidos por determinado usuário tenha diversas avaliações negativas, o mesmo poderá sofrer sanções conforme § 5 destes Termos de Uso."
                    "\n2.3 As avaliações devem ser feitas a fim de melhorar a qualidade dos roteiros para outros usuários."),
            _buildTopic(
                context,
                title: "3. Permissões de uso da aplicação",
                message:
                "Nesse artigo será explicado sobre o uso dos dados e do smartphone do usuário por meio da aplicação. "
                    " 3.1 Nice Travel utilizará o armazenamento do seu dispositivo para fornecer o serviço offline, a fim de garantir uma maior comodidade durante as viagens."
                    "\n 3.2 Ao publicar o seu cronograma, ele ficará disponível para todos os usuários, e só poderá ser removido pelo usuário criador, com exceção do caso onde a conta do usuário seja bloqueada e/ou excluída conforme § 5 destes Termos de Uso."
                    "\n 3.3 Ao publicar ou republicar o cronograma, será necessário a utilização dados de rede, então, é recomendado o uso conectado em alguma rede Wi-Fi."
                    "\n 3.4 A busca por novos roteiros é feito de forma online, na qual é utilizado dados de rede, então, é recomendado o uso conectado em alguma rede Wi-Fi."),
            _buildTopic(
                context,
                title: "4. Mudanças de Termos de uso",
                message:
                "Nice Travel tem o direito de ajustar os termos de uso caso haja qualquer mudança que necessite alteração nos termos de uso."
                    "\nCaso os termos de uso sejam alterados, será exibido os termos de uso com as mudanças para que o usuário fique ciente das mudanças."),
            _buildTopic(
                context,
                title: "5. Bloqueios em caso de mal uso",
                message:
                "Caso o usuário utilize o aplicativo de má fé, violando o que está regido pelo § 2 destes Termos de Uso, a conta do mesmo poderá ser bloqueada."
                    "E todos os seus dados poderão ser excluídos. "),
            _buildTopic(
                context,
                title: "6. Rejeição dos termos de uso",
                message:
                "Em caso de rejeições dos termos de uso, recomendamos a exclusão do aplicativo, e sugerimos que entre em contato com o suporte"
                    "para que seja realizada os devidos ajustes caso seja possível. "
                    "\nContato para o suporte: nicetravel.mobileapp@.gmail.com.br."),
          ],
        ),
      ),
    );
  }


  Widget _buildTopic(BuildContext context, {@required String title, @required String message}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: RichText(
          textAlign: TextAlign.justify,
          text: TextSpan(
              text: '$title\n\n',
              style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color, fontSize: 18),
              children: <TextSpan>[
                TextSpan(text: message, style: TextStyle(fontSize: 14))
              ])),
    );
  }
}
