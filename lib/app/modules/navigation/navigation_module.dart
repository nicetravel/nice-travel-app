import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/modules/navigation/page/navigation_page.dart';

class NavigationModule extends ChildModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
    ModularRouter(AppModule.NAVEGACAO_PATH, child: (_, args) => NavigationPage()),
  ];

  static Inject get to => Inject<NavigationModule>.of();
}
