import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';

class MenuTabModel {
  final IconData icon;
  final String name;
  final String navigationPath;

  MenuTabModel(this.icon, this.name, this.navigationPath);

  static List<MenuTabModel> getTabModels() {
    List<MenuTabModel> tabs = [];
    tabs.add(
        MenuTabModel(Icons.policy, "Termos de Uso ", AppModule.TERMOS_USO));
    return tabs;
  }
}
