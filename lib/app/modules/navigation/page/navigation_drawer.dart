import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/meu_cronograma_controller.dart';
import 'package:nice_travel/app/modules/navigation/page/drawer_tile.dart';
import 'package:nice_travel/app/modules/navigation/page/menu_tab_model.dart';

class NavigationDrawer extends StatelessWidget {
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          ListView(
            padding: EdgeInsets.only(left: 32.0, top: 16.0),
            children: <Widget>[
              _buildLogo(context),
              Divider(height: 20),
              createDrawerTiles(),
            ],
          )
        ],
      ),
    );
  }

  Container _buildLogo(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      height: 170.0,
      child: Stack(
        children: <Widget>[
          Center(
            child: Image.asset("assets/images/logo.png"),
          ),
          buildSignInButton(context)
        ],
      ),
    );
  }

  Widget buildSignInButton(BuildContext context) {
    return Observer(
        builder: (BuildContext context) => Positioned(
              left: 0.0,
              bottom: 0.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    " ${!_securityService.isLoggedIn() ? "" : "Olá, " + _securityService.getSessionUser().displayName}",
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                  ),
                  GestureDetector(
                    child: buildLoginButton(context),
                    onTap: () => logout(context),
                  )
                ],
              ),
            ));
  }

  Widget buildLoginButton(BuildContext context) {
    if (!_securityService.isLoggedIn()) {
      return Row(
        children: <Widget>[
          Text(
            "Entrar ",
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
          Icon(
            Icons.login,
            size: 16,
            color: Theme.of(context).primaryColor,
          )
        ],
      );
    }
    return Row(
      children: <Widget>[
        GestureDetector(
          onTap: () => logout(context),
          child: Text(
            "Sair ",
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        Icon(
          Icons.logout,
          size: 16,
          color: Theme.of(context).primaryColor,
        )
      ],
    );
  }

  Widget createDrawerTiles() {
    return Column(
      children: MenuTabModel.getTabModels().map((e) => drawerTile(e)).toList(),
    );
  }

  DrawerTile drawerTile(MenuTabModel tab) {
    return DrawerTile(tab.icon, tab.name, tab.navigationPath,
        key: Key("DrawerTile_${tab.navigationPath}"));
  }

  logout(BuildContext context) {
    if (!_securityService.isLoggedIn()) {
      Navigator.of(context).pushNamed(AppModule.LOGIN_PATH);
    } else {
      _securityService.signOut();
      Modular.dispose<IMeuCronogramaRepository>();
      Modular.dispose<MeuCronogramaController>();

      Navigator.of(context)
          .pushNamedAndRemoveUntil(AppModule.VIAGEM_PATH, (_) => true);
    }
  }
}
