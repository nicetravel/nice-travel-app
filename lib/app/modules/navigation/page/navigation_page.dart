import 'package:flutter/material.dart';
import 'package:nice_travel/app/modules/navigation/page/navigation_drawer.dart';
import 'package:nice_travel/app/shared/widgets/navigation/custom_bottom_navigation_bar.dart';

class NavigationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NavigationDrawer(),
      bottomNavigationBar: CustomBottomNavigationBar(selectedIndex: 2,),
    );
  }
}
