import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/modules/home/page/home_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_widget.dart';

class CronogramaFilter extends StatefulWidget {
  @override
  _CronogramaFilterState createState() => _CronogramaFilterState();
}

class _CronogramaFilterState extends State<CronogramaFilter> {
  final _autocompleteController = CityAutocompleteController();
  final _controller = Modular.get<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(8),
      elevation: 3.00,
      child: Container(
        margin: EdgeInsets.all(2),
        child: _buildAutocompleteCity(),
      ),
    );
  }

  Widget _buildAutocompleteCity() {
    return CityAutocompleteWidget(
        clearFunction: () => _updateSchedules(),
        controller: _autocompleteController,
        inputDecoration: InputDecoration(
            hintText: "Vai para onde?",
            border: InputBorder.none,
            prefixIcon: Icon(Icons.search)),
        onSelectCity: () => _updateSchedules());
  }

  void _updateSchedules() {
    _controller.setPlaceId(_autocompleteController.getPlaceId());
  }
}
