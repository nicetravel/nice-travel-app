import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/home/page/home_controller.dart';
import 'package:nice_travel/app/modules/meu_cronograma/widgets/cronograma_list_item.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';

class CronogramasList extends StatefulWidget {
  final String placeId;

  const CronogramasList({Key key, this.placeId}) : super(key: key);

  @override
  _CronogramasListState createState() => _CronogramasListState();
}

class _CronogramasListState extends State<CronogramasList> {
  final _controller = HomeModule.to.get<HomeController>();

  ScrollController controller;
  var mustReload = true;

  @override
  void initState() {
    super.initState();
    controller = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    controller.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return APIList<ApiResponse<List<Cronograma>>>(
        elements: () {
          if (_controller.filteredChanged()) {
            return _controller.findAll(widget.placeId);
          }
          return Future.value(ApiResponse.completed(_controller.cronogramas));
        },
        onCompleted: (list) {
          _controller.fillCronogramas(list);
          if (!_controller.couldSearchMoreElements()) {
            mustReload = false;
          }
        },
        listWidget:
            Observer(builder: (_) => buildList(_controller.cronogramas)));
  }

  Widget buildList(List<Cronograma> cronogramas) {
    if (cronogramas.isNotEmpty) {
      return Expanded(
        child: ListView.builder(
          controller: controller,
          itemBuilder: (BuildContext context, int index) {
            return MeuCronogramaListItem(cronograma: cronogramas[index]);
          },
          itemCount: cronogramas.length,
        ),
      );
    } else {
      return Expanded(
        child: Center(
            child: Text(
          "Nenhum roteiro cadastrado ainda."
          "\nAproveite e seja o primeiro a cadastrar.",
          style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        )),
      );
    }
  }

  void _scrollListener() {
    if (controller.position.extentAfter < 1 && mustReload) {
      var _mustReload = _controller.searchMoreElements();
      setState(() {
        this.mustReload = _mustReload;
      });
    }
  }
}
