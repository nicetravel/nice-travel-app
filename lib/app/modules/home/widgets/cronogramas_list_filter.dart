import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/home/page/home_controller.dart';
import 'package:nice_travel/app/modules/home/widgets/cronograma_filter.dart';
import 'package:nice_travel/app/modules/home/widgets/cronogramas_list.dart';

class CronogramasListFilter extends StatelessWidget {
  final _controller = HomeModule.to.get<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CronogramaFilter(),
        Observer(builder: (_) => CronogramasList(placeId: _controller.placeId)),
      ],
    );
  }
}
