// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $HomeController = BindInject(
  (i) => HomeController(
      i<ISecurityService>(), i<ICloudSearchCronogramaRepository>(), i<IDatabase>()),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$_cronogramasAtom = Atom(name: '_HomeControllerBase._cronogramas');

  @override
  List<Cronograma> get _cronogramas {
    _$_cronogramasAtom.reportRead();
    return super._cronogramas;
  }

  @override
  set _cronogramas(List<Cronograma> value) {
    _$_cronogramasAtom.reportWrite(value, super._cronogramas, () {
      super._cronogramas = value;
    });
  }

  final _$_placeIdAtom = Atom(name: '_HomeControllerBase._placeId');

  @override
  String get _placeId {
    _$_placeIdAtom.reportRead();
    return super._placeId;
  }

  @override
  set _placeId(String value) {
    _$_placeIdAtom.reportWrite(value, super._placeId, () {
      super._placeId = value;
    });
  }

  final _$_qtdElementsAtom = Atom(name: '_HomeControllerBase._qtdElements');

  @override
  int get _qtdElements {
    _$_qtdElementsAtom.reportRead();
    return super._qtdElements;
  }

  @override
  set _qtdElements(int value) {
    _$_qtdElementsAtom.reportWrite(value, super._qtdElements, () {
      super._qtdElements = value;
    });
  }

  final _$_HomeControllerBaseActionController =
      ActionController(name: '_HomeControllerBase');

  @override
  void fillCronogramas(List<Cronograma> cronogramas) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.fillCronogramas');
    try {
      return super.fillCronogramas(cronogramas);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  bool searchMoreElements() {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.searchMoreElements');
    try {
      return super.searchMoreElements();
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPlaceId(String placeId) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.setPlaceId');
    try {
      return super.setPlaceId(placeId);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
