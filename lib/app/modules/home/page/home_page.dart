import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/modules/home/widgets/cronogramas_list_filter.dart';
import 'package:nice_travel/app/shared/custom_app_bar.dart';
import 'package:nice_travel/app/shared/widgets/navigation/custom_bottom_navigation_bar.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  @override
  void initState() {
    super.initState();
    controller.signInSilently();
    controller.hasAcceptedTermosUso().then((accepted) {
      if (!accepted) {
        getTermosUsoDialog(
            context: context, action: () => controller.acceptedTermosUso())
          ..show();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: CronogramasListFilter(),
      bottomNavigationBar: CustomBottomNavigationBar(selectedIndex: 0,),
    );
  }
}
