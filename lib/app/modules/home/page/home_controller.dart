import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/util/constants.dart';

part 'home_controller.g.dart';

@Injectable()
class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final ISecurityService _securityService;
  final ICloudSearchCronogramaRepository _repository;
  final IDatabase _localDatabase;
  final storage = new FlutterSecureStorage();

  _HomeControllerBase(this._securityService, this._repository, this._localDatabase);

  static final int qtdElementsRetrieve = 10;

  @observable
  List<Cronograma> _cronogramas = ObservableList<Cronograma>();

  @observable
  String _placeId;

  @observable
  int _qtdElements = qtdElementsRetrieve;

  bool _acceptedTermosUso = false;

  bool _enableRefreshAPISearch = true;

  void setAcceptedTermosUso(bool acceptedTermosUso) {
    _acceptedTermosUso = acceptedTermosUso;
  }

  @action
  void fillCronogramas(List<Cronograma> cronogramas) {
    if (this._cronogramas.isEmpty ||
        this._cronogramas.length != cronogramas.length) {
      this._enableRefreshAPISearch = false;
      this._cronogramas = cronogramas.asObservable();
    }
  }

  @action
  bool searchMoreElements() {
    if (couldSearchMoreElements()) {
      this._qtdElements += qtdElementsRetrieve;
      this._enableRefreshAPISearch = true;
      return true;
    }
    return false;
  }

  bool couldSearchMoreElements() => _cronogramas.length == this._qtdElements;

  @action
  void setPlaceId(String placeId) {
    if (this._placeId != placeId) {
      this._enableRefreshAPISearch = true;
      this._placeId = placeId;
    }
  }

  List<Cronograma> get cronogramas => _cronogramas;

  String get placeId => _placeId;

  Future<ApiResponse<List<Cronograma>>> findAll(String placeId) {
    return _repository.findCronogramas(placeId, _qtdElements);
  }

  filteredChanged() {
    return _enableRefreshAPISearch;
  }

  void signInSilently() async {
    bool loggedIn = await _securityService.signInSilently();
    if(loggedIn){
      var sessionUser = _securityService.getSessionUser();
      _localDatabase.synchronizedFromApi(sessionUser);
    }
  }

  Future<bool> hasAcceptedTermosUso() async {
    if(!_acceptedTermosUso) {
      String accepted = await storage.read(key: ACCEPTED_TERMOS_USO_KEY);
      if (accepted != null && accepted == 'true') {
        setAcceptedTermosUso(true);
        return true;
      } else {
        setAcceptedTermosUso(false);
        return false;
      }
    }
    return true;
  }

  void acceptedTermosUso() async {
    storage.write(key: ACCEPTED_TERMOS_USO_KEY, value: "true");
    setAcceptedTermosUso(true);
  }

}
