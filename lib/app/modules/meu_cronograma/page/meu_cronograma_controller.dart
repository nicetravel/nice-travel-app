import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

part 'meu_cronograma_controller.g.dart';

class MeuCronogramaController = _MeuCronogramaControllerBase
    with _$MeuCronogramaController;

abstract class _MeuCronogramaControllerBase with Store {
  final IMeuCronogramaRepository _repository;
  final ISecurityService _securityService;
  final IDatabase _localDatabase;

  _MeuCronogramaControllerBase(
      this._repository, this._securityService, this._localDatabase);

  @observable
  List<Cronograma> _meusCronogramas = ObservableList<Cronograma>();

  bool _enableRefreshAPISearch = true;

  List<Cronograma> get meusCronogramas => _meusCronogramas;

  @action
  void addCronograma(Cronograma cronograma) {
    _meusCronogramas.add(cronograma);
  }

  @action
  void fillCronogramas(List<Cronograma> cronogramas) {
    if (this._enableRefreshAPISearch && cronogramas.length > 0) {
      this._enableRefreshAPISearch = false;
      this._meusCronogramas = cronogramas.asObservable();
    }
  }

  Future<ApiResponse<List<Cronograma>>> findAll() {
    return _repository.findAll();
  }

  canRefresh() {
    return _enableRefreshAPISearch;
  }

  Future<bool> synchronizedFromApi() async {
    var sessionUser = _securityService.getSessionUser();
    if (sessionUser != null) {
     return await _localDatabase.synchronizedFromApi(sessionUser);
    }
    return false;
  }

}
