import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_widget.dart';
import 'package:nice_travel/app/shared/custom_app_bar.dart';
import 'package:nice_travel/app/shared/loading_util.dart';

import 'novo_cronograma_controller.dart';

class NovoCronogramaPage extends StatefulWidget {
  final MeuCronograma cronograma;

  const NovoCronogramaPage({Key key, this.cronograma}) : super(key: key);

  @override
  _NovoCronogramaPageState createState() => _NovoCronogramaPageState();
}

class _NovoCronogramaPageState
    extends ModularState<NovoCronogramaPage, NovoCronogramaController> {
  GlobalKey<FormState> _formKey = GlobalKey();

  final autocompleteController = CityAutocompleteController();

  @override
  void initState() {
    controller.meuCronograma = widget.cronograma ?? MeuCronograma();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: "Novo Cronograma"),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Form(
              key: _formKey,
              child: Wrap(runSpacing: 15.0, children: <Widget>[
                _buildAutocompleteCity(),
                _builInputField(
                    initalValue: controller.meuCronograma.qtdDias,
                    hintText: 'Quantidade de Dias',
                    onChanged: controller.meuCronograma.setQtdDias,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    keyboardType: TextInputType.number),
                _buildSaveButton(),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAutocompleteCity() {
    return CityAutocompleteWidget(
        controller: autocompleteController,
        clearFunction: () => controller.updateCity(
            autocompleteController.getPlaceId(),
            autocompleteController.getNameCity(),
            autocompleteController.getPlaceState()),
        onSelectCity: () => controller.updateCity(
            autocompleteController.getPlaceId(),
            autocompleteController.getNameCity(),
            autocompleteController.getPlaceState()));
  }

  Widget _buildSaveButton() {
    return MaterialButton(
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.save,
                  color: Colors.white,
                ),
                SizedBox(width: 10),
                Text(
                  'Salvar',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                  ),
                ),
              ]),
        ),
        onPressed: () => salvar());
  }

  Widget _builInputField(
      {@required initalValue,
      @required String hintText,
      @required Function onChanged,
      TextInputType keyboardType,
      int maxLength,
      List<TextInputFormatter> inputFormatters}) {
    return TextFormField(
      validator: RequiredValidator(errorText: 'Campo Obrigatório'),
      maxLength: maxLength,
      initialValue: initalValue?.toString(),
      onChanged: onChanged,
      keyboardType: keyboardType,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
        labelText: hintText,
        border: OutlineInputBorder(),
        focusedErrorBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
        errorBorder:
            OutlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      ),
    );
  }

  salvar() {
    if (autocompleteController.cityInputsNull) {
      autocompleteController.clearPlaceID();
    }
    if (_formKey.currentState.validate()) {
      LoadingUtil.showLoadingAlert(context);
      Navigator.of(context).pop();
      controller.saveCronograma().then((value) => Navigator.of(context).pop());
    }
  }
}
