import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/meu_cronograma_controller.dart';

part 'novo_cronograma_controller.g.dart';

@Injectable()
class NovoCronogramaController = _NovoCronogramaControllerBase
    with _$NovoCronogramaController;

abstract class _NovoCronogramaControllerBase with Store {
  IMeuCronogramaRepository _repository =
    AppModule.to.get<IMeuCronogramaRepository>();
  MeuCronogramaController _meuCronogramaController =
      MeuCronogramaModule.to.get<MeuCronogramaController>();

  MeuCronograma meuCronograma;

  Future<void> saveCronograma() async {
    var cronogramaSaved = await _repository.saveOrUpdate(meuCronograma);
    _meuCronogramaController.addCronograma(cronogramaSaved);
    return Future.value();
  }

  updateCity(String placeId, String nomeLugar, String placeState) {
    meuCronograma.setNomeLugar(nomeLugar);
    meuCronograma.setPlaceId(placeId);
    meuCronograma.setPlaceState(placeState);
  }
}
