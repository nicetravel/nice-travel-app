// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meu_cronograma_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MeuCronogramaController on _MeuCronogramaControllerBase, Store {
  final _$_meusCronogramasAtom =
      Atom(name: '_MeuCronogramaControllerBase._meusCronogramas');

  @override
  List<Cronograma> get _meusCronogramas {
    _$_meusCronogramasAtom.reportRead();
    return super._meusCronogramas;
  }

  @override
  set _meusCronogramas(List<Cronograma> value) {
    _$_meusCronogramasAtom.reportWrite(value, super._meusCronogramas, () {
      super._meusCronogramas = value;
    });
  }

  final _$_MeuCronogramaControllerBaseActionController =
      ActionController(name: '_MeuCronogramaControllerBase');

  @override
  void addCronograma(Cronograma cronograma) {
    final _$actionInfo = _$_MeuCronogramaControllerBaseActionController
        .startAction(name: '_MeuCronogramaControllerBase.addCronograma');
    try {
      return super.addCronograma(cronograma);
    } finally {
      _$_MeuCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fillCronogramas(List<Cronograma> cronogramas) {
    final _$actionInfo = _$_MeuCronogramaControllerBaseActionController
        .startAction(name: '_MeuCronogramaControllerBase.fillCronogramas');
    try {
      return super.fillCronogramas(cronogramas);
    } finally {
      _$_MeuCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
