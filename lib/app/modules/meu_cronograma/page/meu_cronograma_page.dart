import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/widgets/meus_cronogramas_list.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';
import 'package:nice_travel/app/shared/widgets/navigation/custom_bottom_navigation_bar.dart';
import 'package:nice_travel/app/util/show_toast.dart';

import 'meu_cronograma_controller.dart';

class MeuCronogramaPage extends StatefulWidget {
  final String title;

  const MeuCronogramaPage({Key key, this.title = "Meus Cronogramas"})
      : super(key: key);

  @override
  _MeuCronogramaPageState createState() => _MeuCronogramaPageState();
}

class _MeuCronogramaPageState
    extends ModularState<MeuCronogramaPage, MeuCronogramaController> {
  final _securityService = AppModule.to.get<ISecurityService>();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _synchronizedIfRequired();
  }

  void _synchronizedIfRequired() async {
    var synchronized = await controller.synchronizedFromApi();
    if (synchronized) {
      setState(() {
        /*Reload*/
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: CustomBottomNavigationBar(selectedIndex: 1,),
        key: _scaffoldKey,
        body: _buildListCronogramas(),
        floatingActionButton: FloatingActionButton.extended(
            icon: Icon(Icons.add),
            label: Text("Novo "),
            onPressed: () => _sendToNovoCronogramaPage()));
  }

  Future<ApiResponse<List<Cronograma>>> retrieveElements() {
    if (controller.canRefresh()) {
      return controller.findAll();
    }
    return Future.value(ApiResponse.completed(controller.meusCronogramas));
  }

  _buildListCronogramas() {
    return APIList<ApiResponse<List<Cronograma>>>(
        elements: () => retrieveElements(),
        onCompleted: (list) => controller.fillCronogramas(list),
        listWidget: Observer(builder: (_) => _buildListCrongorama()));
  }

  Widget _buildListCrongorama() {
    if (controller.meusCronogramas.isNotEmpty) {
      return MeusCronogramasList(controller.meusCronogramas);
    } else {
      return Center(
          child: Text(
        "Nenhum roteiro cadastrado ainda."
        "\nAproveite e cadastre o primeiro.",
        style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ));
    }
  }

  _sendToNovoCronogramaPage() {
    if (_securityService.isLoggedIn()) {
      Navigator.of(context).pushNamed(ModalRoute.of(context).settings.name +
          MeuCronogramaModule.NOVO_CRONOGRAMA_PATH);
    } else {
      buildShowSnackBarToLogin(context, scaffold: _scaffoldKey.currentState);
    }
  }
}
