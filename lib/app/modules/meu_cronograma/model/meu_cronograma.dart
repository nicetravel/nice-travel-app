import 'package:mobx/mobx.dart';

part 'meu_cronograma.g.dart';

class MeuCronograma = MeuCronogramaModel with _$MeuCronograma;

abstract class MeuCronogramaModel with Store {
  @observable
  String _nomeLugar;

  String _placeID;
  String _placeState;

  int _qtdDias;

  int _cod;

  get getCod => _cod;

  get getPlaceId => _placeID;

  @action
  setNomeLugar(String value) {
    _nomeLugar = value;
  }

  setQtdDias(String value) {
    if(value.isNotEmpty) {
      _qtdDias = int.parse(value);
    }
  }

  setPlaceId(String placeID) {
    _placeID = placeID;
  }

  setPlaceState(String placeState) {
    _placeState = placeState;
  }

  int get qtdDias => _qtdDias;

  String get nomeLugar => _nomeLugar;

  String get placeState => _placeState;

  MeuCronogramaModel();

  MeuCronogramaModel.initValues(this._nomeLugar, this._qtdDias, this._cod);
}
