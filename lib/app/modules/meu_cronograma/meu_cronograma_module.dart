import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/meu_cronograma/page/novo_cronograma/novo_cronograma_module.dart';

import 'page/meu_cronograma_controller.dart';
import 'page/meu_cronograma_page.dart';

class MeuCronogramaModule extends ChildModule {
  static const NOVO_CRONOGRAMA_PATH = '/novo-cronograma';

  @override
  List<Bind> get binds => [
        Bind<MeuCronogramaController>((i) => MeuCronogramaController(
            i<IMeuCronogramaRepository>(),
            i<ISecurityService>(),
            i<IDatabase>())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => MeuCronogramaPage()),
        ModularRouter(NOVO_CRONOGRAMA_PATH, module: NovoCronogramaModule()),
      ];

  static Inject get to => Inject<MeuCronogramaModule>.of();
}
