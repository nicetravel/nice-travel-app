import 'package:flutter/material.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/meu_cronograma/widgets/cronograma_list_item.dart';

class MeusCronogramasList extends StatelessWidget {
  final List<Cronograma> meusCronogramas;

  MeusCronogramasList(this.meusCronogramas);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: meusCronogramas.length,
        itemBuilder: (BuildContext context, int index) {
          final meuCronograma = meusCronogramas[index];
          return MeuCronogramaListItem(cronograma: meuCronograma);
        });
  }
}
