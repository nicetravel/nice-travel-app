import 'package:bordered_text/bordered_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/shared/widgets/star_count_widget.dart';
import 'package:nice_travel/app/util/format_util.dart';

class MeuCronogramaListItem extends StatelessWidget {
  final Cronograma cronograma;

  MeuCronogramaListItem({@required this.cronograma});

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) => Visibility(
          visible: !cronograma.removed,
          child: GestureDetector(
            child: Container(
                key: Key("list_item_card_image"),
                margin: EdgeInsets.only(left: 8, right: 8, bottom: 6, top: 6),
                height: 211,
                decoration: _buildBoxShadow(context),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          _buildPrice(),
                          _buildQtdStar(),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.all(10),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    _buildCityAdress(),
                                    buildCityInfo(context),
                                  ]))
                        ],
                      ),
                    ),
                  ],
                )),
            onTap: () => _sendToMeuCronogramaPage(context),
          )),
    );
  }

  void _sendToMeuCronogramaPage(BuildContext context) {
    Navigator.of(context)
        .pushNamed(AppModule.DIA_CRONOGRAMA_PATH, arguments: cronograma);
  }

  BoxDecoration _buildBoxShadow(BuildContext context) {
    Color color = Theme.of(context).brightness == Brightness.dark
        ? Colors.black45
        : Colors.grey;
    return BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: color,
        boxShadow: [
          BoxShadow(
            color: color,
            blurRadius: 1.0,
            spreadRadius: 1.0,
            offset: Offset(
              1.0, // horizontal move
              2.0, // vertica move
            ),
          )
        ],
        image: buildDecorationImage());
  }

  Row buildCityInfo(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _buildPersonName(),
        _buildQtdDias(),
      ],
    );
  }

  Row _buildQtdDias() {
    return Row(
      children: <Widget>[
        BorderedText(
            strokeWidth: 2.0,
            child: Text(
              '${this.cronograma.qtdDays} Dia(s)',
              style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.w900),
            )),
        Icon(
          Icons.today,
          color: Colors.white,
        ),
      ],
    );
  }

  Widget _buildPrice() {
    return Observer(
        builder: (_) => BorderedText(
            strokeWidth: 2.0,
            child: Text(
              'R\$ ${formatMoney(this.cronograma.priceFinal)}',
              style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.w900),
            )));
  }

  Widget _buildQtdStar() {
    return Observer(
      builder: (_) => Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: StarCountWidget(numberLike: cronograma.numberLike),
      ),
    );
  }

  Row _buildPersonName() {
    return Row(
      children: <Widget>[
        Container(
            decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.65),
                blurRadius: 7.0,
              ),
            ]),
            child: Icon(
              Icons.person,
              color: Colors.white,
            )),
        BorderedText(
            strokeWidth: 2.0,
            child: Text(
              this.cronograma.userName,
              style: TextStyle(
                  fontSize: 15,
                  color: Colors.white,
                  fontWeight: FontWeight.w900),
            )),
      ],
    );
  }

  Widget _buildCityAdress() {
    return BorderedText(
        strokeWidth: 2.0,
        child: Text(
          '${cronograma.getCityFormattedName()}',
          style: TextStyle(
              fontSize: 30, color: Colors.white, fontWeight: FontWeight.w900),
        ));
  }

  DecorationImage buildDecorationImage() {
    return DecorationImage(
        fit: BoxFit.cover,
        colorFilter: new ColorFilter.mode(
            Colors.white.withOpacity(0.8), BlendMode.dstATop),
        image: CachedNetworkImageProvider(this.cronograma.firstImage));
  }
}
