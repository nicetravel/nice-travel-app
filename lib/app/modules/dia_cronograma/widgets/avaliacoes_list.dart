import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/avaliacao_action.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';

import 'avaliacoes_bar_chart.dart';

class AvaliacoesList extends StatefulWidget {
  final Cronograma _cronograma;

  AvaliacoesList(this._cronograma);

  @override
  _AvaliacoesListState createState() => _AvaliacoesListState();
}

class _AvaliacoesListState extends State<AvaliacoesList> {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  @override
  Widget build(BuildContext context) {
    return APIList<ApiResponse<List<Avaliacao>>>(
        elements: () =>
            _controller.findAllAvaliacoes(widget._cronograma.scheduleCodAPI),
        onCompleted: (list) => updateList(list),
        listWidget: Observer(
            builder: (_) => Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: buildList(_controller.avaliacoes),
                )));
  }

  updateList(List<Avaliacao> avaliacoes) {
    if (avaliacoes.isNotEmpty) {
      _controller.fillAvaliacoes(avaliacoes);
    }
  }

  Widget buildList(List<Avaliacao> avaliacoes) {
    if (avaliacoes.isEmpty) {
      return _buildEmptyResult();
    }
    Avaliacao avaliacao = getUserAvaliacao(avaliacoes);

    return Column(
      children: [
        _buildAvaliacaoResume(avaliacoes),
        AvaliacaoAction(
          widget._cronograma,
          avaliacao: avaliacao,
        ),
        _buildListAvaliacoes(avaliacoes),
      ],
    );
  }

  Column _buildEmptyResult() {
    return Column(
      children: [
        Expanded(
          child: Center(
              child: Text(
            "Nenhuma avaliação cadastrada ainda.\n" +
                (!isOwner() ? "\nAproveite e seja o primeiro a avaliar." : ""),
            style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          )),
        ),
        Visibility(
            visible: !isOwner(), child: AvaliacaoAction(widget._cronograma)),
      ],
    );
  }

  bool isOwner() =>
      widget._cronograma.isOwner(_securityService.getSessionUser());

  Expanded _buildListAvaliacoes(List<Avaliacao> avaliacoes) {
    return Expanded(
      child: ListView.builder(
          itemCount: avaliacoes.length,
          itemBuilder: (ctx, index) {
            var avaliacao = avaliacoes[index];
            return ListTile(
              title: Text('${avaliacao.description}'),
              leading: Text('${avaliacao.qtdStar} ⭐'),
              subtitle: Text(avaliacao.data),
            );
          }),
    );
  }

  Widget _buildAvaliacaoResume(List<Avaliacao> avaliacoes) {
    double media = getMediaAvaliacoes(avaliacoes);
    var starBarElements = StarBarElements.fromAvaliacoes(avaliacoes);
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "${media.toStringAsFixed(1)}",
                style: TextStyle(fontSize: 50.0),
              ),
              RatingBar.builder(
                itemSize: 20.0,
                allowHalfRating: true,
                initialRating: starBarElements.getMeanStar(),
                minRating: 1,
                direction: Axis.horizontal,
                itemCount: 5,
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: null,
              ),
            ],
          ),
          Expanded(child: AvaliacoesBarChart(starBarElements: starBarElements)),
        ],
      ),
    ));
  }

  double getMediaAvaliacoes(List<Avaliacao> avaliacoes) {
    double media = 0.0;
    avaliacoes.forEach((element) => media += element.qtdStar);
    return media / avaliacoes.length;
  }

  Avaliacao getUserAvaliacao(List<Avaliacao> avaliacoes) {
    var sessionUser = _securityService.getSessionUser();
    if (sessionUser == null) {
      return null;
    }
    var avaliacao = avaliacoes
        .where((element) => element.userUid == sessionUser.uid)
        .toList();
    if (avaliacao.isNotEmpty) {
      return avaliacao[0];
    }
    return null;
  }
}
