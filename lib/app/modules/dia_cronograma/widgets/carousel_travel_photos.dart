import 'package:bordered_text/bordered_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/util/format_util.dart';

class CarouselTravelPhotos extends StatelessWidget {
  final Cronograma cronograma;
  final double heightAppBar;

  const CarouselTravelPhotos({Key key, this.cronograma, this.heightAppBar})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    CarouselOptions options = CarouselOptions(
      height: heightAppBar + 50,
      viewportFraction: 1,
      // viewportFraction: 0.8,
      autoPlay: true,
      // enlargeCenterPage: true,
    );
    return Observer(
        builder: (_) => CarouselSlider.builder(
              itemCount: cronograma.imagesUrl.length,
              options: options,
              itemBuilder: (BuildContext context, int itemIndex, int _) =>
                  Container(
                      height: heightAppBar,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 50.0, top: 5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AppBar(
                              title: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  _buildPrice(),
                                ],
                              ),
                              backgroundColor: Colors.transparent,
                              elevation: 0,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        _cityNameTitle(),
                                      ]),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            // colorFilter: ColorFilter.mode(
                            //     Colors.black.withOpacity(1), BlendMode.dstATop),
                            image: CachedNetworkImageProvider(
                                cronograma.imagesUrl[itemIndex])),
                      )),
            ));
  }

  Widget _buildPrice() {
    return Observer(
      builder: (_) => BorderedText(
          strokeWidth: 2.0,
          child: Text(
            'R\$ ${formatMoney(this.cronograma.priceFinal)}',
            style: TextStyle(
                fontSize: 15, color: Colors.white, fontWeight: FontWeight.w900),
          )),
    );
  }

  Widget _cityNameTitle() {
    return BorderedText(
      strokeWidth: 2.0,
      child: Text(
        '${cronograma.cityAddress}',
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: Colors.white,
          fontFamily: "Literata",
          fontWeight: FontWeight.bold,
          fontSize: 40,
        ),
      ),
    );
  }
}
