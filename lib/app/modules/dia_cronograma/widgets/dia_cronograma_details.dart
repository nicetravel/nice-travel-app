import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/icon_style_activity.dart';
import 'package:nice_travel/app/util/format_util.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class DayScheduleDetails extends StatefulWidget {
  final DiaCronograma diaCronograma;
  final Cronograma cronograma;

  DayScheduleDetails(this.diaCronograma, this.cronograma, {Key key})
      : super(key: key);

  _DayScheduleDetailsState createState() => _DayScheduleDetailsState();
}

class _DayScheduleDetailsState extends State<DayScheduleDetails> {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();
  final ICloudCronogramaRepository _cronogramaRepository =
      DiaCronogramaModule.to.get<ICloudCronogramaRepository>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 12, right: 12, top: 0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListTile(
            leading: buildLeadingIcon(),
            title: _buildTitle(),
            trailing: _buildRemoveButton(context),
            onTap: sendActivityTimeline,
          ),
        ),
      ),
    );
  }

  MaterialButton _buildRemoveButton(BuildContext context) {
    return MaterialButton(
        key: Key("remover_schedule_day_${widget.diaCronograma.day}"),
        child: Icon(
          Icons.delete,
          color: Colors.red,
        ),
        minWidth: 60,
        height: 100,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(60.0)),
        onPressed: () => validateLoginAction(
            context: context,
            user: _securityService.getSessionUser(),
            cronograma: widget.cronograma,
            cronogramaRepository: _cronogramaRepository,
            successAction: () => removeDialog(
                context,
                'Deseja remover o dia ${widget.diaCronograma.day}?',
                deleteScheduleDay)));
  }

  Widget _buildPriceSubtitle() {
    return Observer(
      builder: (_) => Text(
        "R\$: ${formatMoney(widget.diaCronograma.priceDay)}",
        style: TextStyle(
          fontSize: 14,
          color: Colors.green,
          fontFamily: "OpenSans",
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Column(
      children: <Widget>[
        _buildDayText(),
        _buildActivitiesCount(),
        _buildPriceSubtitle(),
      ],
    );
  }

  Widget _buildActivitiesCount() {
    return Observer(
        builder: (_) => Text(
              "${widget.diaCronograma.qtdActivities} Atividades",
              style: TextStyle(fontFamily: "Literata"),
            ));
  }

  Text _buildDayText() {
    return Text(
      "${widget.diaCronograma.day}º Dia",
      style: TextStyle(fontFamily: "Literata", fontWeight: FontWeight.bold),
    );
  }

  Widget buildLeadingIcon() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: widget.diaCronograma.typeFirstActivity != null
            ? Observer(
                builder: (_) => IconStyleActivity(
                        widget.diaCronograma.typeFirstActivity,
                        withColor: true)
                    .icon,
              )
            : IconStyleActivity(Style.OTHER.toString(), withColor: true).icon);
  }

  deleteScheduleDay() {
    Navigator.pop(context);
    showCircularProgress(context);
    _controller
        .deleteDiaCronograma(widget.diaCronograma.id, widget.cronograma)
        .then((_) {
      Navigator.pop(context);
      widget.cronograma.subtractQtdDays();
      _controller.deleteInternal(widget.diaCronograma.id);
    });
  }

  sendActivityTimeline() {
    Map<String, dynamic> parameters = {
      'diaCronograma': widget.diaCronograma,
      'cronograma': widget.cronograma
    };

    Navigator.of(context).pushNamed(
        ModalRoute.of(context).settings.name +
            DiaCronogramaModule.ATIVIDADES_PATH,
        arguments: parameters);
  }
}
