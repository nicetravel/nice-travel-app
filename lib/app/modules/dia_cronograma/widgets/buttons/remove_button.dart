import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/config/error_logger.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/util/constants.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';

class RemoveButton extends StatefulWidget {
  final Cronograma cronograma;

  RemoveButton(this.cronograma);

  @override
  _RemoveButtonState createState() => _RemoveButtonState();
}

class _RemoveButtonState extends State<RemoveButton> {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
      key: Key('remover_schedule'),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.delete,
            size: ICON_SIZE,
            color: Colors.red,
          ),
          textButton("Remover"),
        ],
      ),
      onPressed: () {
        removeDialog(context, "Deseja remover esse cronograma?",
            () => deleteAction(context));
      },
    );
  }

  deleteAction(BuildContext context) {
    showCircularProgress(context);
    _controller.deleteCronograma(widget.cronograma).then((value) {
      widget.cronograma.setRemoved();
      Navigator.pop(context); //Progress
      Navigator.pop(context); //Modal
      Navigator.pop(context); //Page
    }).catchError((exception, stacktrace) {
      Navigator.pop(context); //Progress
      Navigator.pop(context); //Modal
      onError(exception, stacktrace, context);
    });
  }

  void onError(Object error, StackTrace stackTrace, BuildContext context) {
    ErrorLogger().log(error.toString(), stackTrace.toString());
    getErrorDialog(
        dismissOnTouchOutside: true,
        context: context,
        title: "Ocorreu um erro!",
        message: "Erro ao deletar o cronograma, tente novamente mais tarde.")
      ..show();
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: ICON_NAME_SIZE,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        textAlign: TextAlign.center,
      );
}
