import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/floating_modal.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/avaliacoes_list.dart';
import 'package:nice_travel/app/util/constants.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class AvaliarButton extends StatefulWidget {
  final Cronograma cronograma;

  const AvaliarButton({Key key, @required this.cronograma}) : super(key: key);

  @override
  _AvaliarButtonState createState() => _AvaliarButtonState();
}

class _AvaliarButtonState extends State<AvaliarButton> {
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  _buildVoteButton(context);
  }

  MaterialButton _buildVoteButton(BuildContext context) {
    return MaterialButton(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              Icons.comment,
              size: ICON_SIZE,
              color: Colors.green,
            ),
            textButton("Avaliações"),
          ],
        ),
        onPressed: () => validateLoginAction(
            context: context,
            user: _securityService.getSessionUser(),
            cronograma: widget.cronograma,
            successAction: () => avaliarAction(context)));
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: ICON_NAME_SIZE,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        textAlign: TextAlign.center,
      );

  avaliarAction(BuildContext context) async {
    showCustomModalBottomSheet(
        context: context,
        builder: (b) => AvaliacoesList(widget.cronograma),
        containerWidget: (_, animation, child) => FloatingModal(
              child: child,
            ),
        expand: false);
  }
}
