import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/config/error_logger.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/util/constants.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';

class PublishButton extends StatelessWidget {
  final _crontroller = DiaCronogramaModule.to.get<DiaCronogramaController>();
  final Cronograma cronograma;

  PublishButton(this.cronograma);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
      key: Key("publish_schedule_button"),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.publish,
            size: ICON_SIZE,
            color: Colors.green,
          ),
          textButton("Publicar"),
        ],
      ),
      onPressed: () {
        publishCronogramaDialog(
            context,
            "Deseja tornar esse cronograma público?",
            () => publishAction(context));
      },
    );
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: ICON_NAME_SIZE,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        textAlign: TextAlign.center,
      );

  publishAction(BuildContext context) {
    Navigator.of(context).pop();
    showCircularProgress(context);
    _crontroller.publishCronograma(cronograma).then((value) {
      Navigator.of(context).pop();
      onPublishSuccess(context, value);
    }).catchError((exception, stacktrace) {
      Navigator.of(context).pop();
      onError(exception, stacktrace, context);
    });
  }

  void onPublishSuccess(BuildContext context, Cronograma value) {
    getSuccessDialog(
        context: context,
        title: "Parabéns",
        message: "Cronograma publicado com sucesso.",
        action: () => updateCronograma(value))
      ..show();
  }

  updateCronograma(Cronograma cronogramaSaved) {
    cronograma.scheduleCodAPI = cronogramaSaved.scheduleCodAPI;
    cronograma.changePublish(true);
    cronograma.changeImagesUrl(cronogramaSaved.imagesUrl);
  }

  void onError(Object error, StackTrace stackTrace, BuildContext context) {
    ErrorLogger().logInternal(error, stackTrace);
    getErrorDialog(
        context: context,
        title: "Ocorreu um erro!",
        message: "Erro ao publicar o cronograma, tente novamente mais tarde.")
      ..show();
  }
}
