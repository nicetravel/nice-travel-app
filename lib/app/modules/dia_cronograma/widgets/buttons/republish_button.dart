import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/config/error_logger.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/util/constants.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';

class RePublishButton extends StatelessWidget {
  final _crontroller = DiaCronogramaModule.to.get<DiaCronogramaController>();
  final Cronograma cronograma;

  RePublishButton(this.cronograma);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
      key: Key("republish_schedule_button"),
      child: Column(
        children: <Widget>[
          Icon(
            Icons.publish_rounded,
            size: ICON_SIZE,
            color: Colors.green,
          ),
          textButton("Republicar"),
        ],
      ),
      onPressed: () {
        rePublishCronogramaDialog(context, "Deseja republicar esse Cronograma?",
            () => rePublishAction(context));
      },
    );
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: ICON_NAME_SIZE,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        textAlign: TextAlign.center,
      );

  rePublishAction(BuildContext context) {
    Navigator.of(context).pop();
    showCircularProgress(context);
    _crontroller.rePublishCronograma(cronograma).then((value) {
      Navigator.of(context).pop();
      onPublishSuccess(context);
      updateCronograma(value);
    }).catchError((exception, stacktrace) {
      Navigator.of(context).pop();
      onError(exception, stacktrace, context);
    });
  }

  void onPublishSuccess(BuildContext context) {
    getSuccessDialog(
        context: context,
        title: "Parabéns",
        message: "Cronograma republicado com sucesso.")
      ..show();
  }

  updateCronograma(Cronograma cronogramaSaved) {
    cronograma.scheduleCodAPI = cronogramaSaved.scheduleCodAPI;
    cronograma.changePublish(true);
    cronograma.changeImagesUrl(cronogramaSaved.imagesUrl);
  }

  void onError(Object error, StackTrace stackTrace, BuildContext context) {
    ErrorLogger().logInternal(error, stackTrace);
    getErrorDialog(
        context: context,
        title: "Ocorreu um erro!",
        message: "Erro ao republicar o cronograma, tente novamente mais tarde.")
      ..show();
  }
}
