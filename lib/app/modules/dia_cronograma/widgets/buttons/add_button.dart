import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/util/constants.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class AddButton extends StatelessWidget {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();
  final ICloudCronogramaRepository _cronogramaRepository =
      DiaCronogramaModule.to.get<ICloudCronogramaRepository>();
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  final Cronograma cronograma;

  AddButton({Key key, @required this.cronograma})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),
        child: Column(
          children: <Widget>[
            Icon(
              Icons.add_circle,
              size: ICON_SIZE,
              color: Colors.blue,
            ),
            textButton("Novo dia"),
          ],
        ),
        onPressed: () => validateLoginAction(
            cronogramaRepository: _cronogramaRepository,
            context: context,
            user: _securityService.getSessionUser(),
            cronograma: cronograma,
            successAction: () => sendToAtividadePageWithNovoDia(context)));
  }

  Text textButton(String text) => Text(
        text,
        style: TextStyle(
            fontSize: ICON_NAME_SIZE,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        textAlign: TextAlign.center,
      );

  Future sendToAtividadePageWithNovoDia(BuildContext context) async {
    DiaCronograma diaCronograma =
        _controller.createNewDiaCronograma(cronograma);

    Map<String, dynamic> parameters = {
      'diaCronograma': diaCronograma,
      'cronograma': cronograma
    };

    Navigator.of(context).pushNamed(
        AppModule.DIA_CRONOGRAMA_PATH + DiaCronogramaModule.ATIVIDADES_PATH,
        arguments: parameters);
  }
}
