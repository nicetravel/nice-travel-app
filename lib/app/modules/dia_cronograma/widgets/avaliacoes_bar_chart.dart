import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class AvaliacoesBarChart extends StatelessWidget {
  final StarBarElements starBarElements;

  const AvaliacoesBarChart({Key key, @required this.starBarElements})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildDefaultBarChart();
  }

  SfCartesianChart _buildDefaultBarChart() {
    return SfCartesianChart(
      plotAreaBorderWidth: 0,
      primaryXAxis: CategoryAxis(
        majorGridLines: MajorGridLines(width: 0),
      ),
      primaryYAxis: NumericAxis(
          majorGridLines: MajorGridLines(width: 0),
          numberFormat: NumberFormat.compact()),
      series: _getDefaultBarSeries(),
      tooltipBehavior: TooltipBehavior(enable: true),
    );
  }

  List<BarSeries<ChartData, String>> _getDefaultBarSeries() {
    final List<ChartData> chartData = <ChartData>[
      ChartData(
          x: '1 ⭐',
          y: starBarElements.qtdOneStar,
          color: Color.fromARGB(255, 255, 130, 50)),
      ChartData(
          x: '2 ⭐',
          y: starBarElements.qtdTwoStar,
          color: Color.fromARGB(255, 255, 168, 50)),
      ChartData(
          x: '3 ⭐',
          y: starBarElements.qtdThreeStar,
          color: Color.fromARGB(255, 255, 217, 50)),
      ChartData(
          x: '4 ⭐',
          y: starBarElements.qtdFourStar,
          color: Color.fromARGB(255, 174, 216, 130)),
      ChartData(
          x: '5 ⭐',
          y: starBarElements.qtdFiveStar,
          color: Color.fromARGB(255, 121, 201, 150)),
    ];
    return <BarSeries<ChartData, String>>[
      BarSeries<ChartData, String>(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(2), bottomRight: Radius.circular(2)),
          dataSource: chartData,
          xValueMapper: (ChartData sales, _) => sales.x,
          yValueMapper: (ChartData sales, _) => sales.y,
          pointColorMapper: (ChartData sales, _) => sales.color)
    ];
  }
}

class StarBarElements {
  final int qtdOneStar;
  final int qtdTwoStar;
  final int qtdThreeStar;
  final int qtdFourStar;
  final int qtdFiveStar;

  StarBarElements(
      {this.qtdOneStar = 0,
      this.qtdTwoStar = 0,
      this.qtdThreeStar = 0,
      this.qtdFourStar = 0,
      this.qtdFiveStar = 0});

  static StarBarElements fromAvaliacoes(List<Avaliacao> avaliacoes) {
    int qtdOneStar = 0;
    int qtdTwoStar = 0;
    int qtdThreeStar = 0;
    int qtdFourStar = 0;
    int qtdFiveStar = 0;
    avaliacoes.forEach((a) {
      if (a.qtdStar <= 1.9) {
        qtdOneStar += 1;
      } else if (a.qtdStar <= 2.9) {
        qtdTwoStar += 1;
      } else if (a.qtdStar <= 3.9) {
        qtdThreeStar += 1;
      } else if (a.qtdStar <= 4.9) {
        qtdFourStar += 1;
      } else if (a.qtdStar <= 5) {
        qtdFiveStar += 1;
      }
    });

    return StarBarElements(
        qtdOneStar: qtdOneStar,
        qtdTwoStar: qtdTwoStar,
        qtdThreeStar: qtdThreeStar,
        qtdFourStar: qtdFourStar,
        qtdFiveStar: qtdFiveStar);
  }

  getMeanStar() {
    var sumStars = qtdOneStar +
        qtdTwoStar * 2 +
        qtdThreeStar * 3 +
        qtdFourStar * 4 +
        qtdFiveStar * 5;
    var qtdStars =
        qtdOneStar + qtdTwoStar + qtdThreeStar + qtdFourStar + qtdFiveStar;
    return sumStars / qtdStars;
  }
}

class ChartData {
  final dynamic x;
  final num y;
  final Color color;

  ChartData({this.x, this.y, this.color});
}
