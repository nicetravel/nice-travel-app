import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';

class AvaliacaoAction extends StatefulWidget {
  final Cronograma _cronograma;
  final Avaliacao avaliacao;

  const AvaliacaoAction(this._cronograma, {this.avaliacao});

  @override
  _AvaliacaoActionState createState() =>
      _AvaliacaoActionState(currentAvaliacao: this.avaliacao);
}

class _AvaliacaoActionState extends State<AvaliacaoAction> {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();

  final Avaliacao avaliacao;

  _AvaliacaoActionState({Avaliacao currentAvaliacao})
      : this.avaliacao = (currentAvaliacao == null
            ? Avaliacao.newInstance()
            : currentAvaliacao);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RatingBar.builder(
          initialRating: avaliacao.qtdStar,
          minRating: 1,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) => avaliacao.qtdStar = rating,
        ),
        const SizedBox(height: 10.0),
        TextFormField(
          initialValue: avaliacao.description,
          onChanged: (v) => avaliacao.description = v,
          maxLines: 2,
          decoration: InputDecoration(
              labelText: "Descrição", border: OutlineInputBorder()),
          style: TextStyle(fontSize: 18.0),
          textAlign: TextAlign.justify,
        ),
        const SizedBox(height: 10.0),
        MaterialButton(
          key: Key("avaliar_button"),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(22.0)),
          elevation: 18.0,
          color: Theme.of(context).primaryColor,
          clipBehavior: Clip.antiAlias,
          // Add This
          child: new Text('Avaliar',
              style: new TextStyle(fontSize: 16.0, color: Colors.white)),
          onPressed: () => _avaliar(),
        ),
      ],
    );
  }

  _avaliar() async {
    await _controller.avaliar(widget._cronograma, avaliacao);
    Navigator.pop(context);
  }
}
