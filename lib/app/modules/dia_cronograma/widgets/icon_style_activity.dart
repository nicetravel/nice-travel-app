import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

enum Style {
  OTHER,
  BREAK_FAST,
  RESTAURANT,
  BAR,
  PARTY,
  MUSEUM,
  SHOP,
  HISTORICAL_MONUMENT,
  BEACH,
  SWIMMING,
  PARK,
  CHURCH,
  SPORT,
  FITNESS,
  SHOWER,
  BED,
}

class StyleActivity {
  String portugueseLabel;
  String styleLabel;

  StyleActivity(Style style) {
    switch (style) {
      case Style.BAR:
        this.portugueseLabel = "Bar";
        this.styleLabel = 'BAR';
        break;
      case Style.RESTAURANT:
        this.portugueseLabel = "Restaurante";
        this.styleLabel = 'RESTAURANT';
        break;
      case Style.MUSEUM:
        this.portugueseLabel = "Museu";
        this.styleLabel = 'MUSEUM';
        break;
      case Style.SHOP:
        this.portugueseLabel = "Shopping";
        this.styleLabel = 'SHOP';
        break;
      case Style.HISTORICAL_MONUMENT:
        this.portugueseLabel = "Monumento";
        this.styleLabel = 'HISTORICAL MONUMENT';
        break;
      case Style.SWIMMING:
        this.portugueseLabel = "Nadar";
        this.styleLabel = 'SWIMMING';
        break;
      case Style.PARK:
        this.portugueseLabel = "Parque";
        this.styleLabel = 'PARK';
        break;
      case Style.CHURCH:
        this.portugueseLabel = "Igreja";
        this.styleLabel = 'CHURCH';
        break;
      case Style.SPORT:
        this.portugueseLabel = "Esporte";
        this.styleLabel = 'SPORT';
        break;
      case Style.OTHER:
        this.portugueseLabel = "Outro";
        this.styleLabel = 'OTHER';
        break;
      case Style.FITNESS:
        this.portugueseLabel = "Exercícios";
        this.styleLabel = 'FITNESS';
        break;
      case Style.BEACH:
        this.portugueseLabel = "Praia";
        this.styleLabel = 'BEACH';
        break;
      case Style.BREAK_FAST:
        this.portugueseLabel = "Café da manhã";
        this.styleLabel = 'BREAK FAST';
        break;
      case Style.PARTY:
        this.portugueseLabel = "Festa";
        this.styleLabel = 'PARTY';
        break;
      case Style.BED:
        this.portugueseLabel = "Dormir";
        this.styleLabel = 'BED';
        break;
      case Style.SHOWER:
        this.portugueseLabel = "Tomar Banho";
        this.styleLabel = 'SHOWER';
        break;
    }
  }
}

List<StyleActivity> getStyleDescription() {
  return Style.values.map((s) => StyleActivity(s)).toList();
}

class IconStyleActivity {
  Icon icon;
  Color color;
  Color iconColor;
  bool withColor = false;

  IconStyleActivity(String styleActivity, {this.withColor = false, this.iconColor}) {
    Style style = Style.values.firstWhere((s) => s
        .toString()
        .replaceAll("_", " ")
        .toLowerCase()
        .contains(styleActivity.toLowerCase()));
    switch (style) {
      case Style.BED:
        color = Colors.yellow[700];
        icon = Icon(Icons.bedtime_rounded, color: updateIconColor());
        break;
      case Style.SHOWER:
        color = Colors.blue[600];
        icon = Icon(Icons.bathtub_rounded, color: updateIconColor());
        break;
      case Style.BREAK_FAST:
        color = Colors.brown[600];
        icon = Icon(Icons.free_breakfast_rounded, color: updateIconColor());
        break;
      case Style.BAR:
        color = Colors.yellow[600];
        icon = Icon(Icons.sports_bar_rounded, color: updateIconColor());
        break;
      case Style.PARTY:
        color = Colors.deepPurpleAccent;
        icon = Icon(Icons.local_bar_rounded, color: updateIconColor());
        break;
      case Style.RESTAURANT:
        color = Colors.orange;
        icon = Icon(Icons.restaurant_menu_rounded, color: updateIconColor());
        break;
      case Style.MUSEUM:
        color = Colors.brown[400];
        icon = Icon(Icons.museum_rounded, color: updateIconColor());
        break;
      case Style.SHOP:
        color = Colors.purple[400];
        icon = Icon(Icons.shopping_cart_rounded, color: updateIconColor());
        break;
      case Style.HISTORICAL_MONUMENT:
        color = Colors.pinkAccent[100];
        icon = Icon(Icons.camera_alt_rounded, color: updateIconColor());
        break;
      case Style.BEACH:
        color = Colors.blue[100];
        icon = Icon(Icons.beach_access_rounded, color: updateIconColor());
        break;
      case Style.CHURCH:
        color = Colors.blue[300];
        icon = Icon(MdiIcons.church, color: updateIconColor());
        break;
      case Style.SPORT:
        color = Colors.lightGreen;
        icon = Icon(Icons.sports_soccer, color: updateIconColor());
        break;
      case Style.SWIMMING:
        color = Colors.blue[600];
        icon = Icon(MdiIcons.swim, color: updateIconColor());
        break;
      case Style.FITNESS:
        color = Colors.black12;
        icon = Icon(Icons.fitness_center_rounded, color: updateIconColor());
        break;
      case Style.PARK:
        color = Colors.green;
        icon = Icon(Icons.directions_bike_rounded, color: updateIconColor());
        break;
      case Style.OTHER:
        color = Colors.black26;
        icon =
            Icon(Icons.not_listed_location_rounded, color: updateIconColor());
        break;
    }
  }

  Color updateIconColor() => withColor ? color : iconColor;
}
