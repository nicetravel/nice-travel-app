import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/buttons/republish_button.dart';
import 'package:nice_travel/app/shared/widgets/star_count_widget.dart';

import 'buttons/add_button.dart';
import 'buttons/avaliar_button.dart';
import 'buttons/publish_button.dart';
import 'buttons/remove_button.dart';

class ButtonBarScheduleDay extends StatelessWidget {
  final Cronograma _cronograma;
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  ButtonBarScheduleDay(this._cronograma);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            buildQtdStar(),
            AvaliarButton(cronograma: _cronograma),
            Observer(
                builder: (_) => Visibility(
                    visible: !_cronograma.isPublish && isOwner(),
                    child: PublishButton(_cronograma))),
            Visibility(child: AddButton(cronograma: _cronograma)),
            Visibility(
                visible: _cronograma.isPublish &&
                    isOwner() &&
                    _cronograma.scheduleCodAPI != null,
                child: RePublishButton(_cronograma)),
            Visibility(visible: isOwner(), child: RemoveButton(_cronograma)),
          ],
        ),
      ),
    );
  }

  Widget buildQtdStar() {
    return Observer(
      builder: (_) => Visibility(
          visible: _cronograma.findFromAPI,
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: StarCountWidget(numberLike: _cronograma.numberLike),
          )),
    );
  }

  bool isOwner() => _cronograma.isOwner(_securityService.getSessionUser());
}
