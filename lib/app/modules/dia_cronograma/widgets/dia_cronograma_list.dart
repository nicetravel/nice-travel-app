import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/dia_cronograma_details.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class DayScheduleList extends StatefulWidget {
  final Cronograma _cronograma;

  DayScheduleList(this._cronograma);

  @override
  _DayScheduleListState createState() => _DayScheduleListState(_cronograma);
}

class _DayScheduleListState extends State<DayScheduleList> {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();
  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();
  final ICloudCronogramaRepository _cronogramaRepository =
      DiaCronogramaModule.to.get<ICloudCronogramaRepository>();

  final Cronograma _cronograma;

  _DayScheduleListState(this._cronograma);

  @override
  Widget build(BuildContext context) {
    return APIList<ApiResponse<List<DiaCronograma>>>(
        elements: () => _controller.findAll(_cronograma),
        onCompleted: (list) => updateList(list),
        listWidget:
            Observer(builder: (_) => buildList(_controller.diasCronograma)));
  }

  updateList(List<DiaCronograma> diasCronogramas) {
    if (diasCronogramas.isNotEmpty) {
      _controller.fillDiasCronogramas(diasCronogramas);
      diasCronogramas.forEach((dia) => dia.setCronograma(_cronograma));
      _cronograma.scheduleCod = diasCronogramas[0].scheduleCod;
      _cronograma.findFromAPI = true;
    }
  }

  Widget buildList(List<DiaCronograma> diasCronograma) {
    return ReorderableListView(
        children: _createDaysElements(diasCronograma),
        onReorder: (from, to) {
          validateLoginAction(
              cronogramaRepository: _cronogramaRepository,
              context: context,
              user: _securityService.getSessionUser(),
              cronograma: _cronograma,
              successAction: () => _reorder(from, to, diasCronograma));
        });
  }

  List<Widget> _createDaysElements(List<DiaCronograma> diasCronograma) {
    List<Widget> elements = [];
    for (int i = 0; i < diasCronograma.length; i++) {
      var item = diasCronograma[i];
      if (i == diasCronograma.length -1) {
        elements.add(Container(
            key: Key("day_details_${diasCronograma[i].fullID()}"),
            height: 100,
            margin: const EdgeInsets.only(bottom: 60.0),
            child: DayScheduleDetails(
              item,
              _cronograma,
            )));
      } else {
        elements.add(Container(
            key: Key("day_details_${diasCronograma[i].fullID()}"),
            height: 100,
            child: DayScheduleDetails(
              item,
              _cronograma,
            )));
      }
    }
    return elements;
  }

  _reorder(from, to, diasCronograma) {
    if (to == diasCronograma.length) {
      to = diasCronograma.length - 1;
    }
    var idFrom = diasCronograma[from].id;
    var idTo = diasCronograma[to].id;
    _controller.reorderDays(idFrom, idTo);
  }
}
