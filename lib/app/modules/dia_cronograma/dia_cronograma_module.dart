import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cronograma_api_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_local_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_repository_interface.dart';

import 'page/dia_cronograma_controller.dart';
import 'page/dia_cronograma_page.dart';

class DiaCronogramaModule extends ChildModule {
  static const ATIVIDADES_PATH = '/atividades';

  @override
  List<Bind> get binds => [
        Bind<IDiaCronogramaRepository>(
            (i) => DiaCronogramaLocalRepository(i<IDatabase>())),
        Bind<ICloudCronogramaRepository>((b) => CronogramaRepository(
            b<Dio>(),
            b<IDatabase>(),
            b<IMeuCronogramaRepository>(),
            b<ISecurityService>())),
        Bind<DiaCronogramaController>((i) => DiaCronogramaController(
            i<IDiaCronogramaRepository>(),
            i<IMeuCronogramaRepository>(),
            i<ICloudCronogramaRepository>(),
            i<ICloudSearchCronogramaRepository>(),
            i<ISecurityService>())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => DiaCronogramaPage(cronograma: args.data)),
        ModularRouter(ATIVIDADES_PATH, module: AtividadesModule()),
      ];

  static Inject get to => Inject<DiaCronogramaModule>.of();
}
