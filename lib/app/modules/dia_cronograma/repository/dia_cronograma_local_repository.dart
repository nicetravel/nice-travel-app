import 'package:nice_travel/app/config/local_database.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:sqflite/sqflite.dart';

import 'dia_cronograma_repository_interface.dart';

class DiaCronogramaLocalRepository implements IDiaCronogramaRepository {
  List<DiaCronograma> _diasCronograma = [];

  final IDatabase _databaseConfig;

  DiaCronogramaLocalRepository(this._databaseConfig);

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  Future<ApiResponse<List<DiaCronograma>>> findAll(int scheduleCod,
      {bool codFromApi = false}) async {
    if (_diasCronograma.isEmpty) {
      var diasCronogramas = await _databaseConfig
          .fetchDiasCronograma(scheduleCod, codFromApi: codFromApi);
      _diasCronograma.addAll(diasCronogramas);
    }
    return ApiResponse.completed(List.unmodifiable(_diasCronograma));
  }

  @override
  List<DiaCronograma> reorderDays(int from, int to) {
    return _reorder(from, to);
  }

  List<DiaCronograma> _reorder(int from, int to) {
    int indexFrom;
    int indexTo;
    DiaCronograma cronogramaFrom;
    DiaCronograma cronogramaTo;
    _diasCronograma.asMap().forEach((index, element) {
      if (element.id == from) {
        indexFrom = index;
        cronogramaFrom = element;
      }
      if (element.id == to) {
        indexTo = index;
        cronogramaTo = element;
      }
    });
    var diaCronogramaTo = cronogramaTo.day;
    cronogramaTo.day = cronogramaFrom.day;
    _diasCronograma.insert(indexFrom, cronogramaTo);
    _diasCronograma.removeAt(indexFrom + 1);
    cronogramaFrom.day = diaCronogramaTo;
    _diasCronograma.insert(indexTo, cronogramaFrom);
    _diasCronograma.removeAt(indexTo + 1);
    _updateDaysWhenReorder(_diasCronograma);
    return _diasCronograma;
  }

  @override
  Future<int> createDiaCronograma(
      DiaCronograma diaCronograma, int qtdDays, int scheduleCod) async {
    var id = await _saveLocal(diaCronograma, qtdDays, scheduleCod);
    diaCronograma.id = id;
    _diasCronograma.add(diaCronograma);
    return Future.value(id);
  }

  Future<int> _saveLocal(
      DiaCronograma diaCronograma, int qtdDays, int scheduleCod) async {
    final Database db = await _databaseConfig.database;

    db.rawUpdate(
        ' UPDATE ${LocalDatabase.TB_SCHEDULE_TRAVEL} SET NU_DAYS = ? '
        ' WHERE CO_SCHEDULE_TRAVEL = ?',
        [qtdDays, scheduleCod]);

    return db.insert(LocalDatabase.TB_SCHEDULE_DAY, diaCronograma.toMap());
  }

  @override
  Future<void> remove(int scheduleDayCod, int qtdDays, int scheduleCod) async {
    await _deleteLocal(scheduleDayCod, qtdDays, scheduleCod);
    _diasCronograma.remove(DiaCronograma(id: scheduleDayCod));
    _updateDays();
    return Future.value();
  }

  Future<void> _deleteLocal(
      int scheduleDayCod, int qtdDays, int scheduleCod) async {
    final Database db = await _databaseConfig.database;

    db.rawUpdate(
        ' UPDATE ${LocalDatabase.TB_SCHEDULE_TRAVEL} SET NU_DAYS = ? '
        ' WHERE CO_SCHEDULE_TRAVEL = ?',
        [qtdDays, scheduleCod]);

    return db.delete(LocalDatabase.TB_SCHEDULE_DAY,
        where: 'CO_SCHEDULE_DAY = ?', whereArgs: [scheduleDayCod]);
  }

  void _updateDays() async {
    int day = 1;
    final Database db = await _databaseConfig.database;
    _diasCronograma.forEach((diaCronograma) {
      if (day < diaCronograma.day) {
        diaCronograma.setDay(day);
        db.update(LocalDatabase.TB_SCHEDULE_DAY, diaCronograma.toMap(),
            where: 'CO_SCHEDULE_DAY = ?', whereArgs: [diaCronograma.id]);
      }
      day++;
    });
  }

  void _updateDaysWhenReorder(List<DiaCronograma> diasCronograma) async {
    final Database db = await _databaseConfig.database;
    diasCronograma.forEach((diaCronograma) {
      db.execute(
          "UPDATE ${LocalDatabase.TB_SCHEDULE_DAY} SET NU_DAY = ${diaCronograma.day} WHERE CO_SCHEDULE_DAY = ?",
          [diaCronograma.id]);
    });
  }
}
