import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';

class CronogramaAPI {
  final Cronograma _cronograma;
  final List<DiaCronogramaAPI> _diasCronogramas;

  CronogramaAPI(this._cronograma, this._diasCronogramas);

  Map<String, dynamic> toMap() {
    var map = _cronograma.toMapApi();
    var cronogramas = [];
    _diasCronogramas.forEach((cronograma) {
      cronogramas.add(cronograma.toMap());
    });
    map["scheduleDays"] = cronogramas;
    return map;
  }
}

class DiaCronogramaAPI {
  final DiaCronograma _diaCronograma;
  final List<Atividade> _atividades;

  DiaCronogramaAPI(this._diaCronograma, this._atividades);

  Map<String, dynamic> toMap() {
    var map = _diaCronograma.toMapApi();
    var atividades = [];
    _atividades.forEach((atividade) {
      atividades.add(atividade.toMapApi());
    });
    map["activities"] = atividades;
    return map;
  }
}