import 'package:nice_travel/app/util/format_util.dart';

class CronogramaPublishResponse {
  final int scheduleCod;
  final List imagesUrl;
  final List<ScheduleDayResponsePublish> scheduleDays;

  CronogramaPublishResponse(this.scheduleCod, this.scheduleDays,
      this.imagesUrl);

  CronogramaPublishResponse.fromMap(Map<String, dynamic> maps)
      : scheduleCod = maps['scheduleCod'],
        imagesUrl = (maps['imagesUrl'] as List).length > 0
            ? maps['imagesUrl']
            : [
          'https://media.istockphoto.com/vectors/city-silhouette-land-scape-horizontal-city-landscape-downtown-skyline-vector-id959234938?k=6&m=959234938&s=170667a&w=0&h=yjJNnGWHPO23Jxsv2vMsYxbhi3oEEjEwlnM3dUEwBBU='
        ],
        scheduleDays = maps['scheduleDays']
            .map<ScheduleDayResponsePublish>(
                (e) => ScheduleDayResponsePublish.fromMap(e))
            .toList();
}

class ScheduleDayResponsePublish {
  final int scheduleDayId;
  final int day;
  final List<ActivityResponsePublish> activities;

  ScheduleDayResponsePublish(this.scheduleDayId, this.day, this.activities);

  ScheduleDayResponsePublish.fromMap(Map<String, dynamic> maps)
      : scheduleDayId = maps['scheduleDayId'],
        day = maps['day'],
        activities = maps['activities']
            .map<ActivityResponsePublish>(
                (e) => ActivityResponsePublish.fromMap(e))
            .toList();
}

class ActivityResponsePublish {
  final int id;
  final DateTime startActivity;

  ActivityResponsePublish(this.id, this.startActivity);

  String getStartActivity() {
    return formatHoraToString(startActivity);
  }

  ActivityResponsePublish.fromMap(Map<String, dynamic> maps)
      : id = maps['id'],
        startActivity = maps['startActivity'] != null ? formatStringToHora(
            "${maps['startActivity'][0]}:${maps['startActivity'][1]}")
            : null;
}
