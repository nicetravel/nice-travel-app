import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';

class DuplicatedCronogramaAPI {
  final Cronograma cronograma;
  final List<ScheduleDayResponse> scheduleDays;

  DuplicatedCronogramaAPI.fromMap(Map<String, dynamic> maps)
      :  cronograma =  Cronograma.fromMapApi(maps['cronograma']),
        scheduleDays = maps['diasCronogramas']
            .map<ScheduleDayResponse>(
                (e) => ScheduleDayResponse.fromMap(e))
            .toList();
}

class ScheduleDayResponse {
  final int scheduleDayId;
  final int day;
  final List<Atividade> activities;

  ScheduleDayResponse(this.scheduleDayId, this.day, this.activities);

  ScheduleDayResponse.fromMap(Map<String, dynamic> maps)
      : scheduleDayId = maps['scheduleDayId'],
        day = maps['day'],
        activities = maps['activities']
            .map<Atividade>(
                (e) => Atividade.fromMapApi(e))
            .toList();
}
