
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/cronograma.dart';


abstract class ICloudCronogramaRepository implements Disposable {

  Future<Cronograma> rePublish(Cronograma cronograma);

  Future<Cronograma> publish(Cronograma cronograma);

  Future<dynamic> removeCronograma(Cronograma cronograma);

  Future<Cronograma> duplicate(Cronograma cronograma);

  Future<bool> avaliar(Cronograma cronograma, String commentary, double qtdStar);

}
