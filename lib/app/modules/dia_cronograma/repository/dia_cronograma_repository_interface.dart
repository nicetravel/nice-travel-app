import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';

abstract class IDiaCronogramaRepository implements Disposable {

  Future<ApiResponse<List<DiaCronograma>>> findAll(int scheduleCod, { bool codFromApi = false});

  List<DiaCronograma> reorderDays(int from, int to);

  Future<void> remove(int scheduleDayCod, int qtdDays, int scheduleCod);

  Future<int> createDiaCronograma(DiaCronograma diaCronograma, int qtdDays, int scheduleCod);
}
