import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:nice_travel/app/config/api/cronograma_api_connection.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/cronograma_api_resource.dart';

class CronogramaRepository implements ICloudCronogramaRepository {
  final Dio _dio;
  final IDatabase _databaseConfig;
  final IMeuCronogramaRepository _meuCronogramaRepository;
  final ISecurityService _securityService;

  CronogramaRepository(this._dio, this._databaseConfig,
      this._meuCronogramaRepository, this._securityService);

  @override
  Future<Cronograma> publish(Cronograma cronograma) async {
    var diasCronogramas =
        await _databaseConfig.fetchDiasCronograma(cronograma.scheduleCod);

    List<DiaCronogramaAPI> diaCronogramaAPI =
        await getDiasCronogramasAPI(diasCronogramas);

    var cronogramaAPI = CronogramaAPI(cronograma, diaCronogramaAPI);
    var json = jsonEncode(cronogramaAPI.toMap());

    var cronogramaSaved =
        await CronogramaAPIConnection.instance.publishSchedule(_dio, json);

    return _meuCronogramaRepository.publishCronograma(
        cronograma.scheduleCod, cronogramaSaved);
  }


  @override
  Future<Cronograma> rePublish(Cronograma cronograma) async {
    var diasCronogramas =
        await _databaseConfig.fetchDiasCronograma(cronograma.scheduleCod);

    List<DiaCronogramaAPI> diaCronogramaAPI =
        await getDiasCronogramasAPI(diasCronogramas);

    var cronogramaAPI = CronogramaAPI(cronograma, diaCronogramaAPI);
    var json = jsonEncode(cronogramaAPI.toMap());

    var cronogramaSaved =
        await CronogramaAPIConnection.instance.rePublishSchedule(cronograma.scheduleCodAPI, _dio, json);

    return _meuCronogramaRepository.publishCronograma(
        cronograma.scheduleCod, cronogramaSaved);
  }

  @override
  Future<Cronograma> duplicate(Cronograma cronograma) async {
    var cronogramaDuplicated = await CronogramaAPIConnection.instance
        .duplicateSchedule(
            _dio, cronograma.scheduleCodAPI, _securityService.getSessionUser());

    return _meuCronogramaRepository.duplicateCronograma(cronogramaDuplicated);
  }

  @override
  Future<bool> avaliar(Cronograma cronograma, String commentary, double qtdStar) async {
    return await CronogramaAPIConnection.instance
        .avaliar(_dio, cronograma.scheduleCodAPI, _securityService.getSessionUser(), commentary, qtdStar);
  }

  @override
  Future<dynamic> removeCronograma(Cronograma cronograma) async {
    await CronogramaAPIConnection.instance
        .deleteSchedule(_dio, cronograma.scheduleCodAPI);
    await _meuCronogramaRepository.remove(cronograma.scheduleCod);
  }

  Future<List<DiaCronogramaAPI>> getDiasCronogramasAPI(
      List<DiaCronograma> diasCronogramas) async {
    List<DiaCronogramaAPI> diasCronogramasAPI = [];
    for (var diaCronograma in diasCronogramas) {
      var atividades = await _databaseConfig.fetchAtividades(diaCronograma.id);
      diasCronogramasAPI.add(DiaCronogramaAPI(diaCronograma, atividades));
    }

    return diasCronogramasAPI;
  }

  @override
  void dispose() {
    //dispose will be called automatically
  }
}
