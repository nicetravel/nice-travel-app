import 'package:flutter/material.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/animated_background.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/button_bar_schedule_day.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/carousel_travel_photos.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/dia_cronograma_list.dart';

class DiaCronogramaPage extends StatelessWidget {
  final Cronograma cronograma;
  final double _heightAppBar = 240;
  final double _heightBottomButtons = 70;

  DiaCronogramaPage({Key key, @required this.cronograma}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: AnimatedBackgroundWidget(
        child: Stack(
          children: <Widget>[
            _imageAppBar(context),
            _cityCardContainer(context),
            _cityInfoWidget(context),
          ],
        ),
      ),
    ));
  }

  Widget _cityCardContainer(BuildContext context) {
    var height = MediaQuery.of(context).size.height - _heightAppBar;
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(60), topRight: Radius.circular(60)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.white30.withOpacity(0.3),
            Colors.grey[400],
            Colors.blue[100]
          ],
          stops: [0, 0.07, 0.7],
        ),
      ),
      height: height + 15,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: _heightAppBar - 15),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 15.0,
            ),
            Expanded(
              child: Container(
                  height: height - _heightBottomButtons - 0,
                  child: DayScheduleList(cronograma)),
            ),
          ],
        ),
      ),
    );
  }

  Widget _imageAppBar(BuildContext context) {
    return CarouselTravelPhotos(
        cronograma: cronograma, heightAppBar: _heightAppBar);
  }

  Widget _cityInfoWidget(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(left: 5.0, right: 5.0, bottom: 10),
      child: Container(
          padding: const EdgeInsets.only(top: 8.0, bottom: 5.0, right: 3.0),
          margin: EdgeInsets.only(top: height - 72),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.white24.withOpacity(0.7)),
          child: ButtonBarScheduleDay(cronograma)),
    );
  }
}
