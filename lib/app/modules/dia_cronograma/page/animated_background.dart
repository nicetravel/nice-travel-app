import 'package:animated_background/animated_background.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';

class AnimatedBackgroundWidget extends StatefulWidget {
  AnimatedBackgroundWidget({Key key, this.child}) : super(key: key);

  final Widget child;

  @override
  _AnimatedBackgroundWidgetState createState() =>
      new _AnimatedBackgroundWidgetState();
}

class _AnimatedBackgroundWidgetState extends State<AnimatedBackgroundWidget>
    with TickerProviderStateMixin {
  final DiaCronogramaController _controller =
      DiaCronogramaModule.to.get<DiaCronogramaController>();

  ParticleOptions particleOptions = ParticleOptions(
    image: Image.asset('assets/images/star_stroke.png'),
    baseColor: Colors.blue,
    spawnOpacity: 0.0,
    opacityChangeRate: 0.25,
    minOpacity: 1.0,
    maxOpacity: 1.0,
    spawnMinSpeed: 60.0,
    spawnMaxSpeed: 90.0,
    spawnMinRadius: 15.0,
    spawnMaxRadius: 35.0,
    particleCount: 50,
  );

  var particlePaint = Paint()
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1.0;

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) => (AnimatedBackground(
          behaviour:
              _controller.showAnimation ? _buildBehaviour() : EmptyBehaviour(),
          vsync: this,
          child: AnimatedOpacity(
              onEnd: _hideAnimation(),
              opacity: _controller.showAnimation ? 0.7 : 1.0,
              duration: Duration(milliseconds: 1000),
              child: widget.child))),
    );
  }

  Behaviour _buildBehaviour() {
    return RandomParticleBehaviour(
      options: particleOptions,
      paint: particlePaint,
    );
  }

  _hideAnimation() {
    if (_controller.showAnimation) {
      Future.delayed(Duration(milliseconds: 3000),
          () => _controller.hideAnimation());
    }
  }
}
