import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/repository/atividade_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

part 'atividades_controller.g.dart';

class AtividadesController = _AtividadesControllerBase
    with _$AtividadesController;

abstract class _AtividadesControllerBase with Store {
  final IAtividadeRepository _repository;
  final ICloudSearchCronogramaRepository _searchCloudRepository;
  final ISecurityService _securityService;

  @observable
  List<Atividade> atividades = ObservableList<Atividade>();

  _AtividadesControllerBase(
      this._repository, this._searchCloudRepository, this._securityService);

  void fillAtividades(List<Atividade> atividades) {
    this.atividades = atividades.asObservable();
  }

  Future<ApiResponse<List<Atividade>>> findAll(
      int scheduleDayId, Cronograma cronograma) {
    if (cronograma.isOwner(_securityService.getSessionUser())) {
      return _repository.findAll(scheduleDayId);
    }
    return _searchCloudRepository.findAllAtividades(scheduleDayId);
  }

  Future<void> deleteAtividade(Atividade atividade) {
    return _repository.remove(atividade);
  }

  void deleteLocal(Atividade atividade) {
    atividades.remove(atividade);
  }

  Future<int> saveAtividade(Atividade atividade) {
    return _repository.saveOrUpdate(atividade);
  }

  void saveOrUpdateLocal(Atividade atividade, int id) {
    if (atividades.contains(atividade)) {
      atividades.remove(atividade);
    }
    atividades.add(atividade);
    atividades
        .sort((a, b) => a.startActivityDate.compareTo(b.startActivityDate));
  }

  String getFirstActivityType() {
    if (atividades.isNotEmpty) {
      return atividades.first.styleActivity;
    }
    return "OTHER";
  }

  DateTime getLastEndActivityTime(){
    if(atividades.isEmpty){
      return DateTime(2000, 1, 1);
    }
    return atividades[atividades.length -1].finishActivityDate;
  }
}
