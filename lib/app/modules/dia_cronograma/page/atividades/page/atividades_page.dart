import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/atividade_card.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/new_atividade_button.dart';
import 'package:nice_travel/app/modules/dia_cronograma/widgets/icon_style_activity.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:nice_travel/app/shared/custom_app_bar.dart';
import 'package:nice_travel/app/shared/widgets/api_list.dart';
import 'package:timeline_tile/timeline_tile.dart';

import 'atividades_controller.dart';

class AtividadesPage extends StatefulWidget {
  final DiaCronograma diaCronograma;
  final Cronograma cronograma;

  const AtividadesPage(
      {Key key, @required this.diaCronograma, @required this.cronograma})
      : super(key: key);

  @override
  _AtividadesPageState createState() => _AtividadesPageState();
}

class _AtividadesPageState
    extends ModularState<AtividadesPage, AtividadesController> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        widget.diaCronograma
            .setTypeFirstActivity(controller.getFirstActivityType());
        return Future.value(true);
      },
      child: Scaffold(
        appBar: _buildAppBar(context),
        body: APIList<ApiResponse<List<Atividade>>>(
            elements: () =>
                controller.findAll(widget.diaCronograma.getUniqueId, widget.cronograma),
            onCompleted: (list) => controller.fillAtividades(list),
            listWidget:
                Observer(builder: (_) => buildList(controller.atividades))),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return CustomAppBar(
      title: '${widget.diaCronograma.day}º Dia ',
      actions: <Widget>[
        NewAtividadeButton(widget.cronograma, widget.diaCronograma)
      ],
    );
  }

  Widget buildList(List<Atividade> atividades) {
    if (atividades.length > 0) {
      return SingleChildScrollView(
          child:
              Column(children: atividades.map((e) => _timeline(e)).toList()));
    } else {
      return new Container(
        child: Center(
            child: Text(
          "Nenhuma atividade cadastrada ainda :(",
          style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        )),
      );
    }
  }

  TimelineTile _timeline(Atividade atividade) {
    return TimelineTile(
      alignment: TimelineAlign.manual,
      lineXY: 0.1,
      indicatorStyle: IndicatorStyle(
          height: 40,
          width: 50,
          indicator: CircleAvatar(
              backgroundColor: IconStyleActivity(atividade.styleActivity).color,
              child: IconStyleActivity(atividade.styleActivity,
                      iconColor: Theme.of(context).brightness == Brightness.dark
                          ? Colors.white
                          : Colors.black)
                  .icon)),
      endChild: Padding(
        padding: const EdgeInsets.all(5.0),
        child: AtividadeCard(
          cronograma: widget.cronograma,
          diaCronograma: widget.diaCronograma,
          atividade: atividade,
        ),
      ),
    );
  }
}
