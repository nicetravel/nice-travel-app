import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/atividades_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/repository/atividade_local_repository.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/repository/atividade_repository_interface.dart';

import 'page/atividades_controller.dart';

class AtividadesModule extends ChildModule {
  static const ATIVIDADE_PATH = '/atividade';

  @override
  List<Bind> get binds => [
        Bind<IAtividadeRepository>(
            (i) => AtividadeLocalRepository(i<IDatabase>())),
    Bind<AtividadesController>((i) => AtividadesController(
        i<IAtividadeRepository>(),
        i<ICloudSearchCronogramaRepository>(),
        i<ISecurityService>())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => AtividadesPage(
                cronograma: args.data['cronograma'],
                diaCronograma: args.data['diaCronograma'])),
      ];

  static Inject get to => Inject<AtividadesModule>.of();
}
