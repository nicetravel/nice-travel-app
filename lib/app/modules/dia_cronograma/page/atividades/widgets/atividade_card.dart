import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/nova_atividade/nova_atividade_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/floating_modal.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/util/format_util.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class AtividadeCard extends StatelessWidget {
  final Cronograma cronograma;
  final DiaCronograma diaCronograma;
  final Atividade atividade;

  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  final ICloudCronogramaRepository _cronogramaRepository =
      DiaCronogramaModule.to.get<ICloudCronogramaRepository>();

  AtividadeCard(
      {Key key,
      @required this.cronograma,
      @required this.diaCronograma,
      @required this.atividade})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return GestureDetector(
      onTap: () => validateLoginAction(
          cronogramaRepository: _cronogramaRepository,
          context: context,
          user: _securityService.getSessionUser(),
          cronograma: cronograma,
          successAction: () => _sendToEditAtividade(context)),
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 16.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                  "${formatHoraBetweenToString(atividade.startActivityDate, atividade.finishActivityDate)}",
                  style: textTheme.caption),
              Flexible(
                child: Text(
                  atividade.nameOfPlace,
                  style: textTheme.headline6,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Flexible(
                child: Text(
                  'R\$: ${formatMoney(atividade.price)}',
                  style: TextStyle(color: Colors.green, fontSize: 13),
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _sendToEditAtividade(BuildContext context) {
    showCustomModalBottomSheet(
        context: context,
        builder: (b) => NovaAtividadePage(
            diaCronograma: diaCronograma, atividade: atividade),
        containerWidget: (_, animation, child) => FloatingModal(
              child: child,
            ),
        expand: false);
  }
}
