import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/atividades_module.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/atividades_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/page/nova_atividade/nova_atividade_page.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/atividades/widgets/floating_modal.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/util/validate_login_action.dart';

class NewAtividadeButton extends StatelessWidget {
  final Cronograma cronograma;
  final DiaCronograma diaCronograma;

  final ISecurityService _securityService =
      AppModule.to.get<ISecurityService>();

  final AtividadesController controller =
      AtividadesModule.to.get<AtividadesController>();

  final ICloudCronogramaRepository _cronogramaRepository =
      DiaCronogramaModule.to.get<ICloudCronogramaRepository>();

  NewAtividadeButton(this.cronograma, this.diaCronograma);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => validateLoginAction(
          cronogramaRepository: _cronogramaRepository,
          context: context,
          user: _securityService.getSessionUser(),
          cronograma: cronograma,
          successAction: () => _sendToNovaAtividade(context)),
      icon: Icon(Icons.add_circle_outlined),
    );
  }

  _sendToNovaAtividade(BuildContext context) {
    showCustomModalBottomSheet(
        context: context,
        builder: (b) => NovaAtividadePage(
            diaCronograma: diaCronograma,
            atividade: Atividade.newInstance(diaCronograma.id,
                startActivityDate: controller.getLastEndActivityTime())),
        containerWidget: (_, animation, child) => FloatingModal(
              child: child,
            ),
        expand: false);
  }
}
