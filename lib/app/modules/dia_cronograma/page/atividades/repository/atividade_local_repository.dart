import 'package:nice_travel/app/config/local_database.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:sqflite/sqflite.dart';

import 'atividade_repository_interface.dart';

class AtividadeLocalRepository implements IAtividadeRepository {
  List<Atividade> _atividades = [];

  final IDatabase _databaseConfig;

  AtividadeLocalRepository(this._databaseConfig);

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  Future<ApiResponse<List<Atividade>>> findAll(int scheduleDayId) async {
    if (_atividades.isEmpty) {
      var atividades = await _databaseConfig.fetchAtividades(scheduleDayId);
      _atividades.addAll(atividades);
    }

    _atividades
        .sort((a, b) => a.startActivityDate.compareTo(b.startActivityDate));
    return ApiResponse.completed(List.unmodifiable(_atividades));
  }

  @override
  Future<void> remove(Atividade atividade) {
    _atividades.remove(atividade);
    _deleteLocal(atividade.id);
    return Future.value();
  }

  void _deleteLocal(int atividadeCod) async {
    final Database db = await _databaseConfig.database;
    db.delete(LocalDatabase.TB_ACTIVITY,
        where: 'CO_ACTIVITY= ?', whereArgs: [atividadeCod]);
  }

  @override
  Future<int> saveOrUpdate(Atividade atividade) async {
    var id;
    if (atividade.id == null) {
       id = await _saveLocal(atividade);
      _atividades.add(atividade);
    } else {
      _atividades.remove(atividade);
      _atividades.add(atividade);
      _updateLocal(atividade);
      id = atividade.id;
    }
    return Future.value(id);
  }

  void _updateLocal(Atividade atividade) async {
    final Database db = await _databaseConfig.database;
    db.update(LocalDatabase.TB_ACTIVITY, atividade.toMap(),
        where: 'CO_ACTIVITY = ?', whereArgs: [atividade.id]);
  }

  Future<int> _saveLocal(Atividade atividade) async {
    final Database db = await _databaseConfig.database;
    return db.insert(LocalDatabase.TB_ACTIVITY, atividade.toMap());
  }
}
