// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dia_cronograma_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DiaCronogramaController on _DiaCronogramaControllerBase, Store {
  final _$diasCronogramaAtom =
      Atom(name: '_DiaCronogramaControllerBase.diasCronograma');

  @override
  List<DiaCronograma> get diasCronograma {
    _$diasCronogramaAtom.reportRead();
    return super.diasCronograma;
  }

  @override
  set diasCronograma(List<DiaCronograma> value) {
    _$diasCronogramaAtom.reportWrite(value, super.diasCronograma, () {
      super.diasCronograma = value;
    });
  }

  final _$avaliacoesAtom =
      Atom(name: '_DiaCronogramaControllerBase.avaliacoes');

  @override
  List<Avaliacao> get avaliacoes {
    _$avaliacoesAtom.reportRead();
    return super.avaliacoes;
  }

  @override
  set avaliacoes(List<Avaliacao> value) {
    _$avaliacoesAtom.reportWrite(value, super.avaliacoes, () {
      super.avaliacoes = value;
    });
  }

  final _$_showAnimationAtom =
      Atom(name: '_DiaCronogramaControllerBase._showAnimation');

  @override
  bool get _showAnimation {
    _$_showAnimationAtom.reportRead();
    return super._showAnimation;
  }

  @override
  set _showAnimation(bool value) {
    _$_showAnimationAtom.reportWrite(value, super._showAnimation, () {
      super._showAnimation = value;
    });
  }

  final _$_hasVotedAtom = Atom(name: '_DiaCronogramaControllerBase._hasVoted');

  @override
  bool get _hasVoted {
    _$_hasVotedAtom.reportRead();
    return super._hasVoted;
  }

  @override
  set _hasVoted(bool value) {
    _$_hasVotedAtom.reportWrite(value, super._hasVoted, () {
      super._hasVoted = value;
    });
  }

  final _$_DiaCronogramaControllerBaseActionController =
      ActionController(name: '_DiaCronogramaControllerBase');

  @override
  void _changeShowAnimation(bool _showAnimation) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase._changeShowAnimation');
    try {
      return super._changeShowAnimation(_showAnimation);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeHasVoted(bool hasVoted) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.changeHasVoted');
    try {
      return super.changeHasVoted(hasVoted);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void hideAnimation() {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.hideAnimation');
    try {
      return super.hideAnimation();
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fillDiasCronogramas(List<DiaCronograma> diasCronograma) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.fillDiasCronogramas');
    try {
      return super.fillDiasCronogramas(diasCronograma);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void fillAvaliacoes(List<Avaliacao> avaliacoes) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.fillAvaliacoes');
    try {
      return super.fillAvaliacoes(avaliacoes);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addAvaliacao(Avaliacao avaliacao) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.addAvaliacao');
    try {
      return super.addAvaliacao(avaliacao);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addDiaCronograma(DiaCronograma diaCronograma) {
    final _$actionInfo = _$_DiaCronogramaControllerBaseActionController
        .startAction(name: '_DiaCronogramaControllerBase.addDiaCronograma');
    try {
      return super.addDiaCronograma(diaCronograma);
    } finally {
      _$_DiaCronogramaControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
diasCronograma: ${diasCronograma},
avaliacoes: ${avaliacoes}
    ''';
  }
}
