import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/dia_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

part 'dia_cronograma_controller.g.dart';

class DiaCronogramaController = _DiaCronogramaControllerBase
    with _$DiaCronogramaController;

abstract class _DiaCronogramaControllerBase with Store {
  final IDiaCronogramaRepository _repository;
  final IMeuCronogramaRepository _meuCronogramaRepository;
  final ICloudCronogramaRepository _cloudRepository;
  final ICloudSearchCronogramaRepository _searchCloudRepository;
  final ISecurityService _securityService;

  _DiaCronogramaControllerBase(this._repository,
      this._meuCronogramaRepository,
      this._cloudRepository,
      this._searchCloudRepository,
      this._securityService);

  @observable
  List<DiaCronograma> diasCronograma = ObservableList<DiaCronograma>();

  @observable
  List<Avaliacao> avaliacoes = ObservableList<Avaliacao>();

  @observable
  bool _showAnimation = false;

  @observable
  bool _hasVoted = false;

  bool get showAnimation => _showAnimation;

  @action
  void _changeShowAnimation(bool _showAnimation) {
    this._showAnimation = _showAnimation;
  }

  @action
  void changeHasVoted(bool hasVoted) {
    this._hasVoted = hasVoted;
  }

  @action
  void hideAnimation() {
    this._showAnimation = false;
  }

  @action
  void fillDiasCronogramas(List<DiaCronograma> diasCronograma) {
    this.diasCronograma = diasCronograma.asObservable();
  }

  @action
  void fillAvaliacoes(List<Avaliacao> avaliacoes) {
    if(this.avaliacoes.isEmpty) {
      this.avaliacoes = avaliacoes.asObservable();
    }
  }

  @action
  void addAvaliacao(Avaliacao avaliacao) {
    this.avaliacoes.insert(0, avaliacao);
  }

  @action
  void addDiaCronograma(DiaCronograma diaCronograma) {
    this.diasCronograma.add(diaCronograma);
  }

  bool get hasVoted => _hasVoted;

  Future<ApiResponse<List<DiaCronograma>>> findAll(Cronograma cronograma) {
    if (cronograma.isOwner(_securityService.getSessionUser())) {
      if (cronograma.isPublish) {
        return _repository.findAll(cronograma.scheduleCodAPI, codFromApi: true);
      }
      return _repository.findAll(cronograma.scheduleCod);
    }
    return _searchCloudRepository
        .findDiasCronogramas(cronograma.scheduleCodAPI);
  }

  Future deleteDiaCronograma(int diaCronogramaCod, Cronograma cronograma) {
    return _repository.remove(
        diaCronogramaCod, diasCronograma.length - 1, cronograma.scheduleCod);
  }

  Future deleteCronograma(Cronograma cronograma) async {
    if (cronograma.scheduleCodAPI != null) {
      return _cloudRepository.removeCronograma(cronograma);
    } else {
      return _meuCronogramaRepository.remove(cronograma.scheduleCod);
    }
  }

  void deleteInternal(int scheduleDayCod) {
    diasCronograma.removeWhere((element) => element.id == scheduleDayCod);
    updateDays();
  }

  void updateDays() {
    int day = 1;
    diasCronograma.forEach((element) {
      if (day < element.day) {
        element.setDay(day);
      }
      day++;
    });
  }

  Future<bool> avaliar(Cronograma cronograma, Avaliacao avaliacao) async {
    var isNewVote =
    await _cloudRepository.avaliar(
        cronograma, avaliacao.description, avaliacao.qtdStar);
    if (isNewVote) {
      if (avaliacao.qtdStar >= 3) {
        cronograma.updateNumberStar(true);
      }
      avaliacao.userUid = _securityService.getSessionUser().uid;
      addAvaliacao(avaliacao);
    } else {
      updateAvaliacao(avaliacao);
    }
    changeHasVoted(true);
    _changeShowAnimation(true);
    return true;
  }


  void updateAvaliacao(Avaliacao avaliacao) {
    avaliacoes
        .where((element) => element.userUid == avaliacao.userUid)
        .forEach((element) {
      element.description = avaliacao.description;
      element.qtdStar = avaliacao.qtdStar;
    });
  }

  DiaCronograma createNewDiaCronograma(Cronograma cronograma) {
    final day = this.diasCronograma.length + 1;
    var diaCronograma = DiaCronograma.newInstance(day, cronograma);
    addDiaCronograma(diaCronograma);
    _repository
        .createDiaCronograma(diaCronograma, day, cronograma.scheduleCod)
        .then((value) =>
    diasCronograma
        .firstWhere((element) => element.day == day)
        .id = value);
    cronograma.changeQtdDays(day);
    return diaCronograma;
  }

  void reorderDays(int from, int to) {
    var diasCronogramaReordenados = _repository.reorderDays(from, to);
    fillDiasCronogramas(diasCronogramaReordenados);
  }

  Future<Cronograma> publishCronograma(Cronograma cronograma) {
    return _cloudRepository.publish(cronograma);
  }

  Future<Cronograma> rePublishCronograma(Cronograma cronograma) {
    return _cloudRepository.rePublish(cronograma);
  }

  Future<ApiResponse<List<Avaliacao>>> findAllAvaliacoes(int _cronogramaId) {
    if (avaliacoes.isEmpty) {
      //TODO implementar lógica para buscar as avaliações de 20 em 20.
      return _searchCloudRepository.findAllAvaliacoes(_cronogramaId, 99999);
    }
    return Future.value(ApiResponse.completed(avaliacoes));
  }
}
