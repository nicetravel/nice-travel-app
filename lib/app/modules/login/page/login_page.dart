import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:nice_travel/app/config/error_logger.dart';

import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // Firebase
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/boipeba.png"),
            fit: BoxFit.cover,
          ),
        ),
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 35,
            ),
            roundFlatButton(
                MdiIcons.google, "Continuar com Google", Colors.red),
          ],
        ),
      ),
    );
  }

  Future<Null> _signIn() async {
    GoogleSignInAccount user = _googleSignIn.currentUser;

    final onError = (exception, stacktrace) {
      ErrorLogger().log(exception, stacktrace);
      _showErrorSnackbar(
          "Não foi possível realizar o login. Tente novamente mais tarde!");
      user = null;
    };

    if (user == null) {
      _googleSignIn.signIn().catchError(onError).then((userAuthenticated) {
        signInWithCredentials(userAuthenticated, onError);
      });
    }
    return null;
  }

  Future signInWithCredentials(GoogleSignInAccount userAuthenticated,
      onError(dynamic exception, dynamic stacktrace)) async {
    if (userAuthenticated != null) {
      final GoogleSignInAuthentication googleAuth =
          await userAuthenticated.authentication;
      UserCredential _firebaseUser = await _auth
          .signInWithCredential(GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          ))
          .catchError(onError);
      controller.signIn(_firebaseUser.user);
      Modular.navigator.pop();
    } else {
      ErrorLogger().log('Login Cancelado', 'Cancelamento feito pelo usuário');
    }
  }

  _showErrorSnackbar(String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(message)),
    );
  }

  Widget roundFlatButton(IconData icon, String text, Color cor) {
    return Container(
      width: 300,
      height: 65,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: cor,
      ),
      child: TextButton(
          style: TextButton.styleFrom(
            primary: Colors.white,
          ),
          onPressed: () => _signIn(),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(icon),
              Container(
                width: 10,
              ),
              Flexible(
                child: Text(
                  text,
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              )
            ],
          )),
    );
  }
}
