import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';

part 'login_controller.g.dart';

@Injectable()
class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {
  final ISecurityService _securityService;
  final IDatabase _localDatabase;

  _LoginControllerBase(this._securityService, this._localDatabase);

  signIn(User user) async {
    await _securityService.signIn(user);
    synchronizedFromApi();
  }

  Future<bool> synchronizedFromApi() async {
    var sessionUser = _securityService.getSessionUser();
    if (sessionUser != null) {
      return await _localDatabase.synchronizedFromApi(sessionUser);
    }
    return false;
  }
}
