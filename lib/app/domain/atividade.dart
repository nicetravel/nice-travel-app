import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/util/format_util.dart';

part 'atividade.g.dart';

class Atividade = BaseAtividade with _$Atividade;

abstract class BaseAtividade with Store {
  @observable
  String nameOfPlace;

  String description;
  double price;
  String styleActivity;
  DateTime startActivityDate;
  DateTime finishActivityDate;
  int idScheduleDay;
  int id;
  int idApi;

  BaseAtividade(
      {this.description,
      this.nameOfPlace,
      this.price,
      this.styleActivity,
      this.startActivityDate,
      this.finishActivityDate,
      this.idScheduleDay,
      this.id});

  BaseAtividade.newInstance(int idScheduleDay, {startActivityDate}) {
    final currentDate = DateTime(2000, 1, 1, 23);
    this.idScheduleDay = idScheduleDay;
    this.price = 0.0;
    this.styleActivity = "OTHER";
    this.startActivityDate = startActivityDate ?? currentDate;
    this.finishActivityDate = DateTime(this.startActivityDate.year, this.startActivityDate.month, this.startActivityDate.day, 23);
    this.description = '';
    this.nameOfPlace = '';
  }

  BaseAtividade.fromMap(Map<String, dynamic> maps)
      : description = maps['DS_DESCRIBE'],
        nameOfPlace = maps['DS_NAME'],
        price = maps['VL_PRICE'],
        styleActivity = maps['DS_TYPE_ACTIVITY'],
        startActivityDate = formatStringToHora(maps['DT_START']),
        finishActivityDate = formatStringToHora(maps['DT_END']),
        idScheduleDay = maps['CO_SCHEDULE_DAY'],
        id = maps['CO_ACTIVITY'];

  BaseAtividade.fromMapApi(Map<String, dynamic> maps)
      : description = maps['description'],
        nameOfPlace = maps['nameOfPlace'],
        price = maps['price'],
        styleActivity = maps['styleActivity'],
        startActivityDate = formatStringToHora(
            "${maps['startActivity'][0]}:${maps['startActivity'][1]}"),
        finishActivityDate = formatStringToHora(
            "${maps['finishActivity'][0]}:${maps['finishActivity'][1]}"),
        idScheduleDay = maps['idScheduleDay'],
        idApi = maps['id'];

  Map<String, dynamic> toMap() {
    return {
      'DS_DESCRIBE': description,
      'DS_NAME': nameOfPlace,
      'VL_PRICE': price,
      'DS_TYPE_ACTIVITY': styleActivity,
      'DT_START': formatHoraToString(startActivityDate),
      'DT_END': formatHoraToString(finishActivityDate),
      'CO_SCHEDULE_DAY': idScheduleDay,
    };
  }

  Map<String, dynamic> toMapApi() {
    return {
      'description': description,
      'nameOfPlace': nameOfPlace,
      'price': price,
      'styleActivity': styleActivity,
      'startActivity': formatHoraToString(startActivityDate),
      'finishActivity': formatHoraToString(finishActivityDate),
      'idScheduleDay': idScheduleDay,
      'id': idApi,
    };
  }

  setDescription(String description) {
    this.description = description;
  }

  @action
  setNameOfPlace(String nameOfPlace) {
    this.nameOfPlace = nameOfPlace;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Atividade && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}
