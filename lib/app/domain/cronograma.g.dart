// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cronograma.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Cronograma on CronogramaBase, Store {
  final _$qtdDaysAtom = Atom(name: 'CronogramaBase.qtdDays');

  @override
  int get qtdDays {
    _$qtdDaysAtom.reportRead();
    return super.qtdDays;
  }

  @override
  set qtdDays(int value) {
    _$qtdDaysAtom.reportWrite(value, super.qtdDays, () {
      super.qtdDays = value;
    });
  }

  final _$numberLikeAtom = Atom(name: 'CronogramaBase.numberLike');

  @override
  int get numberLike {
    _$numberLikeAtom.reportRead();
    return super.numberLike;
  }

  @override
  set numberLike(int value) {
    _$numberLikeAtom.reportWrite(value, super.numberLike, () {
      super.numberLike = value;
    });
  }

  final _$isPublishAtom = Atom(name: 'CronogramaBase.isPublish');

  @override
  bool get isPublish {
    _$isPublishAtom.reportRead();
    return super.isPublish;
  }

  @override
  set isPublish(bool value) {
    _$isPublishAtom.reportWrite(value, super.isPublish, () {
      super.isPublish = value;
    });
  }

  final _$imagesUrlAtom = Atom(name: 'CronogramaBase.imagesUrl');

  @override
  List<dynamic> get imagesUrl {
    _$imagesUrlAtom.reportRead();
    return super.imagesUrl;
  }

  @override
  set imagesUrl(List<dynamic> value) {
    _$imagesUrlAtom.reportWrite(value, super.imagesUrl, () {
      super.imagesUrl = value;
    });
  }

  final _$removedAtom = Atom(name: 'CronogramaBase.removed');

  @override
  bool get removed {
    _$removedAtom.reportRead();
    return super.removed;
  }

  @override
  set removed(bool value) {
    _$removedAtom.reportWrite(value, super.removed, () {
      super.removed = value;
    });
  }

  final _$priceFinalAtom = Atom(name: 'CronogramaBase.priceFinal');

  @override
  double get priceFinal {
    _$priceFinalAtom.reportRead();
    return super.priceFinal;
  }

  @override
  set priceFinal(double value) {
    _$priceFinalAtom.reportWrite(value, super.priceFinal, () {
      super.priceFinal = value;
    });
  }

  final _$CronogramaBaseActionController =
      ActionController(name: 'CronogramaBase');

  @override
  void changePublish(bool isPublish) {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.changePublish');
    try {
      return super.changePublish(isPublish);
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changePrice(double priceFinal) {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.changePrice');
    try {
      return super.changePrice(priceFinal);
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateNumberStar(bool positiveVote) {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.updateNumberStar');
    try {
      return super.updateNumberStar(positiveVote);
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeQtdDays(int qtdDays) {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.changeQtdDays');
    try {
      return super.changeQtdDays(qtdDays);
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void subtractQtdDays() {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.subtractQtdDays');
    try {
      return super.subtractQtdDays();
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void changeImagesUrl(List<dynamic> imagesUrl) {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.changeImagesUrl');
    try {
      return super.changeImagesUrl(imagesUrl);
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setRemoved() {
    final _$actionInfo = _$CronogramaBaseActionController.startAction(
        name: 'CronogramaBase.setRemoved');
    try {
      return super.setRemoved();
    } finally {
      _$CronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
qtdDays: ${qtdDays},
numberLike: ${numberLike},
isPublish: ${isPublish},
imagesUrl: ${imagesUrl},
removed: ${removed},
priceFinal: ${priceFinal}
    ''';
  }
}
