import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/cronograma.dart';

part 'dia_cronograma.g.dart';

class DiaCronograma = DiaCronogramaBase with _$DiaCronograma;

abstract class DiaCronogramaBase with Store {
  @observable
  int day;

  @observable
  double priceDay;

  int id;
  int idApi;
  int scheduleCod;
  @observable
  int qtdActivities;

  @observable
  String typeFirstActivity;
  Cronograma _cronograma;

  @action
  void setTypeFirstActivity(String typeFirstActivity){
    this.typeFirstActivity = typeFirstActivity;
  }

  String fullID(){
    return '$idApi\_$id';
  }

  int get getUniqueId => id != null ? id : idApi;

  DiaCronogramaBase(
      {@required this.day,
      @required this.priceDay,
      @required this.id,
      @required this.scheduleCod,
      @required this.qtdActivities,
      @required this.typeFirstActivity});

  DiaCronogramaBase.newInstance(int day, Cronograma cronograma) {
    this.qtdActivities = 0;
    this.priceDay = 0.0;
    this.day = day;
    this.scheduleCod = cronograma.scheduleCod;
    this._cronograma = cronograma;
  }

  DiaCronogramaBase.newInstanceFromApi(int day, int scheduleCod, int idApi) {
    this.qtdActivities = 0;
    this.priceDay = 0.0;
    this.day = day;
    this.scheduleCod = scheduleCod;
    this.idApi = idApi;
  }

  void setCronograma(Cronograma value) {
    _cronograma = value;
  }

  Cronograma get cronograma => _cronograma;

  @action
  setPrice(double price) {
    cronograma.changePrice(cronograma.priceFinal - this.priceDay + price);
    this.priceDay = price;
  }

  @action
  setQtdAtividades(int qtdActivities) {
    this.qtdActivities = qtdActivities;
  }

  @action
  setDay(int day) {
    this.day = day;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DiaCronogramaBase &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;

  DiaCronogramaBase.fromMap(Map<String, dynamic> maps)
      : id = maps['CO_SCHEDULE_DAY'],
        day = maps['NU_DAY'],
        scheduleCod = maps['CO_SCHEDULE_TRAVEL'],
        typeFirstActivity = maps['typeFirstActivity'],
        priceDay = maps['priceDay'] ?? 0.0,
        qtdActivities = maps['qtdActivities'] ?? 0;

  DiaCronogramaBase.fromMapApi(Map<String, dynamic> maps)
      : idApi = maps['id'],
        day = maps['day'],
        scheduleCod = maps['scheduleCod'],
        typeFirstActivity = maps['typeFirstActivity'],
        priceDay = maps['priceDay'] != null
            ? double.parse(maps['priceDay'].toString())
            : 0.0,
        qtdActivities = maps['qtdActivities'] ?? 0;

  Map<String, dynamic> toMap() {
    return {'NU_DAY': day, 'CO_SCHEDULE_TRAVEL': scheduleCod};
  }

  Map<String, dynamic> toMapApi() {
    return {
      'day': day,
      'id': idApi,
    };
  }
}
