import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/util/format_util.dart';

part 'avaliacao.g.dart';

class Avaliacao = BaseAvaliacao with _$Avaliacao;

abstract class BaseAvaliacao with Store {
  String description;
  double qtdStar;
  String data;
  String userUid;

  BaseAvaliacao({this.description, this.qtdStar});

  BaseAvaliacao.newInstance() {
    this.description = '';
    this.qtdStar = 3.0;
    this.data = formatDate(DateTime.now());
  }

  BaseAvaliacao.fromMapApi(Map<String, dynamic> maps)
      : description = maps['description'],
        userUid = maps['userUid'],
        qtdStar = double.parse(maps['qtdStar'].toString()),
        data = "${maps['date'][2]}/${maps['date'][1]}/${maps['date'][0]}";

  Map<String, dynamic> toMapApi() {
    return {
      'description': description,
      'qtdStar': qtdStar,
    };
  }
}
