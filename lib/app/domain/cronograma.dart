import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/session_user.dart';

part 'cronograma.g.dart';

class Cronograma = CronogramaBase with _$Cronograma;

abstract class CronogramaBase with Store {
  bool findFromAPI = false;
  int scheduleCod;
  int scheduleCodAPI;

  final String userName;
  final String userUID;
  final String userEmail;

  @observable
  int qtdDays;
  String cityAddress;
  String cityState;
  String placeID;

  @observable
  int numberLike = 0;

  @observable
  bool isPublish = false;

  @observable
  List imagesUrl;

  @observable
  bool removed = false;

  @observable
  double priceFinal = 0.0;

  CronogramaBase(
      {this.scheduleCod,
      @required this.qtdDays,
      this.imagesUrl = const [
        'https://media.istockphoto.com/vectors/city-silhouette-land-scape-horizontal-city-landscape-downtown-skyline-vector-id959234938?k=6&m=959234938&s=170667a&w=0&h=yjJNnGWHPO23Jxsv2vMsYxbhi3oEEjEwlnM3dUEwBBU='
      ],
      @required this.cityAddress,
      @required this.userEmail,
      @required this.placeID,
      @required this.cityState,
      @required this.userName,
      @required this.userUID});

  get firstImage => imagesUrl != null ? imagesUrl.first : '';

  @action
  void changePublish(bool isPublish) {
    this.isPublish = isPublish;
  }

  @action
  void changePrice(double priceFinal) {
    this.priceFinal = priceFinal;
  }

  @action
  void updateNumberStar(bool positiveVote) {
    if (positiveVote) {
      this.numberLike++;
    } else {
      this.numberLike--;
    }
  }

  @action
  void changeQtdDays(int qtdDays) {
    this.qtdDays = qtdDays;
  }

  @action
  void subtractQtdDays() {
    this.qtdDays -= 1;
  }

  @action
  void changeImagesUrl(List imagesUrl) {
    this.imagesUrl = imagesUrl;
  }

  @action
  void setRemoved() {
    this.removed = true;
  }

  String getCityFormattedName() {
    if (cityState == null) {
      return cityAddress;
    }
    return '$cityAddress - $cityState';
  }

  CronogramaBase.fromMap(Map<String, dynamic> maps)
      : scheduleCod = maps['CO_SCHEDULE_TRAVEL'],
        scheduleCodAPI = maps['CO_SCHEDULE_TRAVEL_API'],
        userName = maps['DS_USER'],
        userUID = maps['CO_USER'].toString(),
        userEmail = maps['DS_USER_EMAIL'],
        qtdDays = maps['NU_DAYS'],
        cityAddress = maps['DS_PLACE'],
        placeID = maps['CO_PLACE_ID'],
        cityState = maps['DS_PLACE_STATE'],
        numberLike = maps['NU_LIKE'],
        isPublish = maps['ST_PUBLIC_ACCESS'] == 'true',
        imagesUrl = maps['imagesUrl'] != null
            ? maps['imagesUrl'].split(',')
            : [
                'https://media.istockphoto.com/vectors/city-silhouette-land-scape-horizontal-city-landscape-downtown-skyline-vector-id959234938?k=6&m=959234938&s=170667a&w=0&h=yjJNnGWHPO23Jxsv2vMsYxbhi3oEEjEwlnM3dUEwBBU='
              ],
        priceFinal = maps['priceFinal'] != null
            ? double.parse(maps['priceFinal'].toString())
            : 0.0;

  CronogramaBase.fromMapApi(Map<String, dynamic> maps)
      : scheduleCodAPI = maps['scheduleCod'],
        userName = maps['userName'],
        userUID = maps['userUID'],
        userEmail = maps['userEmail'],
        qtdDays = maps['qtdDays'],
        placeID = maps['placeID'],
        cityAddress = maps['cityAddress'],
        numberLike = maps['numberLike'],
        isPublish = maps['publish'] == true,
        imagesUrl = (maps['imagesUrl'] as List).length > 0
            ? maps['imagesUrl']
            : [
                'https://media.istockphoto.com/vectors/city-silhouette-land-scape-horizontal-city-landscape-downtown-skyline-vector-id959234938?k=6&m=959234938&s=170667a&w=0&h=yjJNnGWHPO23Jxsv2vMsYxbhi3oEEjEwlnM3dUEwBBU='
              ],
        priceFinal = double.parse(maps['priceFinal'].toString()),
        findFromAPI = true;

  Map<String, dynamic> toMap() {
    return {
      'CO_PLACE_ID': placeID,
      'DS_PLACE_STATE': cityState,
      'DS_USER_EMAIL': userEmail,
      'DS_USER': userName,
      'CO_USER': userUID,
      'DS_PLACE': cityAddress,
      'NU_LIKE': numberLike,
      'ST_PUBLIC_ACCESS': isPublish.toString(),
      'NU_DAYS': qtdDays,
      'CO_SCHEDULE_TRAVEL_API': scheduleCodAPI,
      'CO_SCHEDULE_TRAVEL': scheduleCod
    };
  }

  Map<String, dynamic> toMapApi() {
    return {
      'placeID': placeID,
      'userEmail': userEmail,
      'userName': userName,
      'userUID': userUID,
    };
  }

  isOwner(SessionUser user) {
    if (user != null && userUID == user.uid) {
      return true;
    }
    return false;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CronogramaBase &&
          runtimeType == other.runtimeType &&
          scheduleCod == other.scheduleCod;

  @override
  int get hashCode => scheduleCod.hashCode;
}
