// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'atividade.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Atividade on BaseAtividade, Store {
  final _$nameOfPlaceAtom = Atom(name: 'BaseAtividade.nameOfPlace');

  @override
  String get nameOfPlace {
    _$nameOfPlaceAtom.reportRead();
    return super.nameOfPlace;
  }

  @override
  set nameOfPlace(String value) {
    _$nameOfPlaceAtom.reportWrite(value, super.nameOfPlace, () {
      super.nameOfPlace = value;
    });
  }

  final _$BaseAtividadeActionController =
      ActionController(name: 'BaseAtividade');

  @override
  dynamic setNameOfPlace(String nameOfPlace) {
    final _$actionInfo = _$BaseAtividadeActionController.startAction(
        name: 'BaseAtividade.setNameOfPlace');
    try {
      return super.setNameOfPlace(nameOfPlace);
    } finally {
      _$BaseAtividadeActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
nameOfPlace: ${nameOfPlace}
    ''';
  }
}
