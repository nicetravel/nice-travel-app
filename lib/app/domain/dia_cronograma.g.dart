// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dia_cronograma.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$DiaCronograma on DiaCronogramaBase, Store {
  final _$dayAtom = Atom(name: 'DiaCronogramaBase.day');

  @override
  int get day {
    _$dayAtom.reportRead();
    return super.day;
  }

  @override
  set day(int value) {
    _$dayAtom.reportWrite(value, super.day, () {
      super.day = value;
    });
  }

  final _$priceDayAtom = Atom(name: 'DiaCronogramaBase.priceDay');

  @override
  double get priceDay {
    _$priceDayAtom.reportRead();
    return super.priceDay;
  }

  @override
  set priceDay(double value) {
    _$priceDayAtom.reportWrite(value, super.priceDay, () {
      super.priceDay = value;
    });
  }

  final _$qtdActivitiesAtom = Atom(name: 'DiaCronogramaBase.qtdActivities');

  @override
  int get qtdActivities {
    _$qtdActivitiesAtom.reportRead();
    return super.qtdActivities;
  }

  @override
  set qtdActivities(int value) {
    _$qtdActivitiesAtom.reportWrite(value, super.qtdActivities, () {
      super.qtdActivities = value;
    });
  }

  final _$typeFirstActivityAtom =
      Atom(name: 'DiaCronogramaBase.typeFirstActivity');

  @override
  String get typeFirstActivity {
    _$typeFirstActivityAtom.reportRead();
    return super.typeFirstActivity;
  }

  @override
  set typeFirstActivity(String value) {
    _$typeFirstActivityAtom.reportWrite(value, super.typeFirstActivity, () {
      super.typeFirstActivity = value;
    });
  }

  final _$DiaCronogramaBaseActionController =
      ActionController(name: 'DiaCronogramaBase');

  @override
  void setTypeFirstActivity(String typeFirstActivity) {
    final _$actionInfo = _$DiaCronogramaBaseActionController.startAction(
        name: 'DiaCronogramaBase.setTypeFirstActivity');
    try {
      return super.setTypeFirstActivity(typeFirstActivity);
    } finally {
      _$DiaCronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setPrice(double price) {
    final _$actionInfo = _$DiaCronogramaBaseActionController.startAction(
        name: 'DiaCronogramaBase.setPrice');
    try {
      return super.setPrice(price);
    } finally {
      _$DiaCronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setQtdAtividades(int qtdActivities) {
    final _$actionInfo = _$DiaCronogramaBaseActionController.startAction(
        name: 'DiaCronogramaBase.setQtdAtividades');
    try {
      return super.setQtdAtividades(qtdActivities);
    } finally {
      _$DiaCronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setDay(int day) {
    final _$actionInfo = _$DiaCronogramaBaseActionController.startAction(
        name: 'DiaCronogramaBase.setDay');
    try {
      return super.setDay(day);
    } finally {
      _$DiaCronogramaBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
day: ${day},
priceDay: ${priceDay},
qtdActivities: ${qtdActivities},
typeFirstActivity: ${typeFirstActivity}
    ''';
  }
}
