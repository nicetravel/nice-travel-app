
import 'package:flutter/foundation.dart';

class SessionUser {
  final String displayName;
  final String email;
  final String uid;
  String tokenMessage;

  SessionUser({
    @required this.displayName,
    @required this.email,
    @required this.uid,
    @required this.tokenMessage});


  Map<String, dynamic> toMap() {
    return {
      'displayName': displayName,
      'email': email,
      'uid': uid,
      'tokenMessage': tokenMessage,
    };
  }

  SessionUser.fromMap(Map<String, dynamic> maps)
      : displayName = maps['displayName'],
        email = maps['email'],
        uid = maps['uid'],
        tokenMessage = maps['tokenMessage'];

}
