import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';

ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
    buildShowSnackBarToLogin(BuildContext context, {ScaffoldState scaffold}) {
  if (scaffold == null) {
    scaffold = Scaffold.of(context);
  }
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  return  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: const Text('É necessário realizar login'),
      action: SnackBarAction(
          label: 'Login', onPressed: () => navigateToSignIn(context)),
    ),
  );
}

void navigateToSignIn(BuildContext context) {
  Navigator.of(context).pushNamed(AppModule.LOGIN_PATH);
}

ScaffoldFeatureController<SnackBar, SnackBarClosedReason>
    buildShowSnackBarToDuplicateSchedule(BuildContext context, SessionUser user,
        Cronograma cronograma, Function duplicate) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  return  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content:
          const Text('Para alterar é necessário adicionar esse cronograma.'),
      action: SnackBarAction(
          key: Key('adicionar_snackbar'),
          label: 'Adicionar',
          onPressed: () => Function.apply(duplicate, [])),
    ),
  );
}

void showToastMessage(String message, BuildContext context) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(content: Text(message)),
  );
}
