import 'package:intl/intl.dart';

final formatter = NumberFormat.currency(locale: "pt_BR", symbol: "");
final dateFormat = DateFormat("dd/MM/yyyy");
final timeFormat = DateFormat("HH:mm");

String formatMoney(valorAPagar) =>
    valorAPagar == null ? null : formatter.format(valorAPagar);

String formatDate(DateTime date) {
  return dateFormat.format(date);
}

String formatHoraToString(DateTime date) {
  if (date == null) {
    return "";
  }
  return timeFormat.format(date);
}

String formatHoraBetweenToString(DateTime dateStart, DateTime dateEnd) {
  String formattedDate = formatHoraToString(dateStart);
  if (dateEnd != null) {
    formattedDate += " - " + formatHoraToString(dateEnd);
  }
  return formattedDate;
}

DateTime formatStringToHora(String date) {
  if (date.isEmpty) {
    return null;
  }
  return timeFormat.parse(date);
}
