import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:nice_travel/app/modules/dia_cronograma/page/dia_cronograma_controller.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/cloud_repository_interface.dart';
import 'package:nice_travel/app/modules/home/page/home_controller.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';
import 'package:nice_travel/app/util/show_circular_progress.dart';
import 'package:nice_travel/app/util/show_toast.dart';

validateLoginAction(
    {@required BuildContext context,
    @required SessionUser user,
    ICloudCronogramaRepository cronogramaRepository,
    @required Cronograma cronograma,
    @required Function successAction}) {
  if (user == null) {
    buildShowSnackBarToLogin(context);
  } else {
    if (cronograma != null &&
        !cronograma.isOwner(user) &&
        cronogramaRepository != null) {
      buildShowSnackBarToDuplicateSchedule(context, user, cronograma,
          () => _duplicate(context, cronograma, cronogramaRepository));
    } else {
      Function.apply(successAction, []);
    }
  }
}

void _duplicate(BuildContext context, Cronograma cronograma,
    ICloudCronogramaRepository cronogramaRepository) {
  showCircularProgress(context);
  cronogramaRepository.duplicate(cronograma).then((value) async {
    Modular.dispose<HomeController>();
    Modular.dispose<DiaCronogramaController>();
    await Navigator.of(context).pushNamedAndRemoveUntil(
        AppModule.DIA_CRONOGRAMA_PATH, (route) => route.isFirst,
        arguments: value);
  }).catchError((exception, stacktrace) {
    Navigator.of(context).pop();
    getErrorDialogWhenDuplicate(context, exception, stacktrace);
  });
}
