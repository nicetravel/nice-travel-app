import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:nice_travel/app/config/error_logger.dart';
import 'package:nice_travel/app/modules/termosuso/widget/termos_uso_text.dart';

void removeDialog(BuildContext context, String title, Function onDelete) {
  showDialog(
      context: context,
      builder: (_) => AssetGiffyDialog(
            image: Image.asset("assets/images/noo.gif"),
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'Essa ação não poderá ser revertida.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            entryAnimation: EntryAnimation.BOTTOM_LEFT,
            onOkButtonPressed: () => Function.apply(onDelete, []),
          ));
}

void publishCronogramaDialog(
    BuildContext context, String title, Function onPublish) {
  showDialog(
      context: context,
      builder: (_) => AssetGiffyDialog(
            image: Image.asset("assets/images/yes.gif"),
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'Não será possível possível despublicar.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            entryAnimation: EntryAnimation.BOTTOM_LEFT,
            onOkButtonPressed: () => Function.apply(onPublish, []),
          ));
}

void rePublishCronogramaDialog(
    BuildContext context, String title, Function onPublish) {
  showDialog(
      context: context,
      builder: (_) => AssetGiffyDialog(
            image: Image.asset("assets/images/confirm.gif"),
            title: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
            ),
            description: Text(
              'O cronograma será atualizado.',
              textAlign: TextAlign.center,
              style: TextStyle(),
            ),
            entryAnimation: EntryAnimation.BOTTOM_LEFT,
            onOkButtonPressed: () => Function.apply(onPublish, []),
          ));
}

AwesomeDialog getErrorDialogWhenDuplicate(context, error, stackTrace) {
  ErrorLogger().logInternal(error, stackTrace);
  return getErrorDialog(
      dismissOnTouchOutside: true,
      context: context,
      title: "Ocorreu um erro!",
      message: "Erro ao obter o cronograma, tente novamente mais tarde.")
    ..show();
}

AwesomeDialog getErrorDialog(
    {@required BuildContext context,
    String title = 'Erro no Sistema',
    bool dismissOnTouchOutside = true,
    @required String message,
    Function action}) {
  return AwesomeDialog(
    dismissOnTouchOutside: dismissOnTouchOutside,
    headerAnimationLoop: false,
    context: context,
    dialogType: DialogType.ERROR,
    animType: AnimType.SCALE,
    title: title,
    desc: message,
    btnOkColor: Colors.redAccent,
    btnOkOnPress: () => action != null ? Function.apply(action, []) : null,
    onDissmissCallback: () =>
        action != null ? Function.apply(action, []) : null,
  );
}

AwesomeDialog getTermosUsoDialog(
    {@required BuildContext context, Function action}) {
  return AwesomeDialog(
    dismissOnTouchOutside: false,
    headerAnimationLoop: false,
    context: context,
    dialogType: DialogType.WARNING,
    animType: AnimType.SCALE,
    body: Column(
      children: [
        Center(
          child: Text(
            "Termos de uso",
            style: TextStyle(fontSize: 26),
          ),
        ),
        TermosUsoText(),
      ],
    ),
    btnOkText: "Concordo",
    btnOkColor: Colors.blue,
    btnOkOnPress: () => action != null ? Function.apply(action, []) : null,
    onDissmissCallback: () =>
        action != null ? Function.apply(action, []) : null,
  );
}

AwesomeDialog getSuccessDialog(
    {@required BuildContext context,
    @required String title,
    bool dismissOnTouchOutside = true,
    @required String message,
    Function action}) {
  return AwesomeDialog(
    dismissOnTouchOutside: dismissOnTouchOutside,
    headerAnimationLoop: false,
    context: context,
    dialogType: DialogType.SUCCES,
    animType: AnimType.SCALE,
    title: title,
    desc: message,
    btnOkColor: Colors.blue,
    btnOkOnPress: () => action != null ? Function.apply(action, []) : null,
    onDissmissCallback: () =>
        action != null ? Function.apply(action, []) : null,
  );
}
