import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showCircularProgress(BuildContext context) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Center(
          child: CircularProgressIndicator(),
        );
      });
}
