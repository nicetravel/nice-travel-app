import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:mobx/mobx.dart';
import 'package:nice_travel/app/domain/session_user.dart';

import 'interfaces/security_service_interface.dart';

part 'security_service.g.dart';

@Injectable()
class SecurityService = _SecurityServiceBase with _$SecurityService;

abstract class _SecurityServiceBase with Store implements ISecurityService {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final storage = new FlutterSecureStorage();

  @observable
  SessionUser sessionUser;

  @action
  _setSessionUser(SessionUser sessionUser) {
    this.sessionUser = sessionUser;
  }

  @override
  void dispose() {
    //dispose will be called automatically
  }

  @override
  SessionUser getSessionUser() {
    return sessionUser;
  }

  @override
  bool isLoggedIn() {
    return getSessionUser() != null;
  }

  @override
  Future<SessionUser> signIn(User user) async {
    var tokenMessage = await _firebaseMessaging.getToken();
    var sessionUser = SessionUser(
        displayName: user.displayName,
        email: user.email,
        uid: user.uid,
        tokenMessage: tokenMessage);
    _setSessionUser(sessionUser);
    storage.write(key: "SESSION_USER", value: jsonEncode(sessionUser.toMap()));
    return Future.value(sessionUser);
  }

  Future<bool> signInSilently() async {
    if (!isLoggedIn()) {
      var connectivityResult = await (Connectivity().checkConnectivity());

      var userJson = await storage.read(key: "SESSION_USER");
      if (userJson != null) {
        var localStorageUser = jsonDecode(userJson);
        if (localStorageUser != null) {
          _setSessionUser(SessionUser.fromMap(localStorageUser));
          if (connectivityResult == ConnectivityResult.none) {
            return Future.value(true);
          }
        }
      }

      await GoogleSignIn().signInSilently().then((userAuthenticated) async {
        if (userAuthenticated != null) {
          final GoogleSignInAuthentication googleAuth =
              await userAuthenticated.authentication;
          UserCredential _firebaseUser = await FirebaseAuth.instance
              .signInWithCredential(GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken,
            idToken: googleAuth.idToken,
          ));
          signIn(_firebaseUser.user);
          return Future.value(true);
        }
      });
    } else {
      return Future.value(true);
    }
    return Future.value(false);
  }

  @override
  Future<bool> signOut() async {
    await FirebaseAuth.instance.signOut();
    await GoogleSignIn().signOut();
    sessionUser = null;
    storage.delete(key: "SESSION_USER");
    return Future.value(true);
  }
}
