import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_widget.dart';
import 'package:nice_travel/app/config/local_database.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_local_repository.dart';
import 'package:nice_travel/app/interfaces/repository/meu_cronograma_repository_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/dia_cronograma_module.dart';
import 'package:nice_travel/app/modules/home/home_module.dart';
import 'package:nice_travel/app/modules/meu_cronograma/meu_cronograma_module.dart';
import 'package:nice_travel/app/modules/termosuso/termos_uso_module.dart';
import 'package:nice_travel/app/shared/city_autocomplete/google_places_repository.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';

import 'interfaces/repository/search_cronograma_api_repository.dart';
import 'interfaces/security_service_interface.dart';
import 'modules/login/login_module.dart';
import 'modules/navigation/navigation_module.dart';
import 'security_service.dart';

class AppModule extends MainModule {
  static const MEUS_CRONOGRAMAS_PATH = '/meus-cronogramas';
  static const VIAGEM_PATH = '/';
  static const DIA_CRONOGRAMA_PATH = '/dia-cronograma';
  static const LOGIN_PATH = '/login';
  static const TERMOS_USO = '/termos-uso';
  static const NAVEGACAO_PATH = '/navegacao';

  @override
  List<Bind> get binds => [
        Bind<Dio>((i) => DioForNative()),
        Bind<ICityAutocomplete>((i) => GooglePlacesAPI(i<Dio>())),
        Bind<ISecurityService>((i) => SecurityService()),
        Bind<IMeuCronogramaRepository>((b) => MeuCronogramaLocalRepository(
            b<ISecurityService>(), b<IDatabase>())),
        Bind<ICloudSearchCronogramaRepository>(
            (b) => SearchCronogramaRepository(b<Dio>())),
        Bind<IDatabase>(
            (b) => LocalDatabase(b<ICloudSearchCronogramaRepository>())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: HomeModule()),
        ModularRouter(MEUS_CRONOGRAMAS_PATH, module: MeuCronogramaModule()),
        ModularRouter(DIA_CRONOGRAMA_PATH, module: DiaCronogramaModule()),
        ModularRouter(LOGIN_PATH, module: LoginModule()),
        ModularRouter(TERMOS_USO, module: TermosUsoModule()),
        ModularRouter(VIAGEM_PATH, module: NavigationModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
