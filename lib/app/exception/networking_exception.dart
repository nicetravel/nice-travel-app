abstract class InternalError implements Exception {
  String get message;
  String get exceptionMessage;
}

abstract class ExternalError implements Exception {
  String get message;
}

class NetworkingException extends InternalError {
  final String message;
  final String exceptionMessage;
  NetworkingException({this.message, this.exceptionMessage});
}

class GoogleAPIException extends InternalError {
  final String message;
  final String exceptionMessage;
  GoogleAPIException({this.message, this.exceptionMessage});
}