
import 'package:dio/dio.dart';
import 'package:nice_travel/app/config/error_logger.dart';

class ApiException implements Exception {
  final _message;
  final _prefix;

  ApiException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends ApiException {
  FetchDataException([String message])
      : super(message, "Error During Communication: ");
}

class BadRequestException extends ApiException {
  BadRequestException([message]) : super(message, "Invalid Request: ");
}

class UnauthorisedException extends ApiException {
  UnauthorisedException([message]) : super(message, "Unauthorised: ");
}

class InvalidInputException extends ApiException {
  InvalidInputException([String message]) : super(message, "Invalid Input: ");
}

getErrorMessage(Exception e) {
  if (e is DioError) {
    ErrorLogger().log(e.type.toString(), e.message);
    if (e.type == DioErrorType.connectTimeout ||
        e.type == DioErrorType.other) {
      return "Erro de conexão."
          "\nTente novamente mais tarde.";
    }
  }
  return e.toString();
}
