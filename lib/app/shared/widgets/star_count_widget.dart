import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class StarCountWidget extends StatelessWidget {
  final int numberLike;

  const StarCountWidget({Key key, @required this.numberLike}) : super(key: key);

  // BorderedText(
  // strokeWidth: 2.0,
  // child: Text(
  // '${cronograma.numberLike} 👍',
  // style: TextStyle(
  // color: Colors.white,
  // fontFamily: "OpenSans",
  // fontSize: 18,
  // fontWeight: FontWeight.bold),
  // ))

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        BorderedText(
          strokeWidth: 2.0,
          child: Text(
            '$numberLike',
            style: TextStyle(
                color: Colors.white,
                fontFamily: "OpenSans",
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
        Icon(
          MdiIcons.heart,
          size: 25,
          color: Colors.yellow[700],
        ),
      ],
    );
  }
}
