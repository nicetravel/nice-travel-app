import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/shared/widgets/navigation/tab_model.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  final int selectedIndex;

  const CustomBottomNavigationBar({Key key, @required this.selectedIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Colors.grey.withOpacity(0.5), width: 0.5),
        ),
      ),
      child: FFNavigationBar(
        selectedIndex: selectedIndex,
        onSelectTab: (index) => onItemSelected(index, context),
        items: _buildItems(),
        theme: FFNavigationBarTheme(
          selectedItemBorderColor: Colors.transparent,
          barBackgroundColor: Theme.of(context).scaffoldBackgroundColor,
          selectedItemBackgroundColor: Theme.of(context).primaryColor,
          selectedItemIconColor: Colors.white,
          showSelectedItemShadow: true,
          selectedItemLabelColor:
              Theme.of(context).brightness == Brightness.dark
                  ? Colors.white
                  : Colors.black87,
        ),
      ),
    );
  }

  List<FFNavigationBarItem> _buildItems() {
    return TabModel.getTabModels()
        .map<FFNavigationBarItem>((e) => navigationBarItem(e))
        .toList();
  }

  FFNavigationBarItem navigationBarItem(TabModel e) {
    return FFNavigationBarItem(
      iconData: e.icon,
      label: e.name,
    );
  }

  void onItemSelected(selectedIndex, BuildContext context) {
    TabModel tabModelSelected = TabModel.getTabModels()[selectedIndex];
    if (tabModelSelected.navigationPath == "/") {
      Modular.navigator.pop();
    } else {
      Modular.navigator.pushNamedAndRemoveUntil(
          tabModelSelected.navigationPath, ModalRoute.withName('/'));
    }
  }
}
