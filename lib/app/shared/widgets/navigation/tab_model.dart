import 'package:flutter/material.dart';
import 'package:nice_travel/app/app_module.dart';

class TabModel {
  final IconData icon;
  final String name;
  final String navigationPath;

  TabModel(this.icon, this.name, this.navigationPath);

  static List<TabModel> getTabModels() {
    List<TabModel> tabs = [];
    tabs.add(TabModel(Icons.date_range, "Viagem", AppModule.VIAGEM_PATH));
    tabs.add(TabModel(
        Icons.favorite, "Meus Cronogramas", AppModule.MEUS_CRONOGRAMAS_PATH));
    tabs.add(TabModel(Icons.person, "Perfil", AppModule.NAVEGACAO_PATH));
    return tabs;
  }
}
