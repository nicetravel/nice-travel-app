import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool automaticallyImplyLeading;
  final Widget titleWidget;
  final List<Widget> actions;

  const CustomAppBar(
      {Key key,
      this.title,
      this.titleWidget,
      this.actions,
      this.automaticallyImplyLeading})
      : super(key: key);

  @override
  Size get preferredSize => title != null || titleWidget != null
      ? Size.fromHeight(50.0)
      : Size.fromHeight(0.0);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: automaticallyImplyLeading ?? true,
      actions: actions,
      iconTheme:
          IconThemeData(color: Theme.of(context).textTheme.bodyText1.color),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      title: titleWidget != null
          ? titleWidget
          : title != null
              ? Text(
                  title,
                  style: TextStyle(
                      color: Theme.of(context).textTheme.bodyText1.color),
                )
              : null,
      centerTitle: true,
      elevation: 0.0,
      backwardsCompatibility: false,
    );
  }
}
