import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:dio/native_imp.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:nice_travel/app/config/error_logger.dart';
import 'package:nice_travel/app/exception/networking_exception.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';

import 'model/places_model.dart';

class GooglePlacesAPI implements ICityAutocomplete {
  final DioForNative client;

  GooglePlacesAPI(this.client);

  GooglePlacesModel googlePlacesModel = new GooglePlacesModel();

  Future<Either<InternalError, List<GooglePlacesModel>>> getCities(
      String query) async {
    log("Calling Google Place API with Query: " + query);
    var response;
    try {
      response = await client.get(
          "https://maps.googleapis.com/maps/api/place/autocomplete/json",
          queryParameters: {
            "input": query,
            "language": "pt-BR",
            "types": "(cities)",
            //TODO include sessiontoken=1234567890
            "key": FlavorConfig.instance.variables["google_place_api"]
          });
    } catch (ex) {
      log(ex.error);
      return Left(NetworkingException(
          message: "Erro de conexão com a internet.",
          exceptionMessage: ex.toString()));
    }

    List<GooglePlacesModel> lista =
        googlePlacesModel.fromJsonList(response.data['predictions']);
    if (lista.isEmpty) {
      var errorMessage = response.data['error_message'];
      if (errorMessage != null) {
        ErrorLogger().log("Erro com Google Places", errorMessage);
        return Left(GoogleAPIException(
            message: "Ocorreu um erro interno, tente novamente mais tarde.",
            exceptionMessage: errorMessage));
      }
    }
    return Right(lista);
  }

  @override
  void dispose() {
    //dispose will be called automatically
  }
}
