import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/shared/city_autocomplete/city_autocomplete_controller.dart';
import 'package:nice_travel/app/shared/city_autocomplete/model/places_model.dart';
import 'package:nice_travel/app/shared/interfaces/city_autocomplete_interface.dart';
import 'package:nice_travel/app/util/modal_dialog.dart';

class CityAutocompleteWidget extends StatefulWidget {
  final InputDecoration inputDecoration;
  final CityAutocompleteController controller;
  final Function onSelectCity;
  final Function clearFunction;

  const CityAutocompleteWidget({
    Key key,
    @required this.onSelectCity,
    @required this.controller,
    this.inputDecoration,
    this.clearFunction,
  }) : super(key: key);

  @override
  _CityAutocompleteWidgetState createState() => _CityAutocompleteWidgetState(
      inputDecoration, controller, onSelectCity, clearFunction);
}

class _CityAutocompleteWidgetState extends State<CityAutocompleteWidget> {
  final ICityAutocomplete _cityRepository =
      AppModule.to.get<ICityAutocomplete>();
  final TextEditingController _typeAheadController = TextEditingController();

  final InputDecoration inputDecoration;
  final CityAutocompleteController controller;
  final Function onSelectCity;
  final Function clearFunction;

  bool _hasError = false;

  _CityAutocompleteWidgetState(this.inputDecoration, this.controller,
      this.onSelectCity, this.clearFunction);

  Future<List<GooglePlacesModel>> _onTextChanged(String text) {
    controller.cityInputsNull = true;
    return _searchCitySuggestions(text);
  }

  void _onSubmitted(GooglePlacesModel data) {
    controller.changePlaceId(data.placeId);
    controller.changePlaceState(data.terms[data.terms.length - 1].value);
    controller.changeNameCity(data.terms[0].value);
    Function.apply(widget.onSelectCity, []);
    this._typeAheadController.text = data.terms[0].value;
  }

  changeErrorState(bool error) {
    setState(() {
      _hasError = error;
    });
  }

  _showErrorDialog(String message) {
    if (!_hasError && context != null) {
      changeErrorState(true);
      getErrorDialog(
          dismissOnTouchOutside: true,
          context: context,
          title: "Ocorreu um erro!",
          message: message,
          action: () => changeErrorState(false))
        ..show();
    }
  }

  Future<List<GooglePlacesModel>> _searchCitySuggestions(String text) async {
    List<GooglePlacesModel> suggestions = [];

    await _cityRepository.getCities(text).then((onValue) {
      onValue.fold((l) => _showErrorDialog(l.message),
          (r) => suggestions = _updateSuggestions(r));
    }, onError: (error, stackTrace) => _showErrorDialog(error.toString()));
    return suggestions;
  }

  List<GooglePlacesModel> _updateSuggestions(
      List<GooglePlacesModel> suggestions) {
    if (_typeAheadController.value != null && suggestions != null) {
      return suggestions;
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    return TypeAheadFormField<GooglePlacesModel>(
      hideOnEmpty: true,
      hideOnLoading: true,
      suggestionsBoxVerticalOffset: 1.0,
      debounceDuration: const Duration(milliseconds: 500),
      textFieldConfiguration: TextFieldConfiguration(
          controller: this._typeAheadController,
          decoration: InputDecoration(
              prefixIcon:
                  inputDecoration != null ? inputDecoration.prefixIcon : null,
              border: inputDecoration != null
                  ? inputDecoration.border
                  : OutlineInputBorder(),
              labelText: inputDecoration != null
                  ? inputDecoration.labelText
                  : "Vai para onde?",
              hintText: inputDecoration?.hintText,
              suffixIcon: IconButton(
                icon: Icon(Icons.clear),
                onPressed: () => clearFields(),
              ))),
      suggestionsCallback: (pattern) async {
        return _onTextChanged(pattern);
      },
      itemBuilder: itemSuggestion,
      transitionBuilder: (context, suggestionsBox, controller) {
        return suggestionsBox;
      },
      onSuggestionSelected: (suggestion) {
        _onSubmitted(suggestion);
      },
      validator: (value) {
        if(value.isEmpty){
          return 'Campo Obrigatório';
        }
        if (controller.cityInputsNull) {
          clearFields();
          return 'Por favor, selecione novamente a cidade.';
        }
        return null;
      },
    );
  }

  void clearFields() {
    _typeAheadController.clearComposing();
    _typeAheadController.clear();
    controller.clearPlaceID();
    Function.apply(clearFunction, []);
  }

  Widget itemSuggestion(BuildContext context, GooglePlacesModel suggestion) {
    return ListTile(
      key: Key('autocomplete_field_${suggestion.id}'),
      title: Text(suggestion.description),
      trailing: Text(suggestion.terms[1].value),
    );
  }
}
