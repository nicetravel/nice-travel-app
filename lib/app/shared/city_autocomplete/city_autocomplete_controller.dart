import 'package:mobx/mobx.dart';

part 'city_autocomplete_controller.g.dart';

class CityAutocompleteController = _CityAutocompleteControllerBase
    with _$CityAutocompleteController;

abstract class _CityAutocompleteControllerBase with Store {
  @observable
  String _placeId;

  @observable
  String _nameCity;

  @observable
  String _placeState;

  bool cityInputsNull = true;

  @action
  void changePlaceId(String placeId) {
    _placeId = placeId;
  }

  @action
  void changePlaceState(String placeState) {
    _placeState = placeState;
  }

  String getPlaceId() => _placeId;

  @action
  void changeNameCity(String nameCity) {
    _nameCity = nameCity;
    cityInputsNull = _nameCity.isEmpty;
  }

  String getNameCity() => _nameCity;

  String getPlaceState() => _placeState;

  @action
  clearPlaceID() {
    _placeId = null;
    _nameCity = null;
  }
}
