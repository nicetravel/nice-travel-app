// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'security_service.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SecurityService on _SecurityServiceBase, Store {
  final _$sessionUserAtom = Atom(name: '_SecurityServiceBase.sessionUser');

  @override
  SessionUser get sessionUser {
    _$sessionUserAtom.reportRead();
    return super.sessionUser;
  }

  @override
  set sessionUser(SessionUser value) {
    _$sessionUserAtom.reportWrite(value, super.sessionUser, () {
      super.sessionUser = value;
    });
  }

  final _$_SecurityServiceBaseActionController =
      ActionController(name: '_SecurityServiceBase');

  @override
  dynamic _setSessionUser(SessionUser sessionUser) {
    final _$actionInfo = _$_SecurityServiceBaseActionController.startAction(
        name: '_SecurityServiceBase._setSessionUser');
    try {
      return super._setSessionUser(sessionUser);
    } finally {
      _$_SecurityServiceBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
sessionUser: ${sessionUser}
    ''';
  }
}
