import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/session_user.dart';

abstract class ISecurityService implements Disposable {

  SessionUser getSessionUser();
  bool isLoggedIn();
  Future<SessionUser> signIn(User user);
  Future<bool> signOut();
  Future<bool> signInSilently();
}
