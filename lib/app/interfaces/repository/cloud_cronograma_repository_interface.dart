
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';


abstract class ICloudSearchCronogramaRepository implements Disposable {

  Future<ApiResponse<List<Cronograma>>> findCronogramas(String _placeId, int qtdElements);

  Future<ApiResponse<List<Cronograma>>> findCronogramasByUser(String userId);

  Future<ApiResponse<List<DiaCronograma>>> findDiasCronogramas(int scheduleCod);

  Future<ApiResponse<List<Atividade>>> findAllAtividades(int scheduleDayId);

  Future<ApiResponse<List<Avaliacao>>> findAllAvaliacoes(int scheduleCod, int qtdElements);

}
