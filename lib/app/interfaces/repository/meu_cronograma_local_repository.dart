import 'dart:async';

import 'package:nice_travel/app/config/local_database.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/interfaces/database_interface.dart';
import 'package:nice_travel/app/interfaces/security_service_interface.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/cronograma_publish_response.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/duplicated_cronograma_api_resource.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';
import 'package:sqflite/sqflite.dart';

import 'meu_cronograma_repository_interface.dart';

class MeuCronogramaLocalRepository implements IMeuCronogramaRepository {
  final ISecurityService _securityService;
  final IDatabase _databaseConfig;

  MeuCronogramaLocalRepository(this._securityService, this._databaseConfig);

  List<Cronograma> _meusCronogramas = [];

  @override
  void dispose() {
    _meusCronogramas.clear();
  }

  @override
  Future<ApiResponse<List<Cronograma>>> findAll() async {
    if (_meusCronogramas.isEmpty && _securityService.getSessionUser() != null) {
      var cronogromas = await _lazyLoad();
      _meusCronogramas.addAll(cronogromas);
    }
    _meusCronogramas.sort((a, b) => a.scheduleCod.compareTo(b.scheduleCod));
    return ApiResponse.completed(List.unmodifiable(_meusCronogramas));
  }

  Future<List<Cronograma>> _lazyLoad() async {
    final Database db = await _databaseConfig.database;

    var elements = await db.rawQuery('SELECT travel.*, '
        ' ( SELECT group_concat(photo.DS_IMAGE_URL) as imagesUrl FROM ${LocalDatabase.TB_PLACE_PHOTO} photo where photo.CO_SCHEDULE_TRAVEL = travel.CO_SCHEDULE_TRAVEL ) as imagesUrl, '
        ' ( SELECT SUM(atv.VL_PRICE) as atv FROM ${LocalDatabase.TB_ACTIVITY} atv '
        '   INNER JOIN ${LocalDatabase.TB_SCHEDULE_DAY} day '
        '   ON  day.CO_SCHEDULE_DAY = atv.CO_SCHEDULE_DAY '
        '   WHERE day.CO_SCHEDULE_TRAVEL = travel.CO_SCHEDULE_TRAVEL ) as priceFinal'
        ' FROM ${LocalDatabase.TB_SCHEDULE_TRAVEL} travel '
        ' WHERE travel.CO_SCHEDULE_TRAVEL is not null '
        ' AND travel.CO_USER = "${_securityService.getSessionUser()?.uid}"');
    if (elements != null) {
      var cronogramas =
          elements.map<Cronograma>((e) => Cronograma.fromMap(e)).toList();
      return cronogramas;
    }
    return [];
  }

  @override
  Future<int> remove(int codCronograma) async {
    _deleteLocal(codCronograma);
    _meusCronogramas.remove(Cronograma(scheduleCod: codCronograma));
    return Future.value(codCronograma);
  }

  @override
  Future<bool> curtir(Cronograma cronograma) async {
    cronograma.updateNumberStar(true);
    return Future.value(true);
  }

  Future<int> _deleteLocal(int codCronograma) async {
    final Database db = await _databaseConfig.database;
    return db.delete(LocalDatabase.TB_SCHEDULE_TRAVEL,
        where: 'CO_SCHEDULE_TRAVEL = ?', whereArgs: [codCronograma]);
  }


  @override
  Future<Cronograma> saveOrUpdate(MeuCronograma meuCronograma) async {
    if (meuCronograma.getCod == null) {
      Cronograma cronograma = await _criarCronograma(meuCronograma);
      _meusCronogramas.add(cronograma);
      return Future.value(cronograma);
    } else {
      Cronograma cronograma = _atualizarCronograma(meuCronograma);
      _updateCronogramaLocal(cronograma);
      return Future.value(cronograma);
    }
  }

  @override
  Future<Cronograma> duplicateCronograma(
      DuplicatedCronogramaAPI duplicatedCronograma) async {
    if (_meusCronogramas.isEmpty) {
      var cronogromas = await _lazyLoad();
      _meusCronogramas.addAll(cronogromas);
    }

    var cronograma = duplicatedCronograma.cronograma;
    var formattedAddress = cronograma.cityAddress.split(",");
    if (formattedAddress.isNotEmpty) {
      cronograma.cityAddress = formattedAddress[0];
      cronograma.cityState = formattedAddress[formattedAddress.length - 1];
    }
    final Database db = await _databaseConfig.database;
    var id =
        await db.insert(LocalDatabase.TB_SCHEDULE_TRAVEL, cronograma.toMap());
    cronograma.scheduleCod = id;
    _savePlacePhotos(cronograma, db);
    _meusCronogramas.add(cronograma);

    await _saveDiasCronogramaFromApi(
        duplicatedCronograma, cronograma.scheduleCod);

    return cronograma;
  }

  Future _saveDiasCronogramaFromApi(
      DuplicatedCronogramaAPI duplicatedCronograma, int scheduleCod) async {
    for (int day = 0; day < duplicatedCronograma.scheduleDays.length; day++) {
      var diaCronogramaResponse = duplicatedCronograma.scheduleDays[day];
      var diaCronograma = DiaCronograma.newInstanceFromApi(
          diaCronogramaResponse.day,
          scheduleCod,
          diaCronogramaResponse.scheduleDayId);
      final Database db = await _databaseConfig.database;
      var id =
          await db.insert(LocalDatabase.TB_SCHEDULE_DAY, diaCronograma.toMap());

      diaCronogramaResponse.activities.map((atv) {
        atv.idScheduleDay = id;
        return atv;
      }).forEach((atv) => updateAtividade(atv));
    }
  }

  Future<int> _updateCronogramaLocal(Cronograma cronograma) {
    _meusCronogramas.remove(cronograma);
    _meusCronogramas.add(cronograma);
    return _atualizarLocal(cronograma);
  }

  Cronograma _atualizarCronograma(MeuCronograma meuCronograma) {
    var cronograma = _meusCronogramas
        .where((element) => element.scheduleCod == meuCronograma.getCod)
        .first;
    cronograma.cityAddress = meuCronograma.nomeLugar;
    cronograma.cityState = meuCronograma.placeState;
    cronograma.qtdDays = meuCronograma.qtdDias;
    return cronograma;
  }

  Future<Cronograma> _criarCronograma(MeuCronograma meuCronograma) {
    var sessionUser = _securityService.getSessionUser();

    var cronograma = Cronograma(
        cityAddress: meuCronograma.nomeLugar,
        placeID: meuCronograma.getPlaceId,
        cityState: meuCronograma.placeState,
        qtdDays: meuCronograma.qtdDias,
        userEmail: sessionUser.email,
        userName: sessionUser.displayName,
        userUID: sessionUser.uid);

    return _saveLocal(cronograma);
  }

  Future<Cronograma> _saveLocal(Cronograma cronograma) async {
    final Database db = await _databaseConfig.database;
    var id =
        await db.insert(LocalDatabase.TB_SCHEDULE_TRAVEL, cronograma.toMap());
    cronograma.scheduleCod = id;
    _savePlacePhotos(cronograma, db);
    await _saveDiasCronogramas(cronograma);
    return cronograma;
  }

  void _savePlacePhotos(Cronograma cronograma, Database db) {
    Map<String, dynamic> map = {
      'CO_SCHEDULE_TRAVEL': cronograma.scheduleCod,
      'DS_IMAGE_URL': cronograma.imagesUrl[0],
    };
    db.insert(LocalDatabase.TB_PLACE_PHOTO, map);
  }

  Future _saveDiasCronogramas(Cronograma cronograma) async {
    for (int day = 1; day <= cronograma.qtdDays; day++) {
      var diaCronograma =
          DiaCronograma.newInstance(day, cronograma);
      final Database db = await _databaseConfig.database;
      db.insert(LocalDatabase.TB_SCHEDULE_DAY, diaCronograma.toMap());
    }
  }

  Future<int> _atualizarLocal(Cronograma cronograma) async {
    final Database db = await _databaseConfig.database;
    return db.update(LocalDatabase.TB_SCHEDULE_TRAVEL, cronograma.toMap(),
        where: 'CO_SCHEDULE_TRAVEL = ?', whereArgs: [cronograma.scheduleCod]);
  }

  Future<int> updateAtividade(Atividade atividade) async {
    final Database db = await _databaseConfig.database;
    return db.insert(LocalDatabase.TB_ACTIVITY, atividade.toMap());
  }

  @override
  Future<Cronograma> publishCronograma(
      int scheduleCod, CronogramaPublishResponse cronogramaPublish) async {
    final Database db = await _databaseConfig.database;
    Cronograma copy = _getCronogramaCopy(scheduleCod);

    copy.scheduleCodAPI = cronogramaPublish.scheduleCod;
    copy.isPublish = true;
    copy.imagesUrl = cronogramaPublish.imagesUrl;
    await _updateCronogramaLocal(copy);

    cronogramaPublish.scheduleDays.forEach((diaCronograma) async {
      await db.rawUpdate(
          "UPDATE ${LocalDatabase.TB_SCHEDULE_DAY} SET CO_SCHEDULE_DAY_API = ${diaCronograma.scheduleDayId}"
          " WHERE NU_DAY = ${diaCronograma.day}"
          " AND CO_SCHEDULE_TRAVEL = $scheduleCod");
      diaCronograma.activities.forEach((atv) async {
        await db.rawUpdate(
            "UPDATE ${LocalDatabase.TB_ACTIVITY} SET CO_ACTIVITY_API = ${atv.id}"
            " WHERE DT_START = '${atv.getStartActivity()}'");
      });
    });

    await db.delete(LocalDatabase.TB_PLACE_PHOTO,
        where: 'CO_SCHEDULE_TRAVEL = ?', whereArgs: [copy.scheduleCod]);
    cronogramaPublish.imagesUrl.forEach((imageUrl) async {
      Map<String, dynamic> map = {
        'CO_SCHEDULE_TRAVEL': copy.scheduleCod,
        'DS_IMAGE_URL': imageUrl,
      };
      await db.insert(LocalDatabase.TB_PLACE_PHOTO, map);
    });
    return Future.value(copy);
  }

  Cronograma _getCronogramaCopy(int scheduleCod) {
    var cronograma = _meusCronogramas
        .where((element) => element.scheduleCod == scheduleCod)
        .first;

    Cronograma copy = Cronograma.fromMap(cronograma.toMap());
    copy.priceFinal = cronograma.priceFinal;
    return copy;
  }
}
