
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/cronograma_publish_response.dart';
import 'package:nice_travel/app/modules/dia_cronograma/repository/model/duplicated_cronograma_api_resource.dart';
import 'package:nice_travel/app/modules/meu_cronograma/model/meu_cronograma.dart';
import 'package:nice_travel/app/shared/api_response.dart';


abstract class IMeuCronogramaRepository implements Disposable {

  Future<ApiResponse<List<Cronograma>>> findAll();
  Future<Cronograma> saveOrUpdate(MeuCronograma meuCronograma);
  Future<int> remove(int codCronograma);

  Future<Cronograma> duplicateCronograma(DuplicatedCronogramaAPI cronograma);
  Future<bool> curtir(Cronograma cronograma);
  Future<Cronograma> publishCronograma(int scheduleCod, CronogramaPublishResponse cronogramaSaved);
}
