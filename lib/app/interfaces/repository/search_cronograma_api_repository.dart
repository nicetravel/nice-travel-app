import 'package:dio/dio.dart';
import 'package:nice_travel/app/config/api/atividade_api_connection.dart';
import 'package:nice_travel/app/config/api/cronograma_api_connection.dart';
import 'package:nice_travel/app/config/api/dia_cronograma_api_connection.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/avaliacao.dart';
import 'package:nice_travel/app/domain/cronograma.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/exception/api_exception.dart';
import 'package:nice_travel/app/interfaces/repository/cloud_cronograma_repository_interface.dart';
import 'package:nice_travel/app/shared/api_response.dart';

class SearchCronogramaRepository implements ICloudSearchCronogramaRepository {
  final Dio _dio;

  SearchCronogramaRepository(this._dio);

  @override
  Future<ApiResponse<List<Cronograma>>> findCronogramas(
      String _placeId, int _qtdElements) async {
    try {
      List<Cronograma> lista = await CronogramaAPIConnection.instance
          .getScheduleByCityPlaceID(_dio, _placeId, sizeElements: _qtdElements);
      return ApiResponse.completed(List.unmodifiable(lista));
    } catch (e) {
      return ApiResponse.error(getErrorMessage(e));
    }
  }

  @override
  Future<ApiResponse<List<Cronograma>>> findCronogramasByUser(
      String userId) async {
    try {
      List<Cronograma> lista = await CronogramaAPIConnection.instance
          .findCronogramasByUser(_dio, userId);
      return ApiResponse.completed(List.unmodifiable(lista));
    } catch (e) {
      return ApiResponse.error(getErrorMessage(e));
    }
  }

  @override
  Future<ApiResponse<List<DiaCronograma>>> findDiasCronogramas(
      int scheduleCod) async {
    try {
      List<DiaCronograma> lista = await DiaCronogramaApiConnection.instance
          .getDiaCronogramaByCodSchedule(_dio, scheduleCod);
      return ApiResponse.completed(List.unmodifiable(lista));
    } catch (e) {
      return ApiResponse.error(getErrorMessage(e));
    }
  }

  @override
  Future<ApiResponse<List<Atividade>>> findAllAtividades(
      int scheduleDayId) async {
    try {
      List<Atividade> lista = await AtividadeApiConnection.instance
          .getActiviesByScheduleDay(_dio, scheduleDayId);
      return ApiResponse.completed(List.unmodifiable(lista));
    } catch (e) {
      return ApiResponse.error(getErrorMessage(e));
    }
  }


  @override
  Future<ApiResponse<List<Avaliacao>>> findAllAvaliacoes(
      int scheduleDayId, int _qtdElements) async {
    try {
      List<Avaliacao> lista = await CronogramaAPIConnection.instance
          .findAllAvaliacoes(_dio, scheduleDayId, sizeElements: _qtdElements);
      return ApiResponse.completed(List.unmodifiable(lista));
    } catch (e) {
      return ApiResponse.error(getErrorMessage(e));
    }
  }

  @override
  void dispose() {
    //dispose will be called automatically
  }
}
