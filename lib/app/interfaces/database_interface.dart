
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/domain/atividade.dart';
import 'package:nice_travel/app/domain/dia_cronograma.dart';
import 'package:nice_travel/app/domain/session_user.dart';
import 'package:sqflite/sqflite.dart';

abstract class IDatabase implements Disposable {

  Future<Database> get database;

  Future<List<DiaCronograma>> fetchDiasCronograma(int scheduleCod, { bool codFromApi = false});

  Future<List<Atividade>> fetchAtividades(int scheduleDayId);

  Future<bool> synchronizedFromApi(SessionUser sessionUser);

}
