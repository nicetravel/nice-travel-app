import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flavor/flutter_flavor.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:nice_travel/app/app_module.dart';
import 'package:nice_travel/app/config/error_logger.dart';

void main() {
  runZonedGuarded(() async {
    FlavorConfig(
      name: "DEVELOP",
      color: Colors.red,
      location: BannerLocation.bottomStart,
      variables: {
        "google_place_api": 'AIzaSyCu52jF0FrKZoj2AgMlSDTvCU6pS1t3KU8',
        "url_api": "http://192.168.0.55:8080",
        "url_key": "admin",
      },
    );
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    FlutterError.onError = (error) {
      print("Error From INSIDE FRAME_WORK");
      ErrorLogger().logInternalFlutterError(error);
      print("----------------------");
    };
    return runApp(ModularApp(module: AppModule()));
  }, (error, stackTrace) {
    ErrorLogger().logInternal(error, stackTrace);
  });
}
