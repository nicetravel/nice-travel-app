# Nice Travel
[![pipeline status](https://gitlab.com/nicetravel/nice-travel-app/badges/master/pipeline.svg)](https://gitlab.com/nicetravel/nice-travel-app/-/commits/master) [![coverage report](https://gitlab.com/nicetravel/nice-travel-app/badges/master/coverage.svg)](https://gitlab.com/nicetravel/nice-travel-app/-/commits/master)

Projeto para possibilitar criar, adquirir e compartilhar roteiros de viagens.

Encontre roteiros ideias para sua viagem, gaste menos e aproveite mais. 
